using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class UIWidgetDepthSetterBase : MonoBehaviour
{
    public int DepthBase = 0;
}
