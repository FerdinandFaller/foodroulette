﻿using System;
using FoodRoulette;
using UnityEngine;

/// <summary>
/// Ever wanted to be able to auto-center on an object within a draggable panel?
/// Attach this script to the container that has the objects to center on as its children.
/// </summary>

[AddComponentMenu("NGUI/Interaction/Center On Child with Event")]
public class CenterOnChildWithEvent : MonoBehaviour
{
    public SpringPanel.OnFinished onFinished;

    public event Action<GameObject, GameObject> CenteredObjectChangedEvent;

    UIDraggablePanel mDrag;
    GameObject mCenteredObject;
    private GameObject mPrevCenteredObject;

    /// <summary>
    /// Game object that the draggable panel is currently centered on.
    /// </summary>

    public GameObject centeredObject { get { return mCenteredObject; } }

    void OnEnable() { Recenter(); }
    void OnDragFinished() { if (enabled) Recenter(); }

    /// <summary>
    /// Recenter the draggable list on the center-most child.
    /// </summary>

    public void Recenter()
    {
        if (mDrag == null)
        {
            mDrag = NGUITools.FindInParents<UIDraggablePanel>(gameObject);

            if (mDrag == null)
            {
                Logger.Log(new Log(GetType() + " requires " + typeof(UIDraggablePanel) + " on a parent object in order to work", PrintSeverity.LogWarning));
                enabled = false;
                return;
            }
            else
            {
                mDrag.onDragFinished = OnDragFinished;
            }
        }
        if (mDrag.panel == null) return;

        // Calculate the panel's center in world coordinates
        Vector4 clip = mDrag.panel.clipRange;
        Transform dt = mDrag.panel.cachedTransform;
        Vector3 center = dt.localPosition;
        center.x += clip.x;
        center.y += clip.y;
        center = dt.parent.TransformPoint(center);

        // Offset this value by the momentum
        Vector3 offsetCenter = center - mDrag.currentMomentum * (mDrag.momentumAmount * 0.1f);
        mDrag.currentMomentum = Vector3.zero;

        float min = float.MaxValue;
        Transform closest = null;
        Transform trans = transform;

        // Determine the closest child
        for (int i = 0, imax = trans.childCount; i < imax; ++i)
        {
            Transform t = trans.GetChild(i);
            float sqrDist = Vector3.SqrMagnitude(t.position - offsetCenter);

            if (sqrDist < min)
            {
                min = sqrDist;
                closest = t;
            }
        }

        if (closest != null)
        {
            mPrevCenteredObject = mCenteredObject;
            mCenteredObject = closest.gameObject;

            if (mCenteredObject != mPrevCenteredObject && CenteredObjectChangedEvent != null)
            {
                Logger.Log(new EventFiringLog("UICenterOnChildWithEvent - Recenter - CenteredObjectChangedEvent"));
                CenteredObjectChangedEvent(mPrevCenteredObject, mCenteredObject);
            }

            // Figure out the difference between the chosen child and the panel's center in local coordinates
            Vector3 cp = dt.InverseTransformPoint(closest.position);
            Vector3 cc = dt.InverseTransformPoint(center);
            Vector3 offset = cp - cc;

            // Offset shouldn't occur if blocked by a zeroed-out scale
            if (mDrag.scale.x == 0f) offset.x = 0f;
            if (mDrag.scale.y == 0f) offset.y = 0f;
            if (mDrag.scale.z == 0f) offset.z = 0f;

            // Spring the panel to this calculated position
            SpringPanel.Begin(mDrag.gameObject, dt.localPosition - offset, 8f).onFinished = onFinished;
        }
        else
        {
            mPrevCenteredObject = mCenteredObject;
            mCenteredObject = null;

            if (CenteredObjectChangedEvent != null)
            {
                Logger.Log(new EventFiringLog("UICenterOnChildWithEvent - Recenter - CenteredObjectChangedEvent"));
                CenteredObjectChangedEvent(mPrevCenteredObject, mCenteredObject);
            }
        }
    }
}