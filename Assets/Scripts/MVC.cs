﻿using UnityEngine;


public class MVC : MonoBehaviour
{
    public LogoView LogoView;
    public HomeView HomeView;
    public CategoriesView CategoriesView;
    public MealsOfCategoryView MealsOfCategoryView;
    public MealOverviewView MealOverviewView;
    public MealAddEditView MealAddEditView;
    public CategoryAddEditView CategoryAddEditView;
    public EditIngredientView EditIngredientView;
    public IngredientOfMealAddEditView IngredientOfMealAddEditView;
    public UnitAddEditView UnitAddEditView;
    public SearchView SearchView;
    public ConfirmView ConfirmView;
    public LoadingIndicatorView LoadingIndicatorView;
    public PlannerView PlannerView;
    public PlannerMealPickerView PlannerMealPickerView;
    public CreditsAndOptionsView CreditsAndOptionsView;


    void Start()
    {
        var model = new FoodRouletteModel(UIVersion.AppVersion);

        var args = new FoodRouletteController.FoodRouletteControllerArgs
            {
                CategoriesView = CategoriesView,
                LogoView = LogoView,
                HomeView = HomeView,
                CategoryAddEditView = CategoryAddEditView,
                EditIngredientView = EditIngredientView,
                IngredientOfMealAddEditView = IngredientOfMealAddEditView,
                MealAddEditView = MealAddEditView,
                MealOverviewView = MealOverviewView,
                MealsOfCategoryView = MealsOfCategoryView,
                UnitAddEditView = UnitAddEditView,
                SearchView = SearchView,
                ConfirmView = ConfirmView,
                LoadingIndicatorView = LoadingIndicatorView,
                PlannerView = PlannerView,
                PlannerMealPickerView = PlannerMealPickerView,
                CreditsAndOptionsView = CreditsAndOptionsView
            };
        var controller = new FoodRouletteController(model, args);
    }
}