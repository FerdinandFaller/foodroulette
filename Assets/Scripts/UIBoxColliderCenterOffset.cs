﻿using UnityEngine;


[ExecuteInEditMode]
public class UIBoxColliderCenterOffset : MonoBehaviour
{
    public Vector3 RelativeOffset = Vector3.zero;
    public Vector3 AbsoluteOffset = Vector3.zero;

    private BoxCollider _boxCollider;


    void Start()
    {
        _boxCollider = (BoxCollider) collider;
    }

    void Update()
    {
        var offset = _boxCollider.size;

        offset.x = offset.x * RelativeOffset.x;
        offset.y = offset.y * RelativeOffset.y;
        offset.z = offset.z * RelativeOffset.z;

        offset += AbsoluteOffset;

        if (_boxCollider.center != offset) _boxCollider.center = offset;
    }
}
