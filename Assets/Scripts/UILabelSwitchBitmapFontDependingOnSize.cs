using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(UILabel))]
[ExecuteInEditMode]
public class UILabelSwitchBitmapFontDependingOnSize : MonoBehaviour
{
    private UILabel _label;
    private Vector3 _cachedLocalScale;
    private static bool _isApplyingFont;
    private float _lastScaleCheck;
    private const float ScaleCheckInterval = 1f;
    private bool _scaleChanged;
    private const float ScaleThreshold = 3;


    void Awake()
    {
        _label = GetComponent<UILabel>();

        _cachedLocalScale = Vector3.zero;

        ClearFonts();
    }

    void Start()
    {
        SetClosestFontForCurrentSize();
    }
    
    void Update()
    {
        if (_label == null)
        {
            _label = GetComponent<UILabel>();
            return;
        }

        // Label transform changed
        if (Time.realtimeSinceStartup > _lastScaleCheck + ScaleCheckInterval &&
            (Math.Abs(_label.transform.localScale.x - _cachedLocalScale.x) > ScaleThreshold ||
            Math.Abs(_label.transform.localScale.y - _cachedLocalScale.y) > ScaleThreshold))
        {
            _lastScaleCheck = Time.realtimeSinceStartup;
            _scaleChanged = true;

            _cachedLocalScale = transform.localScale;

            SetClosestFontForCurrentSize();
        }
    }

    void LateUpdate()
    {
        if (_scaleChanged)
        {
            _scaleChanged = false;

            _cachedLocalScale = transform.localScale;
        }
    }

    private void SetClosestFontForCurrentSize()
    {
        var smallerAxis = Mathf.Min(_cachedLocalScale.x, _cachedLocalScale.y);
        _label.font = BitmapFontManager.GetClosestFont((int) smallerAxis);
    }

    private void ClearFonts()
    {
        var oldFonts = GetComponents<UIFont>();

        if (oldFonts != null)
        {
            // Destroy all but the first one
            for (int i = 0; i < oldFonts.Count(); i++)
            {
#if UNITY_EDITOR
                DestroyImmediate(oldFonts[i]);
#else
                Destroy(oldFonts[i]);
#endif
            }
        }
    }
}
