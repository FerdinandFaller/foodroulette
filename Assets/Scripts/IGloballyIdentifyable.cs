using System;

namespace FoodRoulette
{
    public interface IGloballyIdentifyable
    {
        Guid Guid { get; }
    }
}