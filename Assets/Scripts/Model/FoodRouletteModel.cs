using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using FoodRoulette;
using FoodRoulette.Model;
using UnityEngine;


public class FoodRouletteModel : IFoodRouletteModel
{
    public IFoodRouletteData Data { get; private set; }

    private Category _categoryInCategories;
    private Meal _mealInMealOverview;
    private string _dividerNameInMealAddEdit;
    private Meal _mealInMealAddEdit;
    private Meal _mealOfIngredientOfMealInIngredientOfMealAddEdit;
    private IngredientOfMeal _ingredientOfMealInIngredientOfMealAddEdit;
    private Ingredient _ingredientInEditIngredient;
    private Category _categoryInCategoryAddEdit;
    private Unit _unitInUnitAddEdit;

    private int _requestedPlannerSlotIndex;
    private readonly Version _appVersion;


    public FoodRouletteModel(Version appVersion)
    {
        Data = new JSONPrefsFoodRouletteData(this);
        _appVersion = appVersion;
    }

    public void Initialize()
    {
        DeSerializeEverything();
        Data.CheckAndUpdate(_appVersion);
    }

    public bool ContainsCategory(Guid categoryGuid)
    {
        return categoryGuid != Guid.Empty && Data.Categories.ContainsKey(categoryGuid);
    }

    public bool ContainsMeal(Guid mealGuid)
    {
        return mealGuid != Guid.Empty && Data.Meals.ContainsKey(mealGuid);
    }

    public bool ContainsIngredient(Guid ingredientGuid)
    {
        return ingredientGuid != Guid.Empty && Data.Ingredients.ContainsKey(ingredientGuid);
    }

    public bool ContainsUnit(Guid unitGuid)
    {
        return unitGuid != Guid.Empty && Data.Units.ContainsKey(unitGuid);
    }

    public void Get(Guid categoryGuid, out Category category)
    {
        category = ContainsCategory(categoryGuid) ? Data.Categories[categoryGuid] : null;
    }

    public void Get(Guid mealGuid, out Meal meal)
    {
        meal = ContainsMeal(mealGuid) ? Data.Meals[mealGuid] : null;
    }

    public void Get(Guid ingredientGuid, out Ingredient ingredient)
    {
        ingredient = ContainsIngredient(ingredientGuid) ? Data.Ingredients[ingredientGuid] : null;
    }

    public void Get(Guid unitGuid, out Unit unit)
    {
        unit = ContainsUnit(unitGuid) ? Data.Units[unitGuid] : null;
    }

    public void Get(string categoryName, out Category category)
    {
        if (string.IsNullOrEmpty(categoryName)) throw new ArgumentNullException("categoryName");

        ReadOnlyCollection<Category> collection;
        Get(categoryName, out collection);

        if (collection.Count > 0)
        {
            category = collection[0];
        }
        else
        {
            throw new ArgumentOutOfRangeException("categoryName");
        }
    }

    public void Get(string mealName, out Meal meal)
    {
        if (string.IsNullOrEmpty(mealName)) throw new ArgumentNullException("mealName");

        ReadOnlyCollection<Meal> collection;
        Get(mealName, out collection);

        if (collection.Count > 0)
        {
            meal = collection[0];
        }
        else
        {
            throw new ArgumentOutOfRangeException("mealName");
        }
    }

    public void Get(string ingredientName, out Ingredient ingredient)
    {
        if (string.IsNullOrEmpty(ingredientName)) throw new ArgumentNullException("ingredientName");

        ReadOnlyCollection<Ingredient> collection;
        Get(ingredientName, out collection);

        if (collection.Count > 0)
        {
            ingredient = collection[0];
        }
        else
        {
            throw new ArgumentOutOfRangeException("ingredientName");
        }
    }

    public void Get(string unitSingular, out Unit unit)
    {
        if (string.IsNullOrEmpty(unitSingular)) throw new ArgumentNullException("unitSingular");

        ReadOnlyCollection<Unit> collection;
        Get(unitSingular, out collection);

        unit = collection.Count > 0 ? collection[0] : null;
    }

    public void Get(string categoryName, out ReadOnlyCollection<Category> categories)
    {
        if (string.IsNullOrEmpty(categoryName)) throw new ArgumentNullException("categoryName");

        var collection = Data.Categories.Values.Where(c => c.Name == categoryName).ToList();

        categories = collection.AsReadOnly();

        if(categories.Count == 0) throw new ArgumentOutOfRangeException("categoryName");
    }

    public void Get(string mealName, out ReadOnlyCollection<Meal> meals)
    {
        if (string.IsNullOrEmpty(mealName)) throw new ArgumentNullException("mealName");

        var collection = Data.Meals.Values.Where(c => c.Name == mealName).ToList();

        meals = collection.AsReadOnly();

        if(meals.Count == 0) throw new ArgumentOutOfRangeException("mealName");
    }

    public void Get(string ingredientName, out ReadOnlyCollection<Ingredient> ingredients)
    {
        if (string.IsNullOrEmpty(ingredientName)) throw new ArgumentNullException("ingredientName");

        var collection = Data.Ingredients.Values.Where(c => c.Name == ingredientName).ToList();

        ingredients = collection.AsReadOnly();

        if(ingredients.Count == 0) throw new ArgumentOutOfRangeException("ingredientName");
    }

    public void Get(string unitSingular, out ReadOnlyCollection<Unit> units)
    {
        if (string.IsNullOrEmpty(unitSingular)) throw new ArgumentNullException("unitSingular");

        var collection = Data.Units.Values.Where(c => c.Singular == unitSingular).ToList();

        units = collection.AsReadOnly();
    }

    public void GetAll(out ReadOnlyCollection<Category> categories)
    {
        categories = new ReadOnlyCollection<Category>(Data.Categories.Values.ToList());
    }

    public void GetAll(out ReadOnlyCollection<Meal> meals)
    {
        meals = new ReadOnlyCollection<Meal>(Data.Meals.Values.ToList());
    }

    public void GetAll(out ReadOnlyCollection<Ingredient> ingredients)
    {
        ingredients = new ReadOnlyCollection<Ingredient>(Data.Ingredients.Values.ToList());
    }

    public void GetAll(out ReadOnlyCollection<Unit> units)
    {
        units = new ReadOnlyCollection<Unit>(Data.Units.Values.ToList());
    }

    public Unit GetNoUnitUnit()
    {
        Unit unit;
        Get("No Unit", out unit);

        if (unit == null)
        {
            unit = new Unit(Guid.NewGuid(), "No Unit", "No Units", "n/a");
            Add(unit);
            Serialize(unit);
        }

        return unit;
    }

    public Dictionary<Guid, Type> Search(string name)
    {
        // Search in Categories
        var results = Data.Categories.Values.Where(category => category.Name.ToUpper().Contains(name.ToUpper())).ToDictionary(category => category.Guid, category => category.GetType());

        // Search in Meals
        foreach (var meal in Data.Meals.Values.Where(meal => meal.Name.ToUpper().Contains(name.ToUpper())))
        {
            results.Add(meal.Guid, meal.GetType());
        }

        // Search in Ingredients
        foreach (var ingredient in Data.Ingredients.Values.Where(ingredient => ingredient.Name.ToUpper().Contains(name.ToUpper())))
        {
            results.Add(ingredient.Guid, ingredient.GetType());
        }

        // Search in Units
        foreach (var unit in Data.Units.Values.Where(unit => unit.Name.ToUpper().Contains(name.ToUpper())))
        {
            results.Add(unit.Guid, unit.GetType());
        }

        return results;
    }

    public void Add(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");
        if (Data.Categories.ContainsKey(category.Guid)) return;

        Data.Categories.Add(category.Guid, category);

        if (CategoryAddedEvent == null) return;
        CategoryAddedEvent(category);
    }
    public event Action<Category> CategoryAddedEvent;

    public void Add(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (Data.Meals.ContainsKey(meal.Guid)) return;

        Data.Meals.Add(meal.Guid, meal);

        if (MealAddedEvent == null) return;
        MealAddedEvent(meal);
    }
    public event Action<Meal> MealAddedEvent;

    public void Add(Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (Data.Ingredients.ContainsKey(ingredient.Guid)) return;

        Data.Ingredients.Add(ingredient.Guid, ingredient);

        if (IngredientAddedEvent == null) return;
        IngredientAddedEvent(ingredient);
    }
    public event Action<Ingredient> IngredientAddedEvent;

    public void Add(Unit unit)
    {
        if (unit == null) throw new ArgumentNullException("unit");
        if (Data.Units.ContainsKey(unit.Guid)) return;

        Data.Units.Add(unit.Guid, unit);

        if (UnitAddedEvent == null) return;
        UnitAddedEvent(unit);
    }
    public event Action<Unit> UnitAddedEvent;

    public void Remove(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");
        if (!Data.Categories.ContainsKey(category.Guid)) return;

        Data.Delete(category);

        if (CategoryRemovedEvent != null)
        {
            CategoryRemovedEvent(category);
        }
    }
    public event Action<Category> CategoryRemovedEvent;

    public void Remove(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) return;

        Data.Delete(meal);

        if (MealRemovedEvent != null)
        {
            MealRemovedEvent(meal);
        }
    }
    public event Action<Meal> MealRemovedEvent;

    public void Remove(Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) return;

        Data.Delete(ingredient);

        if (IngredientRemovedEvent != null)
        {
            IngredientRemovedEvent(ingredient);
        }
    }
    public event Action<Ingredient> IngredientRemovedEvent;

    public void Remove(Unit unit)
    {
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) return;
        if (unit == GetNoUnitUnit()) return;

        Data.Delete(unit);

        if (UnitRemovedEvent != null)
        {
            UnitRemovedEvent(unit);
        }
    }
    public event Action<Unit> UnitRemovedEvent;

    public void SetCategoryInMealsOfCategory(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");
        if (!Data.Categories.ContainsKey(category.Guid)) throw new ArgumentOutOfRangeException("category");

        _categoryInCategories = category;

        if (CategoryInMealsOfCategoryWasSetEvent == null) return;
        CategoryInMealsOfCategoryWasSetEvent(category);
    }
    public event Action<Category> CategoryInMealsOfCategoryWasSetEvent;

    public void GetCategoryInMealsOfCategory(out Category category)
    {
        category = _categoryInCategories;
    }

    public void SetMealInMealOverview(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");

        _mealInMealOverview = meal;

        if (MealInMealOverviewWasSetEvent == null) return;
        MealInMealOverviewWasSetEvent(meal);
    }
    public event Action<Meal> MealInMealOverviewWasSetEvent;

    public void GetMealInMealOverview(out Meal meal)
    {
        meal = _mealInMealOverview;
    }

    public void SetSelectedDividerInMealAddEdit(string dividerName)
    {
        if (string.IsNullOrEmpty(dividerName)) throw new ArgumentNullException("dividerName");

        _dividerNameInMealAddEdit = dividerName;

        if (SelectedDividerInMealAddEditWasSetEvent == null) return;
        SelectedDividerInMealAddEditWasSetEvent(dividerName);
    }
    public event Action<string> SelectedDividerInMealAddEditWasSetEvent;

    public void GetSelectedDividerInMealAddEdit(out string dividerName)
    {
        dividerName = _dividerNameInMealAddEdit;
    }

    public void SetMealInMealAddEdit(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        //if (_mealInMealAddEdit == meal) return;

        _mealInMealAddEdit = meal;

        if (MealInMealAddEditWasSetEvent == null) return;
        MealInMealAddEditWasSetEvent(meal);
    }
    public event Action<Meal> MealInMealAddEditWasSetEvent;

    public void GetMealInMealAddEdit(out Meal meal)
    {
        meal = _mealInMealAddEdit;
    }

    public void SetMealAndIngredientOfMealInIngredientOfMealAddEdit(Meal meal, IngredientOfMeal ingredientOfMeal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredientOfMeal == null) throw new ArgumentNullException("ingredientOfMeal");
        if (ingredientOfMeal.IngredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientOfMeal");
        if (float.IsInfinity(ingredientOfMeal.Amount) || float.IsNaN(ingredientOfMeal.Amount)) throw new ArgumentNullException("ingredientOfMeal");

        _mealOfIngredientOfMealInIngredientOfMealAddEdit = meal;
        _ingredientOfMealInIngredientOfMealAddEdit = ingredientOfMeal;

        if (MealAndIngredientOfMealInMealAddEditWasSetEvent == null) return;
        MealAndIngredientOfMealInMealAddEditWasSetEvent(meal, ingredientOfMeal);
    }
    public event Action<Meal, IngredientOfMeal> MealAndIngredientOfMealInMealAddEditWasSetEvent;

    public void GetMealAndIngredientOfMealInIngredientOfMealAddEdit(out Meal meal, out IngredientOfMeal ingredientOfMeal)
    {
        meal = _mealOfIngredientOfMealInIngredientOfMealAddEdit;
        ingredientOfMeal = _ingredientOfMealInIngredientOfMealAddEdit;
    }

    public void SetCategoryInCategoryAddEdit(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");
        if (!Data.Categories.ContainsKey(category.Guid)) throw new ArgumentOutOfRangeException("category");
        //if (_categoryInCategoryAddEdit == category) return;

        _categoryInCategoryAddEdit = category;

        if (CategoryInCategoryAddEditWasSetEvent == null) return;
        CategoryInCategoryAddEditWasSetEvent(category);
    }
    public event Action<Category> CategoryInCategoryAddEditWasSetEvent;

    public void GetCategoryInCategoryAddEdit(out Category category)
    {
        category = _categoryInCategoryAddEdit;
    }

    public void SetUnitInUnitAddEdit(Unit unit)
    {
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");
        //if (_unitInUnitAddEdit == unit) return;

        _unitInUnitAddEdit = unit;

        if (UnitInUnitAddEditWasSetEvent == null) return;
        UnitInUnitAddEditWasSetEvent(unit);
    }
    public event Action<Unit> UnitInUnitAddEditWasSetEvent;

    public void GetUnitInUnitAddEdit(out Unit unit)
    {
        unit = _unitInUnitAddEdit;
    }

    public void SetIngredientInEditIngredient(Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!ContainsIngredient(ingredient.Guid)) return;

        _ingredientInEditIngredient = ingredient;

        if (IngredientInEditIngredientWasSetEvent != null) IngredientInEditIngredientWasSetEvent(ingredient);
    }
    public event Action<Ingredient> IngredientInEditIngredientWasSetEvent;

    public void GetIngredientInEditIngredient(out Ingredient ingredient)
    {
        ingredient = _ingredientInEditIngredient;
    }

    public void AddMealToCategory(Meal meal, Category category)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (category == null) throw new ArgumentNullException("category");
        if (!Data.Categories.ContainsKey(category.Guid)) throw new ArgumentOutOfRangeException("category");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (category.ContainsMeal(meal)) return;

        category.AddMeal(meal);

        if (MealAddedToCategoryEvent == null) return;
        MealAddedToCategoryEvent(meal, category);
    }
    public event Action<Meal, Category> MealAddedToCategoryEvent;

    public void RemoveMealFromCategory(Meal meal, Category category)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (category == null) throw new ArgumentNullException("category");
        if (!Data.Categories.ContainsKey(category.Guid)) throw new ArgumentOutOfRangeException("category");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!category.ContainsMeal(meal)) return;

        category.RemoveMeal(meal);

        if (MealRemovedFromCategoryEvent == null) return;
        MealRemovedFromCategoryEvent(meal, category);
    }
    public event Action<Meal, Category> MealRemovedFromCategoryEvent;

    public void AddIngredientToMeal(Meal meal, Ingredient ingredient, float amount, Unit unit)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentNullException("unit");
        if (float.IsInfinity(amount) || float.IsNaN(amount)) throw new ArgumentOutOfRangeException("amount");
        if (meal.ContainsIngredient(ingredient.Guid)) return;

        meal.AddIngredient(ingredient.Guid, amount, unit.Guid);

        if (IngredientAddedToMealEvent == null) return;
        IngredientAddedToMealEvent(meal, ingredient, amount, unit);
    }
    public event Action<Meal, Ingredient, float, Unit> IngredientAddedToMealEvent;

    public void RemoveIngredientFromMeal(Meal meal, Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");
        if (!meal.ContainsIngredient(ingredient.Guid)) return;

        meal.RemoveIngredient(ingredient.Guid);

        if (IngredientOfMealRemovedFromMealEvent == null) return;
        IngredientOfMealRemovedFromMealEvent(meal, ingredient);
    }
    public event Action<Meal, Ingredient> IngredientOfMealRemovedFromMealEvent;

    public void SetNameOfCategory(string name, Category category)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (category == null) throw new ArgumentNullException("category");
        if (name != null && !Data.Categories.ContainsKey(category.Guid)) throw new ArgumentOutOfRangeException("category");

        category.Name = string.IsNullOrEmpty(name) ? string.Empty : name;

        if (NameOfCategoryWasSetEvent == null) return;
        NameOfCategoryWasSetEvent(name, category);
    }
    public event Action<string, Category> NameOfCategoryWasSetEvent;

    public void SetNameOfMeal(string name, Meal meal)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");

        meal.Name = name;

        if (NameOfMealWasSetEvent == null) return;
        NameOfMealWasSetEvent(name, meal);
    }
    public event Action<string, Meal> NameOfMealWasSetEvent;

    public void SetNameOfIngredient(string name, Ingredient ingredient)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");

        ingredient.Name = name;

        if (NameOfIngredientWasSetEvent == null) return;
        NameOfIngredientWasSetEvent(name, ingredient);
    }
    public event Action<string, Ingredient> NameOfIngredientWasSetEvent;

    public void SetNameOfUnit(string name, Unit unit)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");

        unit.Name = name;

        if (NameOfUnitWasSetEvent == null) return;
        NameOfUnitWasSetEvent(name, unit);
    }
    public event Action<string, Unit> NameOfUnitWasSetEvent;

    public void SetRecipeOfMeal(string text, Meal meal)
    {
        if (text == null) throw new ArgumentNullException("text");
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");

        meal.Recipe = text;

        if (RecipeOfMealWasSetEvent == null) return;
        RecipeOfMealWasSetEvent(text, meal);
    }
    public event Action<string, Meal> RecipeOfMealWasSetEvent;

    public void SetAmountOfIngredientInMeal(Meal meal, Ingredient ingredient, float amount)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (float.IsInfinity(amount) || float.IsNaN(amount)) throw new ArgumentOutOfRangeException("amount");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");

        meal.ChangeIngredientAmount(ingredient.Guid, amount);

        if (AmountOfIngredientInMealWasSetEvent == null) return;
        AmountOfIngredientInMealWasSetEvent(meal, ingredient, amount);
    }
    public event Action<Meal, Ingredient, float> AmountOfIngredientInMealWasSetEvent;

    public void GetAmountOfIngredientInMeal(out float amount, Meal meal, Ingredient ingredient)
    {
        amount = 0;

        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");
        if (!meal.ContainsIngredient(ingredient.Guid)) return;

        amount = meal.GetIngredientAmount(ingredient.Guid);
    }

    public void SetUnitOfIngredientInMeal(Meal meal, Ingredient ingredient, Unit unit)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");

        meal.ChangeIngredientUnit(ingredient.Guid, unit.Guid);

        if (UnitOfIngredientInMealWasSetEvent == null) return;
        UnitOfIngredientInMealWasSetEvent(meal, ingredient, unit);
    }
    public event Action<Meal, Ingredient, Unit> UnitOfIngredientInMealWasSetEvent;

    public void GetUnitOfIngredientInMeal(out Unit unit, Meal meal, Ingredient ingredient)
    {
        unit = null;

        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");
        if (!meal.ContainsIngredient(ingredient.Guid)) return;

        var unitGuid = meal.GetIngredientUnitGuid(ingredient.Guid);
        unit = Data.Units[unitGuid];
    }

    public void SetSingularOfUnit(string singular, Unit unit)
    {
        if (singular == null) throw new ArgumentNullException("singular");
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");

        unit.Singular = singular;

        if (SingularOfUnitWasSetEvent == null) return;
        SingularOfUnitWasSetEvent(singular, unit);
    }
    public event Action<string, Unit> SingularOfUnitWasSetEvent;

    public void SetPluralOfUnit(string plural, Unit unit)
    {
        if (plural == null) throw new ArgumentNullException("plural");
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");

        unit.Plural = plural == string.Empty ? unit.Singular : plural;

        if (PluralOfUnitWasSetEvent == null) return;
        PluralOfUnitWasSetEvent(unit.Plural, unit);
    }
    public event Action<string, Unit> PluralOfUnitWasSetEvent;

    public void SetUnitOfMeasurementOfUnit(string unitOfMeasurement, Unit unit)
    {
        if (unitOfMeasurement == null) throw new ArgumentNullException("unitOfMeasurement");
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");

        unit.UnitOfMeasurement = unitOfMeasurement;

        if (UnitOfMeasurementOfUnitWasSetEvent == null) return;
        UnitOfMeasurementOfUnitWasSetEvent(unitOfMeasurement, unit);
    }
    public event Action<string, Unit> UnitOfMeasurementOfUnitWasSetEvent;

    public void GetAllPlannerMeals(out ReadOnlyCollection<Meal> plannerMeals)
    {
        plannerMeals = new ReadOnlyCollection<Meal>(Data.PlannerMeals);
    }

    public void SetMealInPlannerSlotIndex(Meal meal, int slotIndex)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");
        if (slotIndex < 0 || slotIndex > 6) throw new ArgumentOutOfRangeException("slotIndex");

        Data.PlannerMeals[slotIndex] = meal;

        if (MealWasSetInPlannerSlotEvent != null)
        {
            MealWasSetInPlannerSlotEvent(meal, slotIndex);
        }
    }
    public event Action<Meal, int> MealWasSetInPlannerSlotEvent;

    public void GetRequestedPlannerSlotIndex(out int slotIndex)
    {
        slotIndex = _requestedPlannerSlotIndex;
    }

    public void SetRequestedPlannerSlotIndex(int slotIndex)
    {
        if (slotIndex < 0 || slotIndex > 6) throw new ArgumentOutOfRangeException("slotIndex");

        _requestedPlannerSlotIndex = slotIndex;

        if (RequestedPlannerSlotIndexWasSetEvent != null)
        {
            RequestedPlannerSlotIndexWasSetEvent(_requestedPlannerSlotIndex);
        }
    }
    public event Action<int> RequestedPlannerSlotIndexWasSetEvent;

    public void SerializeEverything()
    {
        SerializeAllPlannerMeals();
        SerializeAllCategories();
        SerializeAllMeals();
        SerializeAllIngredients();
        SerializeAllUnits();
        
        if (EverythingSerializedEvent == null) return;
        EverythingSerializedEvent();
    }
    public event Action EverythingSerializedEvent;

    public void DeSerializeEverything()
    {
        //Debug.LogError(Time.realtimeSinceStartup);
        DeSerializeAllUnits();
        DeSerializeAllIngredients();
        DeSerializeAllMeals();
        DeSerializeAllCategories();
        DeSerializeAllPlannerMeals();
        //Debug.LogError(Time.realtimeSinceStartup);

        if (EverythingDeSerializedEvent == null) return;
        EverythingDeSerializedEvent();
    }
    public event Action EverythingDeSerializedEvent;

    public void SerializeAllCategories()
    {
        try
        {
            Data.WriteAllCategories();

            if (AllCategoriesSerializedEvent == null) return;
            AllCategoriesSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllCategoriesSerializedEvent;

    public void DeSerializeAllCategories()
    {
        try
        {
            Data.ReadAllCategories();
            
            if (AllCategoriesDeSerializedEvent == null) return;
            AllCategoriesDeSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllCategoriesDeSerializedEvent;

    public void SerializeAllMeals()
    {
        try
        {
            Data.WriteAllMeals();

            if (AllMealsSerializedEvent == null) return;
            AllMealsSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllMealsSerializedEvent;

    public void DeSerializeAllMeals()
    {
        try
        {
            Data.ReadAllMeals();

            if (AllMealsDeSerializedEvent == null) return;
            AllMealsDeSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllMealsDeSerializedEvent;

    public void SerializeAllIngredients()
    {
        try
        {
            Data.WriteAllIngredients();

            if (AllIngredientsSerializedEvent == null) return;
            AllIngredientsSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllIngredientsSerializedEvent;

    public void DeSerializeAllIngredients()
    {
        try
        {
            Data.ReadAllIngredients();

            if (AllIngredientsDeSerializedEvent == null) return;
            AllIngredientsDeSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllIngredientsDeSerializedEvent;

    public void SerializeAllUnits()
    {
        try
        {
            Data.WriteAllUnits();

            if (AllUnitsSerializedEvent == null) return;
            AllUnitsSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllUnitsSerializedEvent;

    public void DeSerializeAllUnits()
    {
        try
        {
            Data.ReadAllUnits();

            if (AllUnitsDeSerializedEvent == null) return;
            AllUnitsDeSerializedEvent();
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
            throw;
        }
    }
    public event Action AllUnitsDeSerializedEvent;

    public void Serialize(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");
        if (!Data.Categories.ContainsKey(category.Guid)) throw new ArgumentOutOfRangeException("category");

        try
        {
            Data.Write(category);

            if (CategorySerializedEvent == null) return;
            CategorySerializedEvent(category);
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
        }
    }
    public event Action<Category> CategorySerializedEvent;

    public void Serialize(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (!Data.Meals.ContainsKey(meal.Guid)) throw new ArgumentOutOfRangeException("meal");

        try
        {
            Data.Write(meal);

            if (MealSerializedEvent == null) return;
            MealSerializedEvent(meal);
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
        }
    }
    public event Action<Meal> MealSerializedEvent;

    public void Serialize(Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!Data.Ingredients.ContainsKey(ingredient.Guid)) throw new ArgumentOutOfRangeException("ingredient");

        try
        {
            Data.Write(ingredient);

            if (IngredientSerializedEvent == null) return;
            IngredientSerializedEvent(ingredient);
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
        }
    }
    public event Action<Ingredient> IngredientSerializedEvent;

    public void Serialize(Unit unit)
    {
        if (unit == null) throw new ArgumentNullException("unit");
        if (!Data.Units.ContainsKey(unit.Guid)) throw new ArgumentOutOfRangeException("unit");

        try
        {
            Data.Write(unit);

            if (UnitSerializedEvent == null) return;
            UnitSerializedEvent(unit);
        }
        catch (Exception ex)
        {
            Logger.Log(new Log(ex.Message, PrintSeverity.LogWarning));
        }
    }
    public event Action<Unit> UnitSerializedEvent;

    public void SerializeAllPlannerMeals()
    {
        if(Data.PlannerMeals == null) throw new NullReferenceException("PlannerMeals is null");

        Data.WriteAllPlannerMeals();

        if (AllPlannerMealsSerialized != null)
        {
            AllPlannerMealsSerialized();
        }
    }
    public event Action AllPlannerMealsSerialized;

    public void DeSerializeAllPlannerMeals()
    {
        Data.ReadAllPlannerMeals();

        if (AllPlannerMealsDeSerialized != null)
        {
            AllPlannerMealsDeSerialized();
        }
    }
    public event Action AllPlannerMealsDeSerialized;
}