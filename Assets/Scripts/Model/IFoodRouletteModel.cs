using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace FoodRoulette.Model
{
    public interface IFoodRouletteModel
    {
        IFoodRouletteData Data { get; }

        void Initialize();

        bool ContainsCategory(Guid categoryGuid);
        bool ContainsMeal(Guid mealGuid);
        bool ContainsIngredient(Guid ingredientGuid);
        bool ContainsUnit(Guid unitGuid);

        void Get(Guid categoryGuid, out Category category);
        void Get(Guid mealGuid, out Meal meal);
        void Get(Guid ingredientGuid, out Ingredient ingredient);
        void Get(Guid unitGuid, out Unit unit);

        void Get(string categoryName, out Category category);
        void Get(string mealName, out Meal meal);
        void Get(string ingredientName, out Ingredient ingredient);
        void Get(string unitSingular, out Unit unit);

        void Get(string categoryName, out ReadOnlyCollection<Category> categories);
        void Get(string mealName, out ReadOnlyCollection<Meal> meals);
        void Get(string ingredientName, out ReadOnlyCollection<Ingredient> ingredients);
        void Get(string unitSingular, out ReadOnlyCollection<Unit> units);

        void GetAll(out ReadOnlyCollection<Category> categories);
        void GetAll(out ReadOnlyCollection<Meal> meals);
        void GetAll(out ReadOnlyCollection<Ingredient> ingredients);
        void GetAll(out ReadOnlyCollection<Unit> units);

        Unit GetNoUnitUnit();

        Dictionary<Guid, Type> Search(string name);

        void Add(Category category);
        event Action<Category> CategoryAddedEvent;
        void Add(Meal meal);
        event Action<Meal> MealAddedEvent;
        void Add(Ingredient ingredient);
        event Action<Ingredient> IngredientAddedEvent;
        void Add(Unit unit);
        event Action<Unit> UnitAddedEvent;

        void Remove(Category category);
        event Action<Category> CategoryRemovedEvent;
        void Remove(Meal meal);
        event Action<Meal> MealRemovedEvent;
        void Remove(Ingredient ingredient);
        event Action<Ingredient> IngredientRemovedEvent;
        void Remove(Unit unit);
        event Action<Unit> UnitRemovedEvent;

        void SetCategoryInMealsOfCategory(Category category);
        event Action<Category> CategoryInMealsOfCategoryWasSetEvent;
        void GetCategoryInMealsOfCategory(out Category category);

        void SetMealInMealOverview(Meal meal);
        event Action<Meal> MealInMealOverviewWasSetEvent;
        void GetMealInMealOverview(out Meal meal);

        void SetSelectedDividerInMealAddEdit(string dividerName);
        event Action<string> SelectedDividerInMealAddEditWasSetEvent;
        void GetSelectedDividerInMealAddEdit(out string dividerName);

        void SetMealInMealAddEdit(Meal meal);
        event Action<Meal> MealInMealAddEditWasSetEvent;
        void GetMealInMealAddEdit(out Meal meal);

        void SetMealAndIngredientOfMealInIngredientOfMealAddEdit(Meal meal, IngredientOfMeal ingredientOfMeal);
        event Action<Meal, IngredientOfMeal> MealAndIngredientOfMealInMealAddEditWasSetEvent;
        void GetMealAndIngredientOfMealInIngredientOfMealAddEdit(out Meal meal, out IngredientOfMeal ingredientOfMeal);

        void SetCategoryInCategoryAddEdit(Category category);
        event Action<Category> CategoryInCategoryAddEditWasSetEvent;
        void GetCategoryInCategoryAddEdit(out Category category);

        void SetUnitInUnitAddEdit(Unit unit);
        event Action<Unit> UnitInUnitAddEditWasSetEvent;
        void GetUnitInUnitAddEdit(out Unit unit);

        void SetIngredientInEditIngredient(Ingredient ingredient);
        event Action<Ingredient> IngredientInEditIngredientWasSetEvent;
        void GetIngredientInEditIngredient(out Ingredient ingredient);

        void AddMealToCategory(Meal meal, Category category);
        event Action<Meal, Category> MealAddedToCategoryEvent;
        void RemoveMealFromCategory(Meal meal, Category category);
        event Action<Meal, Category> MealRemovedFromCategoryEvent;

        void AddIngredientToMeal(Meal meal, Ingredient ingredient, float amount, Unit unit);
        event Action<Meal, Ingredient, float, Unit> IngredientAddedToMealEvent;
        void RemoveIngredientFromMeal(Meal meal, Ingredient ingredient);
        event Action<Meal, Ingredient> IngredientOfMealRemovedFromMealEvent;

        void SetNameOfCategory(string name, Category category);
        event Action<string, Category> NameOfCategoryWasSetEvent;
        void SetNameOfMeal(string name, Meal meal);
        event Action<string, Meal> NameOfMealWasSetEvent;
        void SetNameOfIngredient(string name, Ingredient ingredient);
        event Action<string, Ingredient> NameOfIngredientWasSetEvent;
        void SetNameOfUnit(string name, Unit unit);
        event Action<string, Unit> NameOfUnitWasSetEvent;

        void SetRecipeOfMeal(string text, Meal meal);
        event Action<string, Meal> RecipeOfMealWasSetEvent;

        void SetAmountOfIngredientInMeal(Meal meal, Ingredient ingredient, float amount);
        event Action<Meal, Ingredient, float> AmountOfIngredientInMealWasSetEvent;
        void GetAmountOfIngredientInMeal(out float amount, Meal meal, Ingredient ingredient);

        void SetUnitOfIngredientInMeal(Meal meal, Ingredient ingredient, Unit unit);
        event Action<Meal, Ingredient, Unit> UnitOfIngredientInMealWasSetEvent;
        void GetUnitOfIngredientInMeal(out Unit unit, Meal meal, Ingredient ingredient);

        void SetSingularOfUnit(string singular, Unit unit);
        event Action<string, Unit> SingularOfUnitWasSetEvent;
        void SetPluralOfUnit(string plural, Unit unit);
        event Action<string, Unit> PluralOfUnitWasSetEvent;
        void SetUnitOfMeasurementOfUnit(string unitOfMeasurement, Unit unit);
        event Action<string, Unit> UnitOfMeasurementOfUnitWasSetEvent;

        void GetAllPlannerMeals(out ReadOnlyCollection<Meal> plannerMeals);
        void SetMealInPlannerSlotIndex(Meal meal, int slotIndex);
        event Action<Meal, int> MealWasSetInPlannerSlotEvent;

        void GetRequestedPlannerSlotIndex(out int slotIndex);
        void SetRequestedPlannerSlotIndex(int slotIndex);
        event Action<int> RequestedPlannerSlotIndexWasSetEvent;

        void SerializeEverything();
        event Action EverythingSerializedEvent;

        void DeSerializeEverything();
        event Action EverythingDeSerializedEvent;

        void SerializeAllCategories();
        event Action AllCategoriesSerializedEvent;

        void DeSerializeAllCategories();
        event Action AllCategoriesDeSerializedEvent;

        void SerializeAllMeals();
        event Action AllMealsSerializedEvent;

        void DeSerializeAllMeals();
        event Action AllMealsDeSerializedEvent;

        void SerializeAllIngredients();
        event Action AllIngredientsSerializedEvent;

        void DeSerializeAllIngredients();
        event Action AllIngredientsDeSerializedEvent;

        void SerializeAllUnits();
        event Action AllUnitsSerializedEvent;

        void DeSerializeAllUnits();
        event Action AllUnitsDeSerializedEvent;

        void Serialize(Category category);
        event Action<Category> CategorySerializedEvent;

        void Serialize(Meal meal);
        event Action<Meal> MealSerializedEvent;

        void Serialize(Ingredient ingredient);
        event Action<Ingredient> IngredientSerializedEvent;

        void Serialize(Unit unit);
        event Action<Unit> UnitSerializedEvent;

        void SerializeAllPlannerMeals();
        event Action AllPlannerMealsSerialized;

        void DeSerializeAllPlannerMeals();
        event Action AllPlannerMealsDeSerialized;
    }
}