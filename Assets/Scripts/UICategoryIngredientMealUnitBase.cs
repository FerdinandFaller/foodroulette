﻿using System;
using FoodRoulette;
using UnityEngine;


public class UICategoryIngredientMealUnitBase<T> : MonoBehaviour, IComparable<T>, IGloballyIdentifyable where T : CategoryIngredientMealUnitBase
{
    public UILabel NameLabel;
    public UISprite Background;

    public Guid Guid { get; private set; }

    private T _content;
    public T Content
    {
        get { return _content; }

        internal set
        {
            var oldContent = _content;
            _content = value;

            if (oldContent == _content) return;

            if (ContentChangedEvent != null)
            {
                Logger.Log(new EventFiringLog("UICategoryIngredientMealUnitBase - Content(Set) - ContentChangedEvent - oldContent: " + oldContent + " newContent: " + _content));
                ContentChangedEvent(oldContent, _content);
            }

            if (_content == null) return;

            // Subscribe Events
            _content.NameChangedEvent -= OnContentNameChanged;
            _content.NameChangedEvent += OnContentNameChanged;
            Logger.Log(new EventSubscriptionLog("UICategoryIngredientMealUnitBase - Content(Set) - _content.NameChangedEvent += OnContentNameChanged"));

            // Trigger first time
            if (oldContent != null)
            {
                OnContentNameChanged(this, new NameChangedEventArgs(oldContent.Name, _content.Name));
            }
            else
            {
                OnContentNameChanged(this, new NameChangedEventArgs(null, _content.Name));
            }
        }
    }

    public event Action<T, T> ContentChangedEvent;


    protected virtual void Start()
    {
        Guid = Guid.NewGuid();

        UIStretch stretchScript = null;
        if (Background != null) stretchScript = Background.GetComponent<UIStretch>();
        if (stretchScript != null)
        {
            var grid = NGUITools.FindInParents<UIGrid>(gameObject);
            stretchScript.panelContainer = NGUITools.FindInParents<UIPanel>(grid.gameObject);
        }
    }

    protected virtual void OnEnable()
    {
        if (_content == null) return;

        RefreshLabelText();
    }

    protected virtual void OnContentNameChanged(object sender, NameChangedEventArgs args)
    {
        Logger.Log(new EventReceivingLog("UICategoryIngredientMealUnitBase - OnContentNameChanged - NewName: " + args.NewName));

        if (args == null) throw new ArgumentNullException("args");
        if (args.NewName == null) throw new NullReferenceException();

        if (NameLabel != null) NameLabel.text = args.NewName;
        if (gameObject != null) gameObject.name = args.NewName;
    }

    public virtual void RefreshLabelText()
    {
        if (NameLabel == null) return;

        NameLabel.text = _content.Name;
    }

    public int CompareTo(T other)
    {
        if (other == null) throw new ArgumentNullException("other");

        return Guid.CompareTo(other.Guid);
    }

    public override string ToString()
    {
        return _content.Name;
    }

    void OnDestroy()
    {
        if (_content != null)
        {
            _content.NameChangedEvent -= OnContentNameChanged;
        }
    }
}
