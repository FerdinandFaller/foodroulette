using System;

namespace FoodRoulette
{
    public class IngredientOfMeal :  IComparable<IngredientOfMeal>
    {
        private Guid _ingredientGuid;
        public Guid IngredientGuid
        {
            get { return _ingredientGuid; }
            set
            {
                var temp = value;

                _ingredientGuid = value;

                if (temp == Guid.Empty || IngredientChangedEvent == null) return;
                Logger.Log(new EventFiringLog("IngredientOfMeal - IngredientChangedEvent - NewAmount: " + value));
                IngredientChangedEvent(this, new IngredientChangedEventArgs { OldIngredient = temp, NewIngredient = value });
            }
        }

        private float _amount;
        public float Amount
        {
            get { return _amount; }
            set
            {
                var temp = _amount;

                _amount = value;

                if (Math.Abs(temp - _amount) < 0.00001f || AmountChangedEvent == null) return;
                Logger.Log(new EventFiringLog("IngredientOfMeal - AmountChangedEvent - NewAmount: " + value));
                AmountChangedEvent(this, new AmountChangedEventArgs{OldAmount = temp, NewAmount = value});
            }
        }

        private Guid _unitGuid;
        public Guid UnitGuid
        {
            get { return _unitGuid; }
            set
            {
                var temp = value;

                _unitGuid = value;

                if (temp == Guid.Empty || UnitChangedEvent == null) return;
                Logger.Log(new EventFiringLog("IngredientOfMeal - UnitChangedEvent - NewUnit: " + value));
                UnitChangedEvent(this, new UnitChangedEventArgs{OldUnit = temp, NewUnit = value});
            }
        }

        public event EventHandler<IngredientChangedEventArgs> IngredientChangedEvent;
        public event EventHandler<AmountChangedEventArgs> AmountChangedEvent;
        public event EventHandler<UnitChangedEventArgs> UnitChangedEvent;


        public int CompareTo(IngredientOfMeal other)
        {
            if (IngredientGuid.CompareTo(other.IngredientGuid) == 0)
            {
                return UnitGuid.CompareTo(other.UnitGuid) == 0 ? 0 : UnitGuid.CompareTo(other.UnitGuid);
            }

            return IngredientGuid.CompareTo(other.IngredientGuid);
        }
    }
}