using FoodRoulette;


public class UIMeal : UICategoryIngredientMealUnitBase<Meal>
{
    public override void RefreshLabelText()
    {
        NameLabel.text = Content.Name;
    }
}
