using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class UIWidgetDepthSetterOffset : MonoBehaviour
{
    public int DepthOffset = 0;

    private UIWidget _uiWidget;
    private UIWidgetDepthSetterBase _base;


    void Awake()
    {
        _uiWidget = gameObject.GetComponent<UIWidget>();
        _base = NGUITools.FindInParents<UIWidgetDepthSetterBase>(gameObject);
    }

    void Start()
    {
        if (_uiWidget == null)  _uiWidget = gameObject.GetComponent<UIWidget>();
        if (_base == null) _base = NGUITools.FindInParents<UIWidgetDepthSetterBase>(gameObject);
    }

    void Update()
    {
        if(_uiWidget == null) return;
        if(_base == null) return;
        if(_uiWidget.depth == _base.DepthBase + DepthOffset) return;

        _uiWidget.depth = _base.DepthBase + DepthOffset;
    }
}
