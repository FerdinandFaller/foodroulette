using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace FoodRoulette
{
    public static class ViewsManager
    {
        private static readonly List<FoodRouletteView> History;
        private static readonly Dictionary<FoodRouletteView, List<Collider>> ColliderStates;
        private static FoodRouletteView _currentView;
        private static Job _loadingIndicatorJob;

        public static bool HasPreviousView
        {
            get
            {
                if (History.Count < 2)
                {
                    return false;
                }
                return History[History.Count - 2] != null;
            }
        }

        public static FoodRouletteView PreviousView
        {
            get{ return History[History.Count - 2]; }
        }

        public static List<FoodRouletteView> Views;

        public static event Action<FoodRouletteView, FoodRouletteView> NextEvent; // Previous, Current
        public static event Action<FoodRouletteView, FoodRouletteView> BackEvent; // OldCurrent, Current


        static ViewsManager()
        {
            History = new List<FoodRouletteView>();
            Views = new List<FoodRouletteView>();
            ColliderStates = new Dictionary<FoodRouletteView, List<Collider>>();
        }

        public static void Next<T>(bool addToHistory = true, float previousViewFadeOutDuration = 0.75f, float nextViewFadeInDuration = 0.5f, float nextViewMoveInDuration = 0.5f) where T : FoodRouletteView
        {
            var prevView = _currentView;
            _currentView = GetView<T>();
            if (_currentView == null) throw new NullReferenceException();
            if (prevView == _currentView) return;
            if (!Views.Contains(_currentView)) return;

            if (HasPreviousView && PreviousView == _currentView)
            {
                Back();
                return;
            }

            if (addToHistory) History.Add(_currentView);

            if (NextEvent != null)
            {
                Logger.Log(new EventFiringLog("ViewsManager - Next - NextEvent"));
                NextEvent(prevView, _currentView);
            }

            SaveColliderStates(_currentView);

            if (prevView != null)
            {
                FadeOut(prevView.gameObject, previousViewFadeOutDuration);
                EnableOrDisableCollidersOfView(prevView, false);
            }

            FadeIn(_currentView.gameObject, nextViewFadeInDuration);
            MoveIn(_currentView.gameObject, nextViewMoveInDuration, Vector3.zero);

            EnableOrDisableCollidersOfView(_currentView, true);
        }

        public static void Back(float currentScreenFadeOutDuration = 0.5f, float currentScreenMoveOutDuration = 0.5f, float previousScreenFadeInDuration = 0.25f)
        {
            if (!HasPreviousView)
            {
                Logger.Log(new Log("ViewsManager - Back - No previousUiScreen available", PrintSeverity.LogWarning));
                return;
            }

            var oldCurrent = _currentView;
            _currentView = PreviousView;

            History.RemoveAt(History.Count - 1);

            if (BackEvent != null)
            {
                Logger.Log(new EventFiringLog("ViewsManager - Back - BackEvent"));
                BackEvent(oldCurrent, _currentView);
            }

            FadeOut(oldCurrent.gameObject, currentScreenFadeOutDuration);
            MoveOut(oldCurrent.gameObject, currentScreenMoveOutDuration, Vector3.zero);
            EnableOrDisableCollidersOfView(oldCurrent, false);

            FadeIn(_currentView.gameObject, previousScreenFadeInDuration);
            MoveIn(_currentView.gameObject, 0, Vector3.zero);
            EnableOrDisableCollidersOfView(_currentView, true);
        }

        public static void ShowConfirmationView(string labelText, bool showOnlyConfirmButton, EventHandler<UIButtonMessageWithEvent.TriggeredEventArgs> confirmButtonTriggeredHandler, EventHandler<UIButtonMessageWithEvent.TriggeredEventArgs> cancelButtonTriggeredHandler)
        {
            var confirmView = GetView<ConfirmView>() as ConfirmView;

            if (confirmView != null)
            {
                confirmView.Label.text = labelText;
                confirmView.ConfirmButtonTriggeredHandler = confirmButtonTriggeredHandler;
                confirmView.CancelButtonTriggeredHandler = cancelButtonTriggeredHandler;

                if (showOnlyConfirmButton)
                {
                    confirmView.ConfirmButtonCentered.gameObject.SetActive(true);
                    confirmView.ConfirmButton.gameObject.SetActive(false);
                    confirmView.CancelButton.gameObject.SetActive(false);
                }
                else
                {
                    confirmView.ConfirmButtonCentered.gameObject.SetActive(false);
                    confirmView.ConfirmButton.gameObject.SetActive(true);
                    confirmView.CancelButton.gameObject.SetActive(true);
                }

                FadeIn(confirmView.gameObject, 0.5f);
                MoveIn(confirmView.gameObject, 0, new Vector3(0,0,-0.5f));

                EnableOrDisableCollidersOfView(_currentView, false);
            }
        }

        public static void HideConfirmationView()
        {
            var confirmView = GetView<ConfirmView>() as ConfirmView;

            if (confirmView != null)
            {
                FadeOut(confirmView.gameObject, 0.5f);
                MoveOut(confirmView.gameObject, 0.5f, new Vector3(0,0,-0.5f));
            }

            EnableOrDisableCollidersOfView(_currentView, true);
        }

        public static void ShowLoadingIndicatorView(Vector2 relativeOffset)
        {
            var loadingIndicatorView = GetView<LoadingIndicatorView>() as LoadingIndicatorView;

            if (loadingIndicatorView != null)
            {
                FadeIn(loadingIndicatorView.gameObject, 0);
                MoveIn(loadingIndicatorView.gameObject, 0, new Vector3(0, 0, -0.5f));

                _loadingIndicatorJob = Job.Make(loadingIndicatorView.Run(relativeOffset));
            }
        }

        public static void HideLoadingIndicatorView()
        {
            var loadingIndicatorView = GetView<LoadingIndicatorView>() as LoadingIndicatorView;

            if (loadingIndicatorView != null)
            {
                FadeOut(loadingIndicatorView.gameObject, 0.5f);
                MoveOut(loadingIndicatorView.gameObject, 0, new Vector3(0, 0, -0.5f));

                loadingIndicatorView.Stop();
            }

            if (_loadingIndicatorJob != null && _loadingIndicatorJob.Running) _loadingIndicatorJob.Kill();
        } 

        public static FoodRouletteView GetView<T>() where T : FoodRouletteView
        {
            if (Views.Count == 0) throw new NullReferenceException("Can't get Views before they are filled by UIViewsManager");

            return Views.FirstOrDefault(view => view.GetType() == typeof(T));
        }

        private static void SaveColliderStates(FoodRouletteView view)
        {
            // Revert first to avoid problems with multiple calls
            EnableOrDisableCollidersOfView(view, true);

            // Get all active Colliders of the views gameObject and its children
            var colliders = view.gameObject.GetComponentsInChildren<Collider>();

            if(ColliderStates.ContainsKey(view)) ColliderStates.Remove(view);
            ColliderStates.Add(view, new List<Collider>(colliders));
        }

        private static void EnableOrDisableCollidersOfView(FoodRouletteView view, bool shouldBeEnabled)
        {
            if (!ColliderStates.ContainsKey(view))
            {
                if(!shouldBeEnabled)
                {
                    // We want to disable the colliders, but dont have them saved yet

                    SaveColliderStates(view);
                }
                else
                {
                    // We can't restore something that we don't have saved
                    return;
                }
            }

            List<Collider> colliderList;
            ColliderStates.TryGetValue(view, out colliderList);

            if (colliderList != null)
            {
                foreach (Collider collider in colliderList)
                {
                    if(collider != null) collider.enabled = shouldBeEnabled;
                }
            }
        }

        public static void JumpIn(GameObject go, Vector3 offSet)
        {
            var z = go.transform.localPosition.z;
            go.transform.localPosition = new Vector3(0,0,z) + offSet;
        }

        public static void JumpOut(GameObject go, Vector3 offSet)
        {
            float backgroundWidth = Screen.width * 10;
            if (_currentView != null) backgroundWidth = _currentView.GetBackgroundScale().x;

            var z = go.transform.localPosition.z;
            go.transform.localPosition = new Vector3(backgroundWidth, 0, z) + offSet;
        }

        public static void MoveIn(GameObject go, float duration, Vector3 offSet)
        {
            JumpOut(go, offSet);
            var z = go.transform.localPosition.z;
            var tween = TweenPosition.Begin(go, duration, new Vector3(0,0,z) + offSet);
            tween.method = UITweener.Method.EaseIn;
            tween.tweenGroup = 1;
        }

        public static void MoveOut(GameObject go, float duration, Vector3 offSet)
        {
            float backgroundWidth = Screen.width * 10;
            if (_currentView != null) backgroundWidth = _currentView.GetBackgroundScale().x;

            JumpIn(go, offSet);
            var z = go.transform.localPosition.z;
            var tween = TweenPosition.Begin(go, duration, new Vector3(backgroundWidth, 0, z) + offSet);
            tween.method = UITweener.Method.EaseOut;
            tween.tweenGroup = 2;
        }

        public static void AlphaIn(GameObject go)
        {
            go.GetComponent<UIPanel>().SetAlphaRecursive(1, true);
        }

        public static void AlphaOut(GameObject go)
        {
            go.GetComponent<UIPanel>().SetAlphaRecursive(0.01f, true);
        }

        public static void FadeIn(GameObject go, float duration)
        {
            AlphaOut(go);

            var panels = go.GetComponentsInChildren<UIPanel>();
            foreach (UIPanel uiPanel in panels)
            {
                var t = TweenAlpha.Begin(uiPanel.gameObject, duration, 1);
                t.method = UITweener.Method.EaseIn;
                t.tweenGroup = 3;
            }
        }

        public static void FadeOut(GameObject go, float duration)
        {
            AlphaIn(go);
            
            var viewPanel = go.GetComponent<UIPanel>();
            var panels = go.GetComponentsInChildren<UIPanel>();
            foreach (UIPanel uiPanel in panels)
            {
                var t = TweenAlpha.Begin(uiPanel.gameObject, duration, 0.01f);
                if(uiPanel == viewPanel) t.onFinished += OnFadeOutFinished;
                t.method = UITweener.Method.EaseOut;
                t.tweenGroup = 4;
            }
        }

        private static void OnFadeOutFinished(UITweener tween)
        {
            JumpOut(tween.gameObject, Vector3.zero);
        }
    }
}