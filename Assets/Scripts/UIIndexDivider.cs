using System;
using System.Collections.Generic;
using FoodRoulette;
using UnityEngine;


public class UIIndexDivider : MonoBehaviour
{
    private UIButtonTween _tweenInScript;
    private UIButtonTween _tweenOutScript;

    private SortedList<GameObject, GameObject> _dividerContentList;
    private int _prevIndex = -1;

    public int ActiveIndex = 0;
    public List<GameObject> Dividers;
    public List<GameObject> Contents;


    private void Start()
    {
        _dividerContentList = new SortedList<GameObject, GameObject>(new GameObjectComparer());

        for (var i = 0; i < Dividers.Count; i++)
        {
            _dividerContentList.Add(Dividers[i], Contents[i]);
        }

        // Setup Dividers
        foreach (var go in _dividerContentList.Keys)
        {
            // Event Script
            var eventScript = go.GetComponent<UIButtonMessageWithEvent>() ??
                                go.AddComponent<UIButtonMessageWithEvent>();

            
            eventScript.trigger = UIButtonMessageWithEvent.Trigger.OnClick;
            eventScript.TriggeredEvent += OnDividerClick;
            Logger.Log(new EventSubscriptionLog("UIIndexDivider - Awake - messageScript.TriggeredEvent += OnDividerClick"));

            // Scale Script
            var scaleScript = go.GetComponent<TweenScale>() ??
                              go.AddComponent<TweenScale>();

            scaleScript.enabled = false;
            scaleScript.to = new Vector3(1, 1.5f, 1);
            scaleScript.method = UITweener.Method.EaseInOut;
            scaleScript.duration = 0.25f;

            // Position Script
            var positionScript = go.GetComponent<TweenPosition>() ??
                                 go.AddComponent<TweenPosition>();

            positionScript.enabled = false;
            var pos = go.transform.localPosition;
            const int sizeY = 80; // TODO: Shouldn't be hardcoded!
            positionScript.from = pos;
            positionScript.to = new Vector3(pos.x, -(pos.y + sizeY * 0.25f), pos.z);
            positionScript.method = UITweener.Method.EaseInOut;
            positionScript.duration = 0.25f;
        }

        // Setup Contents
        for (var i = 0; i < _dividerContentList.Values.Count; i++)
        {
            var go = _dividerContentList.Values[i];

            // Deactivate all Contents except the one that is active at the start
            go.SetActive(ActiveIndex == i);
        }

        ScaleDivider(_dividerContentList.Keys[ActiveIndex], true);
        TransformDivider(_dividerContentList.Keys[ActiveIndex], true);
    }

    private void OnDividerClick(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Logger.Log(new EventReceivingLog("UIIndexDivider - OnDividerClick - sender: " + sender));

        var divider = sender as UIButtonMessageWithEvent;
        if (divider == null) throw new ArgumentNullException("sender");

        SelectDivider(divider.gameObject);
    }

    private void SelectDivider(GameObject divider)
    {
        if(!_dividerContentList.ContainsKey(divider)) throw new ArgumentOutOfRangeException("divider");

        // Has the index changed?
        var i = _dividerContentList.IndexOfKey(divider);
        if (i == ActiveIndex) return;

        _prevIndex = ActiveIndex;
        ActiveIndex = i;

        ScaleDivider(_dividerContentList.Keys[ActiveIndex], true);
        ScaleDivider(_dividerContentList.Keys[_prevIndex], false);
        TransformDivider(_dividerContentList.Keys[ActiveIndex], true);
        TransformDivider(_dividerContentList.Keys[_prevIndex], false);

        _dividerContentList.Values[ActiveIndex].SetActive(true);
        _dividerContentList.Values[_prevIndex].SetActive(false);
    }

    private void ScaleDivider(GameObject divider, bool forward)
    {
        if(!_dividerContentList.ContainsKey(divider)) throw new ArgumentOutOfRangeException("divider");

        divider.GetComponent<TweenScale>().Play(forward);
    }

    private void TransformDivider(GameObject divider, bool forward)
    {
        if (!_dividerContentList.ContainsKey(divider)) throw new ArgumentOutOfRangeException("divider");

        divider.GetComponent<TweenPosition>().Play(forward);
    }
}