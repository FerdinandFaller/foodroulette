using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(UILabel))]
[ExecuteInEditMode]
public class UILabelAdjustDynamicFontSize : MonoBehaviour
{
    public UIFont FontPrefab;
    private UILabel _label;
    private Vector3 _cachedLocalScale;
    private UIFont _font;
    private bool _wasRecalculated;
    private static bool _isApplyingFont;


    void Awake()
    {
        _label = GetComponent<UILabel>();

        AddFont();
    }

    void Start()
    {
        // This has to be in Start and not in Awake. Else Listelements would get their own Panel, due to not yet being assigned as child of one.
        SetFont();
    }
    
    void Update()
    {
        if (FontPrefab == null) return;

        if(_wasRecalculated)
        {
            _cachedLocalScale = _label.transform.localScale;
            _wasRecalculated = false;
        }

        // Label transform changed
        if (_font == _label.font)
        {
            if (Math.Abs(_label.transform.localScale.x - _cachedLocalScale.x) > 1 ||
                Math.Abs(_label.transform.localScale.y - _cachedLocalScale.y) > 1)
            {
                if(_font != null)_font.dynamicFontSize = CalculateNewDynamicFontSize();
            }
        }
    }

    private int CalculateNewDynamicFontSize()
    {
        _wasRecalculated = true;
        return (int)(Mathf.Clamp(Mathf.Max(_label.transform.localScale.x, _label.transform.localScale.y), 1, 128) + 0.5f);
    }

    private void SetFont()
    {
        if (FontPrefab == null || _font == null) return;

        _font.atlas = FontPrefab.atlas;
        _font.dynamicFont = FontPrefab.dynamicFont;
        _font.dynamicFontSize = FontPrefab.dynamicFontSize;
        _font.dynamicFontStyle = FontPrefab.dynamicFontStyle;
        _font.horizontalSpacing = FontPrefab.horizontalSpacing;
        _font.material = FontPrefab.material;
        _font.pixelSize = FontPrefab.pixelSize;
        _font.replacement = FontPrefab.replacement;
        _font.spriteName = FontPrefab.spriteName;
        _font.uvRect = FontPrefab.uvRect;
        _font.verticalSpacing = FontPrefab.verticalSpacing;
        //_font.MarkAsDirty();

        _label.font = _font;
        _font.dynamicFontSize = CalculateNewDynamicFontSize();
    }

    private void AddFont()
    {
        var oldFonts = GetComponents<UIFont>();

        if (oldFonts.Any())
        {
            // Destroy all but the first one
            for (int i = 1; i < oldFonts.Count(); i++)
            {
#if UNITY_EDITOR
                DestroyImmediate(oldFonts[i]);
#else
                Destroy(oldFonts[i]);
#endif
            }

            _font = oldFonts[0];
        }
        else
        {
            // We dont have old fonts, create a new one
            if (FontPrefab == null)
            {
                Debug.LogError("Font Prefab is missing!");
                return;
            }

            _font = _label.gameObject.AddComponent<UIFont>();
        }
    }
}
