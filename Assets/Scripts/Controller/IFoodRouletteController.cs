using System.Collections.Generic;

namespace FoodRoulette.Controller
{
    public interface IFoodRouletteController
    {
        #region Categories

        void AddNewCategory(out Category newCategory, string name = "New Category");
        void RemoveCategory(Category category);

        #endregion // Categories


        #region MealsOfCategory View

        void SetCategoryInMealsOfCategory(Category category);
        void AddNewMeal(out Meal newMeal, string name = "New Meal");
        void AddMealToCategory(Meal meal, Category category);

        #endregion // MealsOfCategory View


        #region MealOverview

        void SetMealInMealOverview(Meal meal);
        void RemoveMeal(Meal meal);

        #endregion // MealOverview


        #region MealAddEdit

        void SetSelectedDividerInMealAddEdit(string dividerName);
        void SetMealInMealAddEdit(Meal meal);
        void SetNameOfMeal(string name, Meal meal);
        void RemoveMealFromCategory(Meal meal, Category category);
        void AddNewIngredient(out Ingredient newIngredient, string name = "New Ingredient");
        void AddIngredientToMeal(Meal meal, Ingredient ingredient, float amount, Unit unit);
        void RemoveIngredientFromMeal(Meal meal, Ingredient ingredient);
        void SetAmountOfIngredientInMeal(Meal meal, Ingredient ingredient, float amount);
        void SetUnitOfIngredientInMeal(Meal meal, Ingredient ingredient, Unit unit);
        void SetRecipeOfMeal(string text, Meal meal);

        #endregion // MealAddEdit


        #region CategoryAddEdit

        void SetCategoryInCategoryAddEdit(Category category);
        void SetNameOfCategory(string name, Category category);
        //void AddMealToCategory(Meal meal, Category category); <- Categories
        //void RemoveMealFromCategory(Meal meal, Category category); <- MealAddEdit

        #endregion // CategoryAddEdit


        #region IngredientOfMealAddEdit

        void SetMealAndIngredientOfMealInIngredientOfMealAddEdit(Meal meal, IngredientOfMeal ingredientOfMeal);
        void SetNameOfIngredient(string name, Ingredient ingredient);
        void AddNewUnit(out Unit newUnit, string singular = "New Unit", string plural = "New Units", string unitOfMeasurement = "nu");
        void RemoveIngredient(Ingredient ingredient);

        #endregion // IngredientOfMealAddEdit


        #region UnitAddEdit

        void SetUnitInUnitAddEdit(Unit unit);
        void SetNameOfUnit(string name, Unit unit);
        void SetSingularOfUnit(string singular, Unit unit);
        void SetPluralOfUnit(string plural, Unit unit);
        void SetUnitOfMeasurementOfUnit(string unitOfMeasurement, Unit unit);
        void RemoveUnit(Unit unit);

        #endregion // UnitAddEdit


        #region Planner

        void SetMealInPlannerSlotIndex(Meal meal, int slotIndex);

        #endregion // Planner


        #region PlannerMealPicker

        void SetRequestedPlannerSlotIndex(int slotIndex);

        #endregion // PlannerMealPicker


        #region EditIngredient

        void SetIngredientInEditIngredientView(Ingredient ingredient);

        #endregion
    }
}