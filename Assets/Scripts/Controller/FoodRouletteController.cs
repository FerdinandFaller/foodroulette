using System;
using System.Collections.Generic;
using FoodRoulette;
using FoodRoulette.Controller;
using FoodRoulette.Model;
using UnityEngine;


public class FoodRouletteController : IFoodRouletteController
{
    public struct FoodRouletteControllerArgs
    {
        public LogoView LogoView;
        public HomeView HomeView;
        public CategoriesView CategoriesView;
        public MealsOfCategoryView MealsOfCategoryView;
        public MealOverviewView MealOverviewView;
        public MealAddEditView MealAddEditView;
        public CategoryAddEditView CategoryAddEditView;
        public EditIngredientView EditIngredientView;
        public IngredientOfMealAddEditView IngredientOfMealAddEditView;
        public UnitAddEditView UnitAddEditView;
        public SearchView SearchView;
        public ConfirmView ConfirmView;
        public LoadingIndicatorView LoadingIndicatorView;
        public PlannerView PlannerView;
        public PlannerMealPickerView PlannerMealPickerView;
        public CreditsAndOptionsView CreditsAndOptionsView;
    }

    public IFoodRouletteModel Model;

    public LogoView LogoView;
    public HomeView HomeView;
    public CategoriesView CategoriesView;
    public MealsOfCategoryView MealsOfCategoryView;
    public MealOverviewView MealOverviewView;
    public MealAddEditView MealAddEditView;
    public CategoryAddEditView CategoryAddEditView;
    public EditIngredientView EditIngredientView;
    public IngredientOfMealAddEditView IngredientOfMealAddEditView;
    public UnitAddEditView UnitAddEditView;
    public SearchView SearchView;
    public ConfirmView ConfirmView;
    public LoadingIndicatorView LoadingIndicatorView;
    public PlannerView PlannerView;
    public PlannerMealPickerView PlannerMealPickerView;
    public CreditsAndOptionsView CreditsAndOptionsView;


    public FoodRouletteController(IFoodRouletteModel model, FoodRouletteControllerArgs args)
    {
        if (model == null) throw new ArgumentNullException("model");

        Model = model;

        LogoView = args.LogoView;
        LogoView.Initialize(this, model);

        HomeView = args.HomeView;
        HomeView.Initialize(this, Model);

        CategoriesView = args.CategoriesView;
        CategoriesView.Initialize(this, Model);

        MealsOfCategoryView = args.MealsOfCategoryView;
        MealsOfCategoryView.Initialize(this, Model);

        MealOverviewView = args.MealOverviewView;
        MealOverviewView.Initialize(this, Model);

        MealAddEditView = args.MealAddEditView;
        MealAddEditView.Initialize(this, Model);

        CategoryAddEditView = args.CategoryAddEditView;
        CategoryAddEditView.Initialize(this, Model);

        EditIngredientView = args.EditIngredientView;
        EditIngredientView.Initialize(this, Model);

        IngredientOfMealAddEditView = args.IngredientOfMealAddEditView;
        IngredientOfMealAddEditView.Initialize(this, Model);

        UnitAddEditView = args.UnitAddEditView;
        UnitAddEditView.Initialize(this, Model);

        SearchView = args.SearchView;
        SearchView.Initialize(this, Model);

        ConfirmView = args.ConfirmView;
        ConfirmView.Initialize(this, Model);

        LoadingIndicatorView = args.LoadingIndicatorView;
        LoadingIndicatorView.Initialize(this, Model);

        PlannerView = args.PlannerView;
        PlannerView.Initialize(this, Model);

        PlannerMealPickerView = args.PlannerMealPickerView;
        PlannerMealPickerView.Initialize(this, Model);

        CreditsAndOptionsView = args.CreditsAndOptionsView;
        CreditsAndOptionsView.Initialize(this, Model);

        Model.Initialize();
    }

    public void AddNewCategory(out Category newCategory, string name = "New Category")
    {
        if (string.IsNullOrEmpty(name)) name = "New Category";
        newCategory = new Category(Guid.NewGuid(), name);
        Model.Add(newCategory);

        Model.Serialize(newCategory);
    }

    public void RemoveCategory(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");

        Model.Remove(category);
    }

    public void SetCategoryInMealsOfCategory(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");

        Model.SetCategoryInMealsOfCategory(category);
    }

    public void AddNewMeal(out Meal newMeal, string name = "New Meal")
    {
        if (string.IsNullOrEmpty(name)) name = "New Meal";
        newMeal = new Meal(Guid.NewGuid(), name);
        Model.Add(newMeal);

        Model.Serialize(newMeal);
    }

    public void AddMealToCategory(Meal meal, Category category)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (category == null) throw new ArgumentNullException("category");

        Model.AddMealToCategory(meal, category);

        Model.Serialize(category);
    }

    public void SetMealInMealOverview(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");

        Model.SetMealInMealOverview(meal);
    }

    public void RemoveMeal(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");

        Model.Remove(meal);
    }

    public void SetSelectedDividerInMealAddEdit(string dividerName)
    {
        if (string.IsNullOrEmpty(dividerName)) throw new ArgumentNullException("dividerName");

        Model.SetSelectedDividerInMealAddEdit(dividerName);
    }

    public void SetMealInMealAddEdit(Meal meal)
    {
        if (meal == null) throw new ArgumentNullException("meal");

        // If it is a different Meal than the current meal, switch back to the Categories Divider
        Meal currMeal;
        Model.GetMealInMealAddEdit(out currMeal);

        Model.SetMealInMealAddEdit(meal);

        if (currMeal != meal) SetSelectedDividerInMealAddEdit("Categories");
    }

    public void SetNameOfMeal(string name, Meal meal)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (meal == null) throw new ArgumentNullException("meal");

        Model.SetNameOfMeal(name, meal);

        Model.Serialize(meal);
    }

    public void RemoveMealFromCategory(Meal meal, Category category)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (category == null) throw new ArgumentNullException("category");

        Model.RemoveMealFromCategory(meal, category);

        Model.Serialize(category);
    }

    public void AddNewIngredient(out Ingredient newIngredient, string name = "New Ingredient")
    {
        if (string.IsNullOrEmpty(name)) name = "New Ingredient";
        newIngredient = new Ingredient(Guid.NewGuid(), name);
        Model.Add(newIngredient);

        Model.Serialize(newIngredient);
    }

    public void AddIngredientToMeal(Meal meal, Ingredient ingredient, float amount, Unit unit)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (float.IsInfinity(amount) || float.IsNaN(amount)) throw new ArgumentOutOfRangeException("amount");
        
        if (unit == null)
        {
            unit = Model.GetNoUnitUnit();
        }

        Model.AddIngredientToMeal(meal, ingredient, amount, unit);

        Model.Serialize(meal);
    }

    public void RemoveIngredientFromMeal(Meal meal, Ingredient ingredient)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");

        Model.RemoveIngredientFromMeal(meal, ingredient);

        Model.Serialize(meal);
    }

    public void RemoveIngredient(Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");

        Model.Remove(ingredient);
    }

    public void SetAmountOfIngredientInMeal(Meal meal, Ingredient ingredient, float amount)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (float.IsInfinity(amount) || float.IsNaN(amount)) throw new ArgumentOutOfRangeException("amount");
        
        Model.SetAmountOfIngredientInMeal(meal, ingredient, amount);

        Model.Serialize(meal);
    }

    public void SetUnitOfIngredientInMeal(Meal meal, Ingredient ingredient, Unit unit)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (unit == null) throw new ArgumentNullException("unit");

        Model.SetUnitOfIngredientInMeal(meal, ingredient, unit);

        Model.Serialize(meal);
    }

    public void SetRecipeOfMeal(string text, Meal meal)
    {
        if (text == null) throw new ArgumentNullException("text");
        if (meal == null) throw new ArgumentNullException("meal");

        Model.SetRecipeOfMeal(text, meal);

        Model.Serialize(meal);
    }

    public void SetCategoryInCategoryAddEdit(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");

        Model.SetCategoryInCategoryAddEdit(category);
    }

    public void SetMealAndIngredientOfMealInIngredientOfMealAddEdit(Meal meal, IngredientOfMeal ingredientOfMeal)
    {
        if (meal == null) throw new ArgumentNullException("meal");
        if (ingredientOfMeal == null) throw new ArgumentNullException("ingredientOfMeal");
        if (ingredientOfMeal.IngredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientOfMeal");
        if (float.IsInfinity(ingredientOfMeal.Amount) || float.IsNaN(ingredientOfMeal.Amount)) throw new ArgumentNullException("ingredientOfMeal");

        Model.SetMealAndIngredientOfMealInIngredientOfMealAddEdit(meal, ingredientOfMeal);
    }

    public void SetNameOfCategory(string name, Category category)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (category == null) throw new ArgumentNullException("category");

        Model.SetNameOfCategory(name, category);

        Model.Serialize(category);
    }

    public void SetNameOfIngredient(string name, Ingredient ingredient)
    {
        if (name == null) throw new ArgumentNullException("name");
        if (ingredient == null) return;

        Model.SetNameOfIngredient(name, ingredient);

        Model.Serialize(ingredient);
    }

    public void AddNewUnit(out Unit newUnit, string singular = "New Unit", string plural = "New Units", string unitOfMeasurement = "nu")
    {
        if (string.IsNullOrEmpty(singular)) singular = "New Unit";
        if (string.IsNullOrEmpty(plural)) plural = "New Units";
        if (string.IsNullOrEmpty(unitOfMeasurement)) unitOfMeasurement = "nu";

        newUnit = new Unit(Guid.NewGuid(), singular, plural, unitOfMeasurement);
        Model.Add(newUnit);

        Model.Serialize(newUnit);
    }

    public void SetUnitInUnitAddEdit(Unit unit)
    {
        if (unit == null) throw new ArgumentNullException("unit");

        Model.SetUnitInUnitAddEdit(unit);
    }

    public void SetNameOfUnit(string name, Unit unit)
    {
        if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
        if (unit == null) throw new ArgumentNullException("unit");

        Model.SetNameOfUnit(name, unit);

        Model.Serialize(unit);
    }

    public void SetSingularOfUnit(string singular, Unit unit)
    {
        if (singular == null) throw new ArgumentNullException("singular");
        if (unit == null) throw new ArgumentNullException("unit");

        Model.SetSingularOfUnit(singular, unit);

        Model.Serialize(unit);
    }

    public void SetPluralOfUnit(string plural, Unit unit)
    {
        if (plural == null) throw new ArgumentNullException("plural");
        if (unit == null) throw new ArgumentNullException("unit");

        Model.SetPluralOfUnit(plural, unit);

        Model.Serialize(unit);
    }

    public void SetUnitOfMeasurementOfUnit(string unitOfMeasurement, Unit unit)
    {
        if (unitOfMeasurement == null) throw new ArgumentNullException("unitOfMeasurement");
        if (unit == null) throw new ArgumentNullException("unit");

        Model.SetUnitOfMeasurementOfUnit(unitOfMeasurement, unit);

        Model.Serialize(unit);
    }

    public void RemoveUnit(Unit unit)
    {
        if (unit == null) throw new ArgumentNullException("unit");
        if (unit == Model.GetNoUnitUnit()) return;
        
        Model.Remove(unit);
    }

    public void SetMealInPlannerSlotIndex(Meal meal, int slotIndex)
    {
        Model.SetMealInPlannerSlotIndex(meal, slotIndex);

        Model.SerializeAllPlannerMeals();
    }

    public void SetRequestedPlannerSlotIndex(int slotIndex)
    {
        Model.SetRequestedPlannerSlotIndex(slotIndex);
    }

    public void SetIngredientInEditIngredientView(Ingredient ingredient)
    {
        if (ingredient == null) throw new ArgumentNullException("ingredient");
        if (!Model.ContainsIngredient(ingredient.Guid)) return;

        Model.SetIngredientInEditIngredient(ingredient);
    }
}