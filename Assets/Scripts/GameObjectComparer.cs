using System.Collections.Generic;
using UnityEngine;


namespace FoodRoulette
{
    public class GameObjectComparer : IComparer<GameObject>
    {
        public int Compare(GameObject x, GameObject y)
        {
            return string.CompareOrdinal(x.name, y.name);
        }
    }
}