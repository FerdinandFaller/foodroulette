using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FoodRoulette
{
    public class Category : CategoryIngredientMealUnitBase
    {
        #region EventArgs

        public class MealEventArgs : EventArgs
        {
            public Meal Meal;
        }

        #endregion // EventArgs

        private readonly List<Guid> _meals;

        public int MealsCount { get { return _meals.Count; }}

        public event EventHandler<MealEventArgs> MealAddedEvent;
        public event EventHandler<MealEventArgs> MealRemovedEvent;


        public Category(Guid guid, string name) : base(guid, name)
        {
            _meals = new List<Guid>();
        }

        public void AddMealWithoutEvent(Guid guid)
        {
            if (guid == Guid.Empty) throw new ArgumentException("Guid is empty, can't add", "guid");
            if (_meals.Contains(guid)) return;

            _meals.Add(guid);
        }

        public void AddMeal(Meal meal)
        {
            if (meal == null) throw new ArgumentNullException("meal");
            if (_meals.Contains(meal.Guid)) return;

            _meals.Add(meal.Guid);

            if (MealAddedEvent == null) return;
            Logger.Log(new EventFiringLog("Category - AddMeal - MealAddedEvent - Meal: " + meal));
            MealAddedEvent(this, new MealEventArgs { Meal = meal });
        }

        public void RemoveMeal(Meal meal)
        {
            if (meal == null) throw new ArgumentNullException("meal");
            if (!_meals.Contains(meal.Guid)) return;

            _meals.Remove(meal.Guid);

            if (MealRemovedEvent == null) return;
            Logger.Log(new EventFiringLog("Category - RemoveMeal - MealRemovedEvent - Meal: " + meal));
            MealRemovedEvent(this, new MealEventArgs { Meal = meal });
        }

        public ReadOnlyCollection<Guid> GetReadOnlyMealGuids()
        {
            return _meals.AsReadOnly();
        }

        public bool ContainsMeal(Meal meal)
        {
            if (meal == null) throw new ArgumentNullException("meal");

            return _meals.Contains(meal.Guid);
        }
    }
}