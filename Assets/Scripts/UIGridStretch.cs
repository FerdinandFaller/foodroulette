using UnityEngine;

[ExecuteInEditMode]
public class UIGridStretch : MonoBehaviour
{
    public UIWidget WidgetContainer = null;
    public Vector2 RelativeSize = Vector2.one;

    private UIGrid _grid;
    private UIPanel _panelContainer = null;
    

    void Awake()
    {
        _panelContainer = NGUITools.FindInParents<UIPanel>(gameObject);
        _grid = GetComponent<UIGrid>();
    }

    void Update()
    {
        if (_panelContainer == null && WidgetContainer == null) return;

        Vector2 containerSize;
        if (WidgetContainer != null)
        {
            containerSize = new Vector2(WidgetContainer.relativeSize.x * WidgetContainer.cachedTransform.localScale.x,
                                        WidgetContainer.relativeSize.y * WidgetContainer.cachedTransform.localScale.y);
        }
        else
        {
            // z = width
            // w = height
            containerSize = new Vector2(_panelContainer.clipRange.z, 
                                        _panelContainer.clipRange.w);
        }

        // In this version we're adjusting the grid width and height.
        var cellSize = new Vector2( containerSize.x * RelativeSize.x,
                                    containerSize.y * RelativeSize.y);

        if(_grid.cellWidth != cellSize.x || _grid.cellHeight != cellSize.y)
        {
            _grid.cellWidth = cellSize.x;
            _grid.cellHeight = cellSize.y;

            _grid.repositionNow = true;
        } 
    }
}