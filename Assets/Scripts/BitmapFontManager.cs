using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class BitmapFontManager : MonoBehaviour
{
    public List<int> AvailableFontSizes;
    public List<UIFont> AvailableFonts;

    public static BitmapFontManager Instance;

    private static SortedList<int, UIFont> _sizeToFont;
    

    void Awake()
    {
        MapSizesToFonts();

        Instance = gameObject.GetComponent<BitmapFontManager>();
    }

    private void MapSizesToFonts()
    {
        if (AvailableFonts.Count == 0 || AvailableFontSizes.Count == 0) return;

        if (_sizeToFont == null)
        {
            _sizeToFont = new SortedList<int, UIFont>();
        }

        // Fill the Dictionary
        if (AvailableFonts.Count == AvailableFontSizes.Count)
        {
            for (int i = 0; i < AvailableFontSizes.Count; i++)
            {
                if (AvailableFonts[i] != null && !_sizeToFont.ContainsKey(AvailableFontSizes[i]))
                {
                    _sizeToFont.Add(AvailableFontSizes[i], AvailableFonts[i]);
                }
            }
        }
    }

    public static UIFont GetClosestFont(int fontSize)
    {
		if(_sizeToFont == null || _sizeToFont.Count == 0) return null;
		
        // Go through each entry in the list. Caclulate the diff between the current key and the size. Check if the diff between the previous key and size was higher. 
        // If yes, continue. If no, take the previous keys value as font.

        UIFont previousFont = null;
        var previousDiff = -2f;
        for (int i = 0; i < _sizeToFont.Count; i++)
        {
            var diff = Mathf.Abs(_sizeToFont.Keys[i] - fontSize);

            // Do we already have previous data?
            if (previousDiff > -1f)
            {
                // Is the previous Diff smaller than the current diff?
                if (previousDiff <= diff)
                {
                    // Yes, previous is closer. Use the previousFont as font
                    return previousFont;
                }
                else
                {
                    // No current diff is smaller. Is this the last index?
                    if (_sizeToFont.Count -1 == i)
                    {
                        // Use the current font as font
                        return _sizeToFont.Values[i];
                    }
                }
            }

            previousDiff = diff;
            previousFont = _sizeToFont.Values[i];
        }

        return _sizeToFont.Values[_sizeToFont.Count - 1];
    }

    private void ClearFonts()
    {
        var oldFonts = GetComponents<UIFont>();

        if (oldFonts != null)
        {
            // Destroy all but the first one
            for (int i = 0; i < oldFonts.Count(); i++)
            {
#if UNITY_EDITOR
                DestroyImmediate(oldFonts[i]);
#else
                Destroy(oldFonts[i]);
#endif
            }
        }
    }
}
