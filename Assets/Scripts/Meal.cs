using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace FoodRoulette
{
    public class Meal : CategoryIngredientMealUnitBase
    {
        #region EventArgs

        public class IngredientEventArgs : EventArgs
        {
            public IngredientOfMeal IngredientOfMeal;
        }

        #endregion // EventArgs

        private readonly Dictionary<Guid, IngredientOfMeal> _ingredients;
       
        public int IngredientsCount { get { return _ingredients.Count; } }

        private string _recipe;
        public string Recipe
        {
            get { return _recipe; }
            set
            {
                var temp = _recipe;

                _recipe = value;

                if (temp != _recipe && RecipeChangedEvent != null)
                {
                    Logger.Log(new EventFiringLog("Meal - Recipe(Set) - RecipeChangedEvent - Recipe: " + _recipe));
                    RecipeChangedEvent(_recipe);
                }
            }
        }

        public event EventHandler<IngredientEventArgs> IngredientAddedEvent;
        public event EventHandler<IngredientEventArgs> IngredientRemovedEvent;
        public event EventHandler<IngredientEventArgs> IngredientAmountChangedEvent;
        public event EventHandler<IngredientEventArgs> IngredientUnitChangedEvent;
        public event Action<string> RecipeChangedEvent;


        public Meal(Guid guid, string name) : base(guid, name)
        {
            _ingredients = new Dictionary<Guid, IngredientOfMeal>();
        }

        public bool AddIngredientWithoutEvent(Guid ingredientGuid, float amount, Guid unitGuid)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");
            if (float.IsInfinity(amount) || float.IsNaN(amount)) throw new ArgumentOutOfRangeException("amount");
            if (unitGuid == Guid.Empty) throw new ArgumentNullException("unitGuid");

            if (ContainsIngredient(ingredientGuid)) return false;

            var ingredientOfMeal = new IngredientOfMeal{IngredientGuid = ingredientGuid, Amount = amount, UnitGuid = unitGuid};
            _ingredients.Add(ingredientGuid, ingredientOfMeal);

            ingredientOfMeal.AmountChangedEvent -= OnIngredientOfMealAmountChanged;
            ingredientOfMeal.AmountChangedEvent += OnIngredientOfMealAmountChanged;
            ingredientOfMeal.UnitChangedEvent -= OnIngredientOfMealUnitChanged;
            ingredientOfMeal.UnitChangedEvent += OnIngredientOfMealUnitChanged;

            return true;
        }

        public bool AddIngredient(Guid ingredientGuid, float amount, Guid unitGuid)
        {
            if (AddIngredientWithoutEvent(ingredientGuid, amount, unitGuid))
            {
                if (IngredientAddedEvent != null)
                {
                    var ingredientOfMeal = new IngredientOfMeal { IngredientGuid = ingredientGuid, Amount = amount, UnitGuid = unitGuid };
                    Logger.Log(new EventFiringLog("Meal - AddIngredient - IngredientAddedEvent - IngredientOfMeal: " + ingredientOfMeal));
                    IngredientAddedEvent(this, new IngredientEventArgs { IngredientOfMeal = ingredientOfMeal });
                }
                return true;
            }

            return false;
        }

        public bool RemoveIngredient(Guid ingredientGuid)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");
            if (!ContainsIngredient(ingredientGuid)) return false;

            var ingredientOfMeal = _ingredients[ingredientGuid];

            ingredientOfMeal.AmountChangedEvent -= OnIngredientOfMealAmountChanged;
            ingredientOfMeal.UnitChangedEvent -= OnIngredientOfMealUnitChanged;

            if (IngredientRemovedEvent != null)
            {
                Logger.Log(new EventFiringLog("Meal - RemoveIngredient - IngredientRemovedEvent - IngredientOfMeal: " + ingredientOfMeal));
                IngredientRemovedEvent(this, new IngredientEventArgs { IngredientOfMeal = ingredientOfMeal });
            }

            _ingredients.Remove(ingredientGuid);

            return true;
        }

        public void ChangeIngredientAmount(Guid ingredientGuid, float newAmount)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");
            if (float.IsInfinity(newAmount) || float.IsNaN(newAmount)) throw new ArgumentOutOfRangeException("newAmount");

            if (!ContainsIngredient(ingredientGuid)) return;

            _ingredients[ingredientGuid].Amount = newAmount;
        }

        public void ChangeIngredientUnit(Guid ingredientGuid, Guid newUnitGuid)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");
            if (newUnitGuid == Guid.Empty) throw new ArgumentNullException("newUnitGuid");

            if (!ContainsIngredient(ingredientGuid)) return;

            _ingredients[ingredientGuid].UnitGuid = newUnitGuid;
        }

        public float GetIngredientAmount(Guid ingredientGuid)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");

            return ContainsIngredient(ingredientGuid) ? _ingredients[ingredientGuid].Amount : 0;
        }

        public Guid GetIngredientUnitGuid(Guid ingredientGuid)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");

            return ContainsIngredient(ingredientGuid) ? _ingredients[ingredientGuid].UnitGuid : Guid.Empty;
        }

        public bool ContainsIngredient(Guid ingredientGuid)
        {
            if (ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");

            return _ingredients.ContainsKey(ingredientGuid);
        }

        public IngredientOfMeal GetIngredientOfMeal(Guid ingredientGuid)
        {
            if(ingredientGuid == Guid.Empty) throw new ArgumentNullException("ingredientGuid");
            if (!ContainsIngredient(ingredientGuid)) return null;

            return _ingredients[ingredientGuid];
        }

        public ReadOnlyCollection<Guid> GetReadOnlyIngredientGuids()
        {
            return _ingredients.Keys.ToList().AsReadOnly();
        }

        public ReadOnlyCollection<float> GetReadOnlyIngredientAmounts()
        {
            return _ingredients.Values.Select(ingredientOfMeal => ingredientOfMeal.Amount).ToList().AsReadOnly();
        }

        public ReadOnlyCollection<Guid> GetReadOnlyIngredientUnitGuids()
        {
            return _ingredients.Values.Select(ingredientOfMeal => ingredientOfMeal.UnitGuid).ToList().AsReadOnly();
        }

        public ReadOnlyCollection<IngredientOfMeal> GetReadOnlyIngredientsOfMeal()
        {
            return _ingredients.Values.ToList().AsReadOnly();
        }

        private void OnIngredientOfMealAmountChanged(object sender, AmountChangedEventArgs args)
        {
            var ingredientOfMeal = sender as IngredientOfMeal;

            if (ingredientOfMeal != null && (!ContainsIngredient(ingredientOfMeal.IngredientGuid) || IngredientAmountChangedEvent == null)) return;

            Logger.Log(new EventFiringLog("Meal - OnIngredientOfMealAmountChanged - IngredientAmountChangedEvent - IngredientOfMeal: " + ingredientOfMeal));
            IngredientAmountChangedEvent(this, new IngredientEventArgs { IngredientOfMeal = ingredientOfMeal });
        }

        private void OnIngredientOfMealUnitChanged(object sender, UnitChangedEventArgs args)
        {
            var ingredientOfMeal = sender as IngredientOfMeal;

            if (ingredientOfMeal != null && (!ContainsIngredient(ingredientOfMeal.IngredientGuid) || IngredientUnitChangedEvent == null)) return;

            Logger.Log(new EventFiringLog("Meal - OnIngredientOfMealUnitChanged - IngredientUnitChangedEvent - IngredientOfMeal: " + ingredientOfMeal));
            IngredientUnitChangedEvent(this, new IngredientEventArgs { IngredientOfMeal = ingredientOfMeal });
        }
    }
}