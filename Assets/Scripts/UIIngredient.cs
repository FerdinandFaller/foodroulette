using System;
using System.Globalization;
using FoodRoulette;
using FoodRoulette.Model;
using UnityEngine;


public class UIIngredient : UICategoryIngredientMealUnitBase<Ingredient>
{
    private IFoodRouletteModel _model;
    public IFoodRouletteModel Model
    {
        get { return _model; }
        set
        {
            _model = value;

            RefreshLabelText();
        }
    }

    private Meal _mealIBelongTo;
    public Meal MealIBelongTo
    {
        get { return _mealIBelongTo; }
        set
        {
            var temp = _mealIBelongTo;
            if (temp == value) return; // Not a new Meal
            _mealIBelongTo = value;

            RefreshLabelText();

            if (_mealIBelongTo == null) return;

            _mealIBelongTo.IngredientAmountChangedEvent -= OnMealIngredientAmountChanged;
            _mealIBelongTo.IngredientAmountChangedEvent += OnMealIngredientAmountChanged;
            Logger.Log(new EventSubscriptionLog("UIIngredient - MealIBelongTo(Set) - _mealIBelongTo.Meal.IngredientAmountChangedEvent += OnMealIngredientAmountChanged"));

            _mealIBelongTo.IngredientUnitChangedEvent -= OnMealIngredientUnitChanged;
            _mealIBelongTo.IngredientUnitChangedEvent += OnMealIngredientUnitChanged;
            Logger.Log(new EventSubscriptionLog("UIIngredient - MealIBelongTo(Set) - _mealIBelongTo.IngredientUnitChangedEvent += OnMealIngredientUnitChanged"));

            if (MealIBelongToChangedEvent != null)
            {
                Logger.Log(new EventFiringLog("UIIngredient - MealIBelongTo(Set) - MealIBelongToChangedEvent - _mealIBelongTo: " + value));
                MealIBelongToChangedEvent(value);
            }
        }
    }

    public event Action<Meal> MealIBelongToChangedEvent;


    protected override void OnContentNameChanged(object sender, NameChangedEventArgs args)
    {
        base.OnContentNameChanged(sender, args);

        RefreshLabelText();
    }

    private void OnMealIngredientAmountChanged(object sender, Meal.IngredientEventArgs args)
    {
        Logger.Log(new EventReceivingLog("UIIngredient - OnMealIngredientAmountChanged - Ingredient: " + args.IngredientOfMeal.IngredientGuid + ", Amount: " + args.IngredientOfMeal.Amount));
        if(args.IngredientOfMeal.IngredientGuid != Content.Guid) return;
        RefreshLabelText();
    }

    private void OnMealIngredientUnitChanged(object sender, Meal.IngredientEventArgs args)
    {
        Logger.Log(new EventReceivingLog("UIIngredient - OnMealIngredientUnitChanged - Ingredient: " + args.IngredientOfMeal.IngredientGuid + ", Unit: " + args.IngredientOfMeal.UnitGuid));
        if (args.IngredientOfMeal.IngredientGuid != Content.Guid) return;
        RefreshLabelText();
    }

    public override void RefreshLabelText()
    {
        if (Model == null)
        {
            base.RefreshLabelText();
            return;
        }

        if (NameLabel == null) return;

        if (MealIBelongTo != null && MealIBelongTo.ContainsIngredient(Content.Guid))
        {
            Unit unit;
            _model.GetUnitOfIngredientInMeal(out unit, MealIBelongTo, Content);

            if (unit == null)
            {
                NameLabel.text = Content.Name;
                return;
            }

            var amount = MealIBelongTo.GetIngredientAmount(Content.Guid);
            if (amount > -0.00001f && amount < 0.00001f)
            {
                NameLabel.text = Content.Name + "(" + unit.UnitOfMeasurement + ")";
            }
            else
            {
                NameLabel.text = amount + unit.UnitOfMeasurement + " " + Content.Name;
            }
        }
        else
        {
            NameLabel.text = Content.Name;
        }
    }
}
