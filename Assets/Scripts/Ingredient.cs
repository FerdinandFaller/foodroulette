using System;

namespace FoodRoulette
{
    public class Ingredient : CategoryIngredientMealUnitBase
    {
        public Ingredient(Guid guid, string name) : base(guid, name)
        {
        }

        public override string ToString()
        {
            return base.ToString() + "(" + Guid + ")";
        }
    }
}