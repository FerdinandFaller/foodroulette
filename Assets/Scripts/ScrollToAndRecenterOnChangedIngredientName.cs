using System;
using FoodRoulette;
using UnityEngine;
using System.Collections;


public class ScrollToAndRecenterOnChangedIngredientName : MonoBehaviour
{
    private UIDraggablePanel _draggablePanel;
    private Vector3 _originalPanelPos;
    public UIIngredientsList UiIngredientsList;


    void Awake()
    {
        UiIngredientsList.AddedContentEvent += OnIngredientsListAddedUiIngredient;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedIngredientName - Awake - UiIngredientsList.AddedUiIngredientEvent += OnIngredientsListAddedUiIngredient"));
        UiIngredientsList.RemovedContentEvent += OnIngredientsListRemovedUiIngredient;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedIngredientName - Awake - UiIngredientsList.RemovedUiIngredientEvent += OnIngredientsListRemovedUiIngredient"));

        // Get the DraggablePanel via the IngredientsList
        _draggablePanel = NGUITools.FindInParents<UIDraggablePanel>(UiIngredientsList.Grid.gameObject);
        _originalPanelPos = _draggablePanel.transform.position;
    }

    private void OnIngredientsListAddedUiIngredient(UIIngredient addedUiIngredient)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedIngredientName - OnIngredientsListAddedUiIngredient - addedUiIngredient: " + addedUiIngredient));

        addedUiIngredient.Content.NameChangedEvent += OnIngredientNameChanged;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedIngredientName - OnIngredientsListAddedUiIngredient - addedUiIngredient.Ingredient.NameChangedEvent += OnIngredientNameChanged"));
    }

    private void OnIngredientsListRemovedUiIngredient(UIIngredient removedUiIngredient)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedIngredientName - OnIngredientsListRemovedUiIngredient - removedUiIngredient: " + removedUiIngredient));

        removedUiIngredient.Content.NameChangedEvent -= OnIngredientNameChanged;
        Logger.Log(new EventUnSubscriptionLog("ScrollToAndRecenterOnChangedIngredientName - OnIngredientsListAddedUiIngredient - removedUiIngredient.Ingredient.NameChangedEvent -= OnIngredientNameChanged"));
    }

    private void OnIngredientNameChanged(object sender, NameChangedEventArgs args)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedIngredientName - OnIngredientNameChanged - args.NewName: " + args.NewName));

        UiIngredientsList.StartWaitCoroutine();

        var ingredient = (Ingredient) sender;

        if (!UiIngredientsList.InnerContents.Contains(ingredient)) return;

        foreach (var uiIngredient in UiIngredientsList.Contents)
        {
            if (uiIngredient.Content != ingredient) continue;

            StartCoroutine_Auto(ScrollAndRecenterOnUiIngredientCoRoutine(uiIngredient));
            break;
        }
    }

    private IEnumerator ScrollAndRecenterOnUiIngredientCoRoutine(UIIngredient uiIngredient)
    {
        if (uiIngredient == null) throw new ArgumentNullException("uiIngredient");

        // Wait a bit. The Grid needs a short moment to resort and reposition the added ingredient
        yield return new WaitForSeconds(0.001f);

        // Calculate the Position of the added ingredient
        var pos = uiIngredient.transform.worldToLocalMatrix.MultiplyPoint3x4(_draggablePanel.transform.position);
        pos.z = _originalPanelPos.z;
        pos.x = _originalPanelPos.x;

        // Use a SpringPanel to scroll the DraggablePanel to the added UIIngredient
        var spring = SpringPanel.Begin(_draggablePanel.gameObject, pos, 100);
        spring.onFinished += OnSpringPanelScrollFinished;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedIngredientName - ScrollAndRecenterOnUiIngredientCoRoutine"));
    }

    private void OnSpringPanelScrollFinished()
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedIngredientName - OnSpringPanelScrollFinished"));

        // Force a Recenter by Starting the WaitCoRoutine of the IngredientsList manually
        UiIngredientsList.StartWaitCoroutine();
    }
}
