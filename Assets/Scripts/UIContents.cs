using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class UIContents : MonoBehaviour
{
    [HideInInspector] public Dictionary<string, GameObject> ContentDict;

    public List<string> ContentNames;
    public List<GameObject> ContentGameObjects;


    public void Initialize()
    {
        ContentDict = new Dictionary<string, GameObject>();

        for (var i = 0; i < ContentNames.Count; i++)
        {
            ContentDict.Add(ContentNames[i], ContentGameObjects[i]);
        }
    }
}
