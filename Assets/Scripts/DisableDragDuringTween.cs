using FoodRoulette;
using UnityEngine;


public class DisableDragDuringTween : MonoBehaviour
{
    public UIDraggablePanel DraggablePanel;


    public void OnClick()
    {
        Logger.Log(new EventReceivingLog("UIDisableDragDuringTween - OnClick"));
        DraggablePanel.enabled = false;
    }

    public void OnTweenFinished()
    {
        Logger.Log(new EventReceivingLog("UIDisableDragDuringTween - OnTweenFinished"));
        DraggablePanel.enabled = true;
    }
}
