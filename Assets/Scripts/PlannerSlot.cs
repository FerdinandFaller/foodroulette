using System;
using FoodRoulette;
using UnityEngine;


public class PlannerSlot : MonoBehaviour
{
    public UIMeal UiMeal;
    public UILabel EmptyMessageLabel;
    public UILabel WeekdayLabel;
    public UIButton PickMealButton;
    public UIButton RandomizeMealButton;
    public int Index;


    void Awake()
    {
        UiMeal.ContentChangedEvent -= OnUiMealContentChanged;
        UiMeal.ContentChangedEvent += OnUiMealContentChanged;

        SetWeekdayLabel();
    }

    void Start()
    {
        ShowHideEmptyLabel(UiMeal.Content == null);
        ShowHideMealLabel(UiMeal.Content != null);
    }

    private void SetWeekdayLabel()
    {
        if(Index < 0 || Index > 6) throw new ArgumentOutOfRangeException();
        if(WeekdayLabel == null) throw new NullReferenceException("WekdayLabel == null");

        switch (Index)
        {
            case 0:
                {
                    WeekdayLabel.text = "Monday";
                } break;

            case 1:
                {
                    WeekdayLabel.text = "Tuesday";
                } break;

            case 2:
                {
                    WeekdayLabel.text = "Wednesday";
                } break;

            case 3:
                {
                    WeekdayLabel.text = "Thursday";
                } break;

            case 4:
                {
                    WeekdayLabel.text = "Friday";
                } break;

            case 5:
                {
                    WeekdayLabel.text = "Saturday";
                } break;
            case 6:
                {
                    WeekdayLabel.text = "Sunday";
                } break;
        }
    }

    private void OnUiMealContentChanged(Meal oldMeal, Meal newMeal)
    {
        ShowHideEmptyLabel(newMeal == null);
        ShowHideMealLabel(newMeal != null);
    }

    private void ShowHideEmptyLabel(bool shouldShow)
    {
        EmptyMessageLabel.alpha = shouldShow ? 1 : 0;
        EmptyMessageLabel.enabled = shouldShow;
    }

    private void ShowHideMealLabel(bool shouldShow)
    {
        UiMeal.NameLabel.alpha = shouldShow ? 1 : 0;
        UiMeal.NameLabel.enabled = shouldShow;
    }
}
