﻿using UnityEngine;


[ExecuteInEditMode]
public class UIBoxColliderOffsetTransform : MonoBehaviour
{
    public Vector3 RelativeOffset = Vector3.zero;
    public Vector3 AbsoluteOffset = Vector3.zero;

    private BoxCollider _boxCollider;


    void Start()
    {
        _boxCollider = (BoxCollider) collider;
    }

    void Update()
    {
        var offset = _boxCollider.center;

        offset.x = offset.x * RelativeOffset.x;
        offset.y = offset.y * RelativeOffset.y;
        offset.z = offset.z * RelativeOffset.z;

        if (offset.x < 0.0001f && offset.x > -0.0001f) offset.x = _boxCollider.size.x * RelativeOffset.x;
        if (offset.y < 0.0001f && offset.y > -0.0001f) offset.y = _boxCollider.size.y * RelativeOffset.y;
        if (offset.z < 0.0001f && offset.z > -0.0001f) offset.z = _boxCollider.size.z * RelativeOffset.z;

        offset += AbsoluteOffset;

        if(transform.position != offset) transform.localPosition = offset;
    }
}
