using System;
using FoodRoulette;
using UnityEngine;
using System.Collections;


public class ScrollToAndRecenterOnChangedMealName : MonoBehaviour
{
    private UIDraggablePanel _draggablePanel;
    private Vector3 _originalPanelPos;
    public UIMealsList UiMealsList;


    void Awake()
    {
        UiMealsList.AddedContentEvent += OnMealsListAddedUiMeal;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedMealName - Awake - UiMealsList.AddedUiMealEvent += OnMealsListAddedUiMeal"));
        UiMealsList.RemovedContentEvent += OnMealsListRemovedUiMeal;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedMealName - Awake - UiMealsList.RemovedUiMealEvent += OnMealsListRemovedUiMeal"));

        // Get the DraggablePanel via the IngredientsList
        _draggablePanel = NGUITools.FindInParents<UIDraggablePanel>(UiMealsList.Grid.gameObject);
        _originalPanelPos = _draggablePanel.transform.position;
    }

    private void OnMealsListAddedUiMeal(UIMeal addedUiMeal)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedMealName - OnMealsListAddedUiMeal - addedUiMeal: " + addedUiMeal));

        addedUiMeal.Content.NameChangedEvent += OnMealNameChanged;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedMealName - OnMealsListAddedUiMeal - addedUiMeal.Meal.NameChangedEvent += OnMealNameChanged"));
    }

    private void OnMealsListRemovedUiMeal(UIMeal removedUiMeal)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedMealName - OnMealsListRemovedUiMeal - removedUiMeal: " + removedUiMeal));

        removedUiMeal.Content.NameChangedEvent -= OnMealNameChanged;
        Logger.Log(new EventUnSubscriptionLog("ScrollToAndRecenterOnChangedMealName - OnMealsListAddedUiMeal - removedUiMeal.Meal.NameChangedEvent -= OnMealNameChanged"));
    }

    private void OnMealNameChanged(object sender, NameChangedEventArgs args)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedMealName - OnMealNameChanged - args.NewName: " + args.NewName));

        UiMealsList.StartWaitCoroutine();

        var meal = (Meal) sender;

        if (!UiMealsList.InnerContents.Contains(meal)) return;

        foreach (var uiMeal in UiMealsList.Contents)
        {
            if (uiMeal.Content != meal) continue;

            StartCoroutine_Auto(ScrollAndRecenterOnUiMealCoRoutine(uiMeal));
            break;
        }
    }

    private IEnumerator ScrollAndRecenterOnUiMealCoRoutine(UIMeal uiMeal)
    {
        if (uiMeal == null) throw new ArgumentNullException("uiMeal");

        // Wait a bit. The Grid needs a short moment to resort and reposition the added meal
        yield return new WaitForSeconds(0.001f);

        // Calculate the Position of the added ingredient
        var pos = uiMeal.transform.worldToLocalMatrix.MultiplyPoint3x4(_draggablePanel.transform.position);
        pos.z = _originalPanelPos.z;
        pos.x = _originalPanelPos.x;

        // Use a SpringPanel to scroll the DraggablePanel to the added UIIngredient
        var spring = SpringPanel.Begin(_draggablePanel.gameObject, pos, 100);
        spring.onFinished += OnSpringPanelScrollFinished;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnChangedMealName - ScrollAndRecenterOnUiMealCoRoutine"));
    }

    private void OnSpringPanelScrollFinished()
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnChangedMealName - OnSpringPanelScrollFinished"));

        // Force a Recenter by Starting the WaitCoRoutine of the IngredientsList manually
        UiMealsList.StartWaitCoroutine();
    }
}
