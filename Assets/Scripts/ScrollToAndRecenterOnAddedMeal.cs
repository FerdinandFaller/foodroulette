using System;
using FoodRoulette;
using UnityEngine;
using System.Collections;


public class ScrollToAndRecenterOnAddedMeal : MonoBehaviour
{
    private UIDraggablePanel _draggablePanel;
    private Vector3 _originalPanelPos;
    public UIMealsList UiMealsList;


    void Awake()
    {
        UiMealsList.AddedContentEvent += OnMealsListAddedUiMeal;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnAddedMeal - Awake - UiMealsList.AddedUiMealEvent += OnMealsListAddedUiMeal"));

        // Get the DraggablePanel via the IngredientsList
        _draggablePanel = NGUITools.FindInParents<UIDraggablePanel>(UiMealsList.Grid.gameObject);
        _originalPanelPos = _draggablePanel.transform.position;
    }

    private void OnMealsListAddedUiMeal(UIMeal addedUiMeal)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnAddedMeal - OnMealsListAddedUiMeal - addedUiMeal: " + addedUiMeal));

        StartCoroutine_Auto(ScrollAndRecenterOnUiMealCoRoutine(addedUiMeal));
    }

    private IEnumerator ScrollAndRecenterOnUiMealCoRoutine(UIMeal uiMeal)
    {
        if (uiMeal == null) throw new ArgumentNullException("uiMeal");

        // Wait a bit. The Grid needs a short moment to resort and reposition the added meal
        yield return new WaitForSeconds(0.001f);

        // Calculate the Position of the added Meal
        var pos = uiMeal.transform.worldToLocalMatrix.MultiplyPoint3x4(_draggablePanel.transform.position);
        pos.z = _originalPanelPos.z;
        pos.x = _originalPanelPos.x;

        // Use a SpringPanel to scroll the DraggablePanel to the added UIMeal
        var spring = SpringPanel.Begin(_draggablePanel.gameObject, pos, 100);
        spring.onFinished += OnSpringPanelScrollFinished;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnAddedMeal - ScrollAndRecenterOnUiMealCoRoutine"));
    }

    private void OnSpringPanelScrollFinished()
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnAddedMeal - OnSpringPanelScrollFinished"));

        // Force a Recenter by Starting the WaitCoRoutine of the MealsList manually
        UiMealsList.StartWaitCoroutine();
    }
}
