using System;

namespace FoodRoulette
{
    public class IngredientChangedEventArgs : EventArgs
    {
        public Guid OldIngredient;
        public Guid NewIngredient;
    }
}