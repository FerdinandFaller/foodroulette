using System;
using System.Collections;
using System.Collections.Generic;
using FoodRoulette;
using UnityEngine;

public abstract class FoodRouletteData : IFoodRouletteData
{
    public Dictionary<Guid, Meal> Meals { get; set; }
    public Dictionary<Guid, Ingredient> Ingredients { get; set; }
    public Dictionary<Guid, Unit> Units { get; set; }
    public Dictionary<Guid, Category> Categories { get; set; }
    public Meal[] PlannerMeals { get; set; }
    public Version DataVersion { get; private set; }


    protected FoodRouletteData()
    {
        Categories = new Dictionary<Guid, Category>();
        Meals = new Dictionary<Guid, Meal>();
        Ingredients = new Dictionary<Guid, Ingredient>();
        Units = new Dictionary<Guid, Unit>();
        PlannerMeals = new Meal[7];
    }

    public virtual void CheckAndUpdate(Version appVersion)
    {
        DataVersion = ReadDataVersion();

        Debug.Log("FoodRouletteData - CheckAndUpdate. AppVersion: " + appVersion + ", DataVersion: " + DataVersion);

        if (appVersion > DataVersion)
        {
            UpdateData(appVersion);

            DataVersion = appVersion;
            WriteDataVersion();
        }
    }

    public abstract void WriteAllCategories();
    public abstract void ReadAllCategories();
    public abstract void WriteAllMeals();
    public abstract void ReadAllMeals();
    public abstract void WriteAllIngredients();
    public abstract void ReadAllIngredients();
    public abstract void WriteAllUnits();
    public abstract void ReadAllUnits();
    public abstract Category ReadCategory(Guid guid);
    public abstract Meal ReadMeal(Guid guid);
    public abstract Ingredient ReadIngredient(Guid guid);
    public abstract Unit ReadUnit(Guid guid);
    public abstract void Write(Category category);
    public abstract void Write(Meal meal);
    public abstract void Write(Ingredient ingredient);
    public abstract void Write(Unit unit);
    public abstract void WriteAllPlannerMeals();
    public abstract void ReadAllPlannerMeals();
    public abstract void Delete(Category category);
    public abstract void Delete(Meal meal);
    public abstract void Delete(Ingredient ingredient);
    public abstract void Delete(Unit unit);
    protected abstract Version ReadDataVersion();
    protected abstract void WriteDataVersion();
    protected abstract void UpdateData(Version appVersion);
    protected abstract void AddDefaultData();
}