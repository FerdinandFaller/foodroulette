using System;
using System.Collections.Generic;
using FF.JSONPrefs;
using FoodRoulette;

class JSONPrefsFoodRouletteData : FoodRouletteData
{
    private readonly FoodRouletteModel _model;


    public JSONPrefsFoodRouletteData(FoodRouletteModel model)
    {
        _model = model;

        //JSONPrefs.DeleteAll();
    }

    public override void WriteAllCategories()
    {
        foreach (KeyValuePair<Guid, Category> keyValuePair in Categories)
        {
            Write(keyValuePair.Value);
        }
    }

    public override void ReadAllCategories()
    {
        var categoriesCount = JSONPrefs.GetInt("FoodRouletteData - CategoriesCount: ");

        for (var index = 0; index < categoriesCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Category #" + index));

            Categories.Add(guid, ReadCategory(guid));
        }
    }

    public override void WriteAllMeals()
    {
        foreach (KeyValuePair<Guid, Meal> keyValuePair in Meals)
        {
            Write(keyValuePair.Value);
        }
    }

    public override void ReadAllMeals()
    {
        var mealsCount = JSONPrefs.GetInt("FoodRouletteData - MealsCount: ");

        for (var index = 0; index < mealsCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Meal #" + index));

            Meals.Add(guid, ReadMeal(guid));
        }
    }

    public override void WriteAllIngredients()
    {
        foreach (KeyValuePair<Guid, Ingredient> keyValuePair in Ingredients)
        {
            Write(keyValuePair.Value);
        }
    }

    public override void ReadAllIngredients()
    {
        var ingredientsCount = JSONPrefs.GetInt("FoodRouletteData - IngredientsCount: ");

        for (var index = 0; index < ingredientsCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Ingredient #" + index));
            Ingredients.Add(guid, ReadIngredient(guid));
        }
    }

    public override void WriteAllUnits()
    {
        foreach (KeyValuePair<Guid, Unit> keyValuePair in Units)
        {
            Write(keyValuePair.Value);
        }
    }

    public override void ReadAllUnits()
    {
        var unitsCount = JSONPrefs.GetInt("FoodRouletteData - UnitsCount: ");

        for (var index = 0; index < unitsCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Unit #" + index));
            Units.Add(guid, ReadUnit(guid));
        }
    }

    public override Category ReadCategory(Guid guid)
    {
        if (guid == Guid.Empty) throw new ArgumentException("Guid is empty, can't Deserialize", "guid");
        if (!JSONPrefs.HasKey("Category (" + guid + ") - MealsCount: ")) throw new Exception("Could not find Category MealsCount in JSONPrefs. Guid: " + guid);
        if (!JSONPrefs.HasKey("Category (" + guid + ") - Name: ")) throw new Exception("Could not find Category Name in JSONPrefs. Guid: " + guid);

        var mealsCount = JSONPrefs.GetInt("Category (" + guid + ") - MealsCount: ");
        var name = JSONPrefs.GetString("Category (" + guid + ") - Name: ");

        var category = new Category(guid, name);

        for (var index = 0; index < mealsCount; index++)
        {
            if (!JSONPrefs.HasKey("Category (" + guid + ") - Guid of Meal #" + index)) throw new Exception("Could not find MealGuid in JSONPrefs. guid: " + guid + " index: " + index);

            var g = new Guid(JSONPrefs.GetString("Category (" + guid + ") - Guid of Meal #" + index));

            category.AddMealWithoutEvent(g);
        }

        return category;
    }

    public override Meal ReadMeal(Guid guid)
    {
        if (guid == Guid.Empty) throw new ArgumentException("Guid is empty, can't Deserialize", "guid");
        if (!JSONPrefs.HasKey("Meal (" + guid + ") - IngredientsCount: ")) throw new Exception("Could not find Meal IngredientsCount in JSONPrefs. mealGuid: " + guid);
        if (!JSONPrefs.HasKey("Meal (" + guid + ") - Name: ")) throw new Exception("Could not find Meal Name in JSONPrefs. mealGuid: " + guid);
        if (!JSONPrefs.HasKey("Meal (" + guid + ") - Recipe: ")) throw new Exception("Could not find Meal Recipe in JSONPrefs. mealGuid: " + guid);

        var ingredientsCount = JSONPrefs.GetInt("Meal (" + guid + ") - IngredientsCount: ");
        var name = JSONPrefs.GetString("Meal (" + guid + ") - Name: ");

        var meal = new Meal(guid, name) {Recipe = JSONPrefs.GetString("Meal (" + guid + ") - Recipe: ")};

        for (var index = 0; index < ingredientsCount; index++)
        {
            if (!JSONPrefs.HasKey("Meal (" + guid + ") - Guid of Ingredient #" + index)) throw new Exception("Could not find IngredientGuid in JSONPrefs. mealGuid: " + guid + " index: " + index);
            if (!JSONPrefs.HasKey("Meal (" + guid + ") - Amount of Ingredient #" + index)) throw new Exception("Could not find Ingredient Amount in JSONPrefs. mealGuid: " + guid + " index: " + index);
            if (!JSONPrefs.HasKey("Meal (" + guid + ") - Guid of Unit of Ingredient #" + index)) throw new Exception("Could not find Ingredient Unit in JSONPrefs. mealGuid: " + guid + " index: " + index);

            var g = new Guid(JSONPrefs.GetString("Meal (" + guid + ") - Guid of Ingredient #" + index));
            var a = JSONPrefs.GetFloat("Meal (" + guid + ") - Amount of Ingredient #" + index);
            var u = new Guid(JSONPrefs.GetString("Meal (" + guid + ") - Guid of Unit of Ingredient #" + index));

            meal.AddIngredientWithoutEvent(g, a, u);
        }

        return meal;
    }

    public override Ingredient ReadIngredient(Guid guid)
    {
        if (guid == Guid.Empty) throw new ArgumentException("Guid is empty, can't Deserialize", "guid");
        if (!JSONPrefs.HasKey("Ingredient (" + guid + ") - Name: ")) throw new Exception("Could not find Ingredient Name in JSONPrefs. Guid: " + guid);

        var name = JSONPrefs.GetString("Ingredient (" + guid + ") - Name: ");

        var ingredient = new Ingredient(guid, name);

        return ingredient;
    }

    public override Unit ReadUnit(Guid guid)
    {
        if (guid == Guid.Empty) throw new ArgumentException("Guid is empty, can't Deserialize", "guid");
        if (!JSONPrefs.HasKey("Unit (" + guid + ") - Singular: ")) throw new Exception("Could not find Unit Singular in JSONPrefs. Guid: " + guid);
        if (!JSONPrefs.HasKey("Unit (" + guid + ") - Plural: ")) throw new Exception("Could not find Unit Plural in JSONPrefs. Guid: " + guid);
        if (!JSONPrefs.HasKey("Unit (" + guid + ") - UnitOfMeasurement: ")) throw new Exception("Could not find Unit UnitOfMeasurement in JSONPrefs. Guid: " + guid);

        var singular = JSONPrefs.GetString("Unit (" + guid + ") - Singular: ");
        var plural = JSONPrefs.GetString("Unit (" + guid + ") - Plural: ");
        var unitOfMeasurement = JSONPrefs.GetString("Unit (" + guid + ") - UnitOfMeasurement: ");

        var unit = new Unit(guid, singular, plural, unitOfMeasurement);

        return unit;
    }

    public override void Write(Category category)
    {
        var meals = category.GetReadOnlyMealGuids();

        JSONPrefs.SetString("Category (" + category.Guid + ") - Name: ", category.Name);
        JSONPrefs.SetInt("Category (" + category.Guid + ") - MealsCount: ", meals.Count);

        for (var index = 0; index < meals.Count; index++)
        {
            JSONPrefs.SetString("Category (" + category.Guid + ") - Guid of Meal #" + index, meals[index].ToString());
        }

        int i = 0;
        foreach (KeyValuePair<Guid, Category> keyValuePair in Categories)
        {
            if(keyValuePair.Key == category.Guid)
            {
                JSONPrefs.SetString("FoodRouletteData - Guid of Category #" + i, keyValuePair.Value.Guid.ToString());
            }

            i++;
        }

        JSONPrefs.SetInt("FoodRouletteData - CategoriesCount: ", Categories.Count);
    }

    public override void Write(Meal meal)
    {
        var ingredientGuids = meal.GetReadOnlyIngredientGuids();
        var ingredientAmounts = meal.GetReadOnlyIngredientAmounts();
        var ingredientUnitGuids = meal.GetReadOnlyIngredientUnitGuids();

        JSONPrefs.SetString("Meal (" + meal.Guid + ") - Name: ", meal.Name);
        JSONPrefs.SetString("Meal (" + meal.Guid + ") - Recipe: ", meal.Recipe);
        JSONPrefs.SetInt("Meal (" + meal.Guid + ") - IngredientsCount: ", ingredientGuids.Count);

        for (var index = 0; index < ingredientGuids.Count; index++)
        {
            JSONPrefs.SetString("Meal (" + meal.Guid + ") - Guid of Ingredient #" + index, ingredientGuids[index].ToString());
            JSONPrefs.SetFloat("Meal (" + meal.Guid + ") - Amount of Ingredient #" + index, ingredientAmounts[index]);
            JSONPrefs.SetString("Meal (" + meal.Guid + ") - Guid of Unit of Ingredient #" + index, ingredientUnitGuids[index].ToString());
        }

        int i = 0;
        foreach (KeyValuePair<Guid, Meal> keyValuePair in Meals)
        {
            if (keyValuePair.Key == meal.Guid)
            {
                JSONPrefs.SetString("FoodRouletteData - Guid of Meal #" + i, keyValuePair.Value.Guid.ToString());
            }

            i++;
        }

        JSONPrefs.SetInt("FoodRouletteData - MealsCount: ", Meals.Count);
    }

    public override void Write(Ingredient ingredient)
    {
        JSONPrefs.SetString("Ingredient (" + ingredient.Guid + ") - Name: ", ingredient.Name);

        int i = 0;
        foreach (KeyValuePair<Guid, Ingredient> keyValuePair in Ingredients)
        {
            if (keyValuePair.Key == ingredient.Guid)
            {
                JSONPrefs.SetString("FoodRouletteData - Guid of Ingredient #" + i, keyValuePair.Value.Guid.ToString());
            }

            i++;
        }

        JSONPrefs.SetInt("FoodRouletteData - IngredientsCount: ", Ingredients.Count);
    }

    public override void Write(Unit unit)
    {
        JSONPrefs.SetString("Unit (" + unit.Guid + ") - Singular: ", unit.Singular);
        JSONPrefs.SetString("Unit (" + unit.Guid + ") - Plural: ", unit.Plural);
        JSONPrefs.SetString("Unit (" + unit.Guid + ") - UnitOfMeasurement: ", unit.UnitOfMeasurement);

        int i = 0;
        foreach (KeyValuePair<Guid, Unit> keyValuePair in Units)
        {
            if (keyValuePair.Key == unit.Guid)
            {
                JSONPrefs.SetString("FoodRouletteData - Guid of Unit #" + i, keyValuePair.Value.Guid.ToString());
            }

            i++;
        }

        JSONPrefs.SetInt("FoodRouletteData - UnitsCount: ", Units.Count);
    }

    public override void WriteAllPlannerMeals()
    {
        if (PlannerMeals == null) throw new NullReferenceException("PlannerMeals is null");

        JSONPrefs.SetInt("PlannerMealsCount", PlannerMeals.Length);
        for (var i = 0; i < PlannerMeals.Length; i++)
        {
            if (PlannerMeals[i] == null) continue;
            JSONPrefs.SetString("PlannerMeal" + i, PlannerMeals[i].Guid.ToString());
        }
    }

    public override void ReadAllPlannerMeals()
    {
        PlannerMeals = new Meal[7];

        var plannerMealsCount = JSONPrefs.GetInt("PlannerMealsCount", 0);
        for (int i = 0; i < plannerMealsCount; i++)
        {
            try
            {
                var guid = new Guid(JSONPrefs.GetString("PlannerMeal" + i));
                if (!Meals.ContainsKey(guid)) continue;
                Meal meal;
                Meals.TryGetValue(guid, out meal);
                PlannerMeals[i] = meal;
            }
            catch (FormatException)
            {
            }
        }
    }

    public override void Delete(Category category)
    {
        if (!Categories.ContainsKey(category.Guid)) return;

        // If a Category is deleted, we have to reduce the index of all Categories that have a higher index than the removed one, by one
        // We have to reduce the total CategoriesCount by one

        var categoriesCount = JSONPrefs.GetInt("FoodRouletteData - CategoriesCount: ");
        var categoryFound = false;

        for (var index = 0; index < categoriesCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Category #" + index));

            if (categoryFound)
            {
                // This is a category after the one we removed, so we decrease its index by one
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Category #" + index);
                JSONPrefs.SetString("FoodRouletteData - Guid of Category #" + (index - 1), guid.ToString());
            }
            else if (guid == category.Guid)
            {
                // This is the category we want to remove
                categoryFound = true;
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Category #" + index);
            }

            // This is a category before the one we want to remove
        }

        categoriesCount--;
        JSONPrefs.SetInt("FoodRouletteData - CategoriesCount: ", categoriesCount);

        JSONPrefs.DeleteKey("Category (" + category.Guid + ") - Name: ");
        JSONPrefs.DeleteKey("Category (" + category.Guid + ") - MealsCount: ");

        for (var index = 0; index < category.GetReadOnlyMealGuids().Count; index++)
        {
            JSONPrefs.DeleteKey("Category (" + category.Guid + ") - Guid of Meal #" + index);
        }

        // Also remove the category from the Dictionary
        Categories.Remove(category.Guid);
    }

    public override void Delete(Meal meal)
    {
        if (!Meals.ContainsKey(meal.Guid)) return;

        // Delete the Meal from all Categories first
        foreach (KeyValuePair<Guid, Category> keyValuePair in Categories)
        {
            if (!keyValuePair.Value.ContainsMeal(meal)) continue;

            var mealsCountOfCategory = JSONPrefs.GetInt("Category (" + keyValuePair.Key + ") - MealsCount: ");
            JSONPrefs.SetInt("Category (" + keyValuePair.Key + ") - MealsCount: ", mealsCountOfCategory - 1);

            var mealInCategoryFound = false;

            // Go through all meals
            for (var index = 0; index < mealsCountOfCategory; index++)
            {
                // All meals before the meal we remove keep their index. 
                // The meal we remove gets removed. 
                // All meals after the meal we remove get their index reduced by one.

                var guid = new Guid(JSONPrefs.GetString("Category (" + keyValuePair.Key + ") - Guid of Meal #" + index));

                if (mealInCategoryFound)
                {
                    // This is a meal after the one we removed, so we decrease its index by one
                    JSONPrefs.DeleteKey("Category (" + keyValuePair.Key + ") - Guid of Meal #" + index);
                    JSONPrefs.SetString("Category (" + keyValuePair.Key + ") - Guid of Meal #" + (index - 1), guid.ToString());
                }
                else if (guid == meal.Guid)
                {
                    // This is the meal we want to remove
                    mealInCategoryFound = true;
                    JSONPrefs.DeleteKey("Category (" + keyValuePair.Key + ") - Guid of Meal #" + index);

                    keyValuePair.Value.RemoveMeal(meal);
                }

                // This is a meal before the one we want to remove
            }
        }

        // If a Meal is deleted, we have to reduce the index of all Meals that have a higher index than the removed one, by one
        // We have to reduce the total MealsCount by one

        var mealsCount = JSONPrefs.GetInt("FoodRouletteData - MealsCount: ");
        var mealFound = false;

        for (var index = 0; index < mealsCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Meal #" + index));

            if (mealFound)
            {
                // This is a meal after the one we removed, so we decrease its index by one
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Meal #" + index);
                JSONPrefs.SetString("FoodRouletteData - Guid of Meal #" + (index - 1), guid.ToString());
            }
            else if (guid == meal.Guid)
            {
                // This is the meal we want to remove
                mealFound = true;
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Meal #" + index);
            }

            // This is a meal before the one we want to remove
        }

        mealsCount--;
        JSONPrefs.SetInt("FoodRouletteData - MealsCount: ", mealsCount);

        JSONPrefs.DeleteKey("Meal (" + meal.Guid + ") - Name: ");
        JSONPrefs.DeleteKey("Meal (" + meal.Guid + ") - Recipe: ");
        JSONPrefs.DeleteKey("Meal (" + meal.Guid + ") - IngredientsCount: ");

        for (var index = 0; index < meal.GetReadOnlyIngredientGuids().Count; index++)
        {
            JSONPrefs.DeleteKey("Meal (" + meal.Guid + ") - Guid of Ingredient #" + index);
            JSONPrefs.DeleteKey("Meal (" + meal.Guid + ") - Amount of Ingredient #" + index);
            JSONPrefs.DeleteKey("Meal (" + meal.Guid + ") - Guid of Unit of Ingredient #" + index);
        }

        // Also remove the Meal from the Dictionary
        Meals.Remove(meal.Guid);
    }

    public override void Delete(Ingredient ingredient)
    {
        if (!Ingredients.ContainsKey(ingredient.Guid)) return;

        // Delete the Ingredient from all Meals first
        foreach (KeyValuePair<Guid, Meal> keyValuePair in Meals)
        {
            if (!keyValuePair.Value.ContainsIngredient(ingredient.Guid)) continue;

            var ingredientsCountOfMeal = JSONPrefs.GetInt("Meal (" + keyValuePair.Key + ") - IngredientsCount: ");
            JSONPrefs.SetInt("Meal (" + keyValuePair.Key + ") - IngredientsCount: ", ingredientsCountOfMeal - 1);

            var ingredientInMealFound = false;

            // Go through all Ingredients
            for (var index = 0; index < ingredientsCountOfMeal; index++)
            {
                // All Ingredients before the Ingredient we remove keep their index. 
                // The Ingredient we remove gets removed. 
                // All Ingredients after the Ingredient we remove get their index reduced by one.

                var guid = new Guid(JSONPrefs.GetString("Meal (" + keyValuePair.Key + ") - Guid of Ingredient #" + index));

                if (ingredientInMealFound)
                {
                    // This is a ingredient after the one we removed, so we decrease its index by one
                    JSONPrefs.DeleteKey("Meal (" + keyValuePair.Key + ") - Guid of Ingredient #" + index);
                    JSONPrefs.SetString("Meal (" + keyValuePair.Key + ") - Guid of Ingredient #" + (index - 1), guid.ToString());

                    var amount = JSONPrefs.GetFloat("Meal (" + keyValuePair.Key + ") - Amount of Ingredient #" + index);
                    JSONPrefs.DeleteKey("Meal (" + keyValuePair.Key + ") - Amount of Ingredient #" + index);
                    JSONPrefs.SetFloat("Meal (" + keyValuePair.Key + ") - Amount of Ingredient #" + (index - 1), amount);

                    var unitGuid = JSONPrefs.GetString("Meal (" + keyValuePair.Key + ") - Guid of Unit of Ingredient #" + index);
                    JSONPrefs.DeleteKey("Meal (" + keyValuePair.Key + ") - Guid of Unit of Ingredient #" + index);
                    JSONPrefs.SetString("Meal (" + keyValuePair.Key + ") - Guid of Unit of Ingredient #" + (index - 1), unitGuid);
                }
                else if (guid == ingredient.Guid)
                {
                    // This is the Ingredient we want to remove
                    ingredientInMealFound = true;
                    JSONPrefs.DeleteKey("Meal (" + keyValuePair.Key + ") - Guid of Ingredient #" + index);
                    JSONPrefs.DeleteKey("Meal (" + keyValuePair.Key + ") - Amount of Ingredient #" + index);
                    JSONPrefs.DeleteKey("Meal (" + keyValuePair.Key + ") - Guid of Unit of Ingredient #" + index);

                    keyValuePair.Value.RemoveIngredient(ingredient.Guid);
                }

                // This is an ingredient before the one we want to remove
            }
        }

        // If an Ingredient is deleted, we have to reduce the index of all Ingredients that have a higher index than the removed one, by one
        // We have to reduce the total IngredientsCount by one

        var ingredientsCount = JSONPrefs.GetInt("FoodRouletteData - IngredientsCount: ");
        var ingredientfound = false;

        // Go through each Ingredient
        for (var index = 0; index < ingredientsCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Ingredient #" + index));
            
            if(ingredientfound)
            {
                // This is an ingredient after the one we removed, so we decrease its index by one
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Ingredient #" + index);
                JSONPrefs.SetString("FoodRouletteData - Guid of Ingredient #" + (index-1), guid.ToString());
            }
            else if (guid == ingredient.Guid)
            {
                // This is the ingredient we want to remove
                ingredientfound = true;
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Ingredient #" + index);
            }

            // This is an ingredient before the one we want to remove
        }

        ingredientsCount--;
        JSONPrefs.SetInt("FoodRouletteData - IngredientsCount: ", ingredientsCount);

        JSONPrefs.DeleteKey("Ingredient (" + ingredient.Guid + ") - Name: ");
        JSONPrefs.DeleteKey("Ingredient (" + ingredient.Guid + ") - Guid of its Unit: ");

        // Also remove the Ingredient from the Dictionary
        Ingredients.Remove(ingredient.Guid);
    }

    public override void Delete(Unit unit)
    {
        if (!Units.ContainsKey(unit.Guid)) return;

        var noUnitUnit = _model.GetNoUnitUnit();

        // Switch the Unit from all Meals where it is used with the No Unit Unit, first
        foreach (KeyValuePair<Guid, Meal> keyValuePair in Meals)
        {
            var ingredientsOfMeal = keyValuePair.Value.GetReadOnlyIngredientsOfMeal();

            // Does one of the ingredients of this meal use this unit?
            for (int i = 0; i < ingredientsOfMeal.Count; i++)
            {
                if (ingredientsOfMeal[i].UnitGuid == unit.Guid)
                {
                    // Set the NoUnit Guid here
                    JSONPrefs.SetString("Meal (" + keyValuePair.Key + ") - Guid of Unit of Ingredient #" + i, noUnitUnit.Guid.ToString());

                    ingredientsOfMeal[i].UnitGuid = noUnitUnit.Guid;
                }
            }
        }

        // If an Unit is deleted, we have to reduce the index of all Units that have a higher index than the removed one, by one
        // We have to reduce the total UnitsCount by one

        var unitsCount = JSONPrefs.GetInt("FoodRouletteData - UnitsCount: ");
        var unitFound = false;

        for (var index = 0; index < unitsCount; index++)
        {
            var guid = new Guid(JSONPrefs.GetString("FoodRouletteData - Guid of Unit #" + index));

            if (unitFound)
            {
                // This is an unit after the one we removed, so we decrease its index by one
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Unit #" + index);
                JSONPrefs.SetString("FoodRouletteData - Guid of Unit #" + (index - 1), guid.ToString());
            }
            else if (guid == unit.Guid)
            {
                // This is the unit we want to remove
                unitFound = true;
                JSONPrefs.DeleteKey("FoodRouletteData - Guid of Unit #" + index);
            }

            // This is an ingredient before the one we want to remove
        }

        unitsCount--;
        JSONPrefs.SetInt("FoodRouletteData - UnitsCount: ", unitsCount);

        JSONPrefs.DeleteKey("Unit (" + unit.Guid + ") - Singular: ");
        JSONPrefs.DeleteKey("Unit (" + unit.Guid + ") - Plural: ");
        JSONPrefs.DeleteKey("Unit (" + unit.Guid + ") - UnitOfMeasurement: ");

        // Also remove the Ingredient from the Dictionary
        Units.Remove(unit.Guid);
    }

    protected override Version ReadDataVersion()
    {
        return new Version(JSONPrefs.GetInt("DataVersion-Major", 0), JSONPrefs.GetInt("DataVersion-Minor", 0), JSONPrefs.GetInt("DataVersion-Revision", 0));
    }

    protected override void WriteDataVersion()
    {
        JSONPrefs.SetInt("DataVersion-Major", DataVersion.Major);
        JSONPrefs.SetInt("DataVersion-Minor", DataVersion.Minor);
        JSONPrefs.SetInt("DataVersion-Revision", DataVersion.Revision);
    }

    protected override void UpdateData(Version appVersion)
    {
        if (DataVersion.Major == 0 && DataVersion.Minor == 0)
        {
            AddDefaultData();
            return;
        }

        if (appVersion < new Version(1, 0, 0))
        {
            return;
        }

        // Add aditional Versions and their conversions here
    }

    protected override void AddDefaultData()
    {
        // Here we can always add the most recent version of the default Data, because this method is only used when the app is launched for the first time

        #region Units

        var gram = new Unit(Guid.NewGuid(), "Gram", "Grams", "g");
        _model.Add(gram);

        var milliliter = new Unit(Guid.NewGuid(), "Milliliter", "Milliliters", "ml");
        _model.Add(milliliter);

        var heapedTableSpoon = new Unit(Guid.NewGuid(), "Heaped Table Spoon", "Heaped Table Spoons", "htbsp");
        _model.Add(heapedTableSpoon);

        var pinch = new Unit(Guid.NewGuid(), "Pinch", "Pinches", "p");
        _model.Add(pinch);

        var pod = new Unit(Guid.NewGuid(), "Pod", "Pods", "pod");
        _model.Add(pod);

        var clove = new Unit(Guid.NewGuid(), "Clove", "Cloves", "clove");
        _model.Add(clove);

        #endregion // Units


        #region Ingredients

        var salt = new Ingredient(Guid.NewGuid(), "Salt");
        _model.Add(salt);

        var pepper = new Ingredient(Guid.NewGuid(), "Pepper");
        _model.Add(pepper);

        var milk = new Ingredient(Guid.NewGuid(), "Milk");
        _model.Add(milk);

        var sugar = new Ingredient(Guid.NewGuid(), "Sugar");
        _model.Add(sugar);

        var semolina = new Ingredient(Guid.NewGuid(), "Semolina");
        _model.Add(semolina);

        var vanillaSugar = new Ingredient(Guid.NewGuid(), "Vanilla Sugar");
        _model.Add(vanillaSugar);

        var basil = new Ingredient(Guid.NewGuid(), "Basil");
        _model.Add(basil);

        var spaghetti = new Ingredient(Guid.NewGuid(), "Spaghetti");
        _model.Add(spaghetti);

        var chili = new Ingredient(Guid.NewGuid(), "Chili");
        _model.Add(chili);

        var garlic = new Ingredient(Guid.NewGuid(), "Garlic");
        _model.Add(garlic);

        var pignolia = new Ingredient(Guid.NewGuid(), "Pignolia");
        _model.Add(pignolia);

        var parmesan = new Ingredient(Guid.NewGuid(), "Parmesan");
        _model.Add(parmesan);

        var lemonJuice = new Ingredient(Guid.NewGuid(), "Lemon Juice");
        _model.Add(lemonJuice);

        var oliveOil = new Ingredient(Guid.NewGuid(), "Olive Oil");
        _model.Add(oliveOil);

        var honey = new Ingredient(Guid.NewGuid(), "Honey");
        _model.Add(honey);

        var cream = new Ingredient(Guid.NewGuid(), "Cream");
        _model.Add(cream);

        var driedChickenBroth = new Ingredient(Guid.NewGuid(), "Dried Chicken Broth");
        _model.Add(driedChickenBroth);

        #endregion // Ingredients


        #region Meals

        var basilPesto = new Meal(Guid.NewGuid(), "Spaghetti with Basil Pesto");
        _model.Add(basilPesto);
        _model.AddIngredientToMeal(basilPesto, spaghetti, 250, gram);
        _model.AddIngredientToMeal(basilPesto, basil, 125, gram);
        _model.AddIngredientToMeal(basilPesto, chili, 1, pod);
        _model.AddIngredientToMeal(basilPesto, garlic, 2, clove);
        _model.AddIngredientToMeal(basilPesto, pignolia, 1.5f, heapedTableSpoon);
        _model.AddIngredientToMeal(basilPesto, parmesan, 1.5f, heapedTableSpoon);
        _model.AddIngredientToMeal(basilPesto, honey, 1.5f, heapedTableSpoon);
        _model.AddIngredientToMeal(basilPesto, lemonJuice, 10, milliliter);
        _model.AddIngredientToMeal(basilPesto, oliveOil, 60, milliliter);
        _model.AddIngredientToMeal(basilPesto, cream, 200, milliliter);
        _model.AddIngredientToMeal(basilPesto, driedChickenBroth, 1, heapedTableSpoon);
        _model.SetRecipeOfMeal("Ingredientamounts sufficient for\n" +
                               "2 - 3 Persons.\n\n" +
                               "1. Start cooking the Spaghetti 'al dente'.\n" +
                               "2. Put all ingredients except the spaghetti\n" +
                               "and the cream, into a blender and pur�e\n" +
                               "them smooth.\n" +
                               "3. Put the pur�e together with the cream\n" +
                               "into a pan. Stir it regularly and let it\n" +
                               "cook for about five to ten minutes until\n" +
                               "it has the desired consistency.\n" +
                               "4. Add the spaghetti to the pan and mix\n" +
                               "it with the sauce.\n" +
                               "Hint: Add parmesan shavings when serving\n" +
                               "the dish.", basilPesto);

        var semolinaPudding = new Meal(Guid.NewGuid(), "Semolina Pudding");
        _model.Add(semolinaPudding);
        _model.AddIngredientToMeal(semolinaPudding, milk, 500, milliliter);
        _model.AddIngredientToMeal(semolinaPudding, sugar, 3, heapedTableSpoon);
        _model.AddIngredientToMeal(semolinaPudding, semolina, 6, heapedTableSpoon);
        _model.AddIngredientToMeal(semolinaPudding, salt, 1, pinch);
        _model.AddIngredientToMeal(semolinaPudding, vanillaSugar, 8, gram);
        _model.SetRecipeOfMeal("Ingredientamounts sufficient for\n" +
                               "two Persons.\n\n" +
                               "1. Heat up the milk in a pot, \n" +
                               "but don't let it boil.\n" +
                               "2. Add the rest of the ingredients\n" +
                               "while stirring regularly.\n" +
                               "3. As soon as you recognize that the\n" +
                               "pudding starts to get stiff,\n" +
                               "put off the heat and continue stirring\n" +
                               "until the desired consistency is reached.\n" +
                               "Hint: A lot of different fruits can be\n" +
                               "added to the dish when being served.\n" +
                               "Try strawberries or raspberries!", semolinaPudding);

        #endregion // Meals


        #region Categories

        var breakfast = new Category(Guid.NewGuid(), "Breakfast");
        _model.Add(breakfast);
        _model.AddMealToCategory(semolinaPudding, breakfast);

        var lunch = new Category(Guid.NewGuid(), "Lunch");
        _model.Add(lunch);
        _model.AddMealToCategory(basilPesto, lunch);

        var supper = new Category(Guid.NewGuid(), "Supper");
        _model.Add(supper);
        _model.AddMealToCategory(basilPesto, supper);

        var desert = new Category(Guid.NewGuid(), "Desert");
        _model.Add(desert);
        _model.AddMealToCategory(semolinaPudding, desert);

        #endregion // Categories

        
        _model.SerializeAllUnits();
        _model.SerializeAllIngredients();
        _model.SerializeAllMeals();
        _model.SerializeAllCategories();
    }
}