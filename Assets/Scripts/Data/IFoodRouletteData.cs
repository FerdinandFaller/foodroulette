using System;
using System.Collections;
using System.Collections.Generic;
using FoodRoulette;

public interface IFoodRouletteData
{
    Dictionary<Guid, Meal> Meals { get; set; }
    Dictionary<Guid, Ingredient> Ingredients { get; set; }
    Dictionary<Guid, Unit> Units { get; set; }
    Dictionary<Guid, Category> Categories { get; set; }
    Meal[] PlannerMeals { get; set; }
    Version DataVersion { get; }


    void CheckAndUpdate(Version appVersion);

    void WriteAllCategories();
    void ReadAllCategories();

    void WriteAllMeals();
    void ReadAllMeals();

    void WriteAllIngredients();
    void ReadAllIngredients();

    void WriteAllUnits();
    void ReadAllUnits();

    Category ReadCategory(Guid guid);
    Meal ReadMeal(Guid guid);
    Ingredient ReadIngredient(Guid guid);
    Unit ReadUnit(Guid guid);

    void Write(Category category);
    void Write(Meal meal);
    void Write(Ingredient ingredient);
    void Write(Unit unit);

    void WriteAllPlannerMeals();
    void ReadAllPlannerMeals();

    void Delete(Category category);
    void Delete(Meal meal);
    void Delete(Ingredient ingredient);
    void Delete(Unit unit);
}