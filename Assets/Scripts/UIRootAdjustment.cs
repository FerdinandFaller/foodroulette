using UnityEngine;

[RequireComponent(typeof(UIRoot))]
public class UIRootAdjustment : MonoBehaviour
{
    public int TargetHeight = 720;

    UIRoot _mRoot;

    void Awake() { _mRoot = GetComponent<UIRoot>(); }

    void Update()
    {
        if (Screen.width > Screen.height)
        {
            _mRoot.manualHeight = TargetHeight;
        }
        else
        {
            _mRoot.manualHeight = Mathf.RoundToInt(TargetHeight * ((float)Screen.height / Screen.width));
        }
    }
}