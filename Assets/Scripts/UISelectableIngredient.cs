using System;
using System.Globalization;
using FoodRoulette;
using UnityEngine;

public class UISelectableIngredient : UIIngredient
{
    public UICheckboxWithImprovedEvent UiCheckBox;
    public UIButton UiEditButton;
    public UIInputWithExactEvent UiInput;


    void OnSelect(bool selected)
    {
        // This event gets relayed from the UIInput child GO

        if (selected)
        {
            RefreshLabelText();
            NameLabel.text = "";

            var color = UiInput.activeColor;
            color.a = 1;
            UiInput.activeColor = color;
            UiInput.label.color = color;

            if (string.IsNullOrEmpty(UiInput.text))
            {
                UiInput.IsTextHasChangedEventEnabled = false;
                UiInput.text = "0";
                UiInput.IsTextHasChangedEventEnabled = true;
            }
        }
        else
        {
            base.RefreshLabelText(); // We use "base." to not call our overridden RefreshLabelText Method. That would result in false UiInput.text values.

            var color = UiInput.activeColor;
            color.a = 0;
            UiInput.activeColor = color;
            UiInput.label.color = color;
        }
    }

    public override void RefreshLabelText()
    {
        base.RefreshLabelText();

        if (MealIBelongTo != null)
        {
            var amount = MealIBelongTo.GetIngredientAmount(Content.Guid);
            UiInput.IsTextHasChangedEventEnabled = false;
            UiInput.text = amount.ToString(CultureInfo.InvariantCulture);
            UiInput.IsTextHasChangedEventEnabled = true;
        }
        else
        {
            UiInput.IsTextHasChangedEventEnabled = false;
            UiInput.text = "0";
            UiInput.IsTextHasChangedEventEnabled = true;
        }
    }
}
