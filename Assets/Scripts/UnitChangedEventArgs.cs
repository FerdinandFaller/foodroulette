using System;

namespace FoodRoulette
{
    public class UnitChangedEventArgs : EventArgs
    {
        public Guid OldUnit;
        public Guid NewUnit;
    }
}