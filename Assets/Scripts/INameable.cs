using System;

namespace FoodRoulette
{
    public class NameChangedEventArgs : EventArgs
    {
        public string OldName;
        public string NewName;


        public NameChangedEventArgs(string oldName, string newName)
        {
            OldName = oldName;
            NewName = newName;
        }
    }

    public interface INameable
    {
        string Name { get; }

        event EventHandler<NameChangedEventArgs> NameChangedEvent;
    }
}

