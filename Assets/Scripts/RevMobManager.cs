using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using FoodRoulette;
using UnityEngine;


public class RevMobManager : MonoBehaviour, IRevMobListener
{
	private int _viewChangesCount;
	private const int ViewChangeCountForFullScreenAd = 15;
	private RevMobFullscreen _revMobFullscreen;
	private bool _adLoaded;
	private bool HasInternet
	{
		get { return _hasInternet; }
		set
		{
			Debug.Log("Has Internet: " + value);

			if (!_hasInternet && value)
			{
				_revMobFullscreen = _revmob.CreateFullscreen();
			}

			_hasInternet = value;
		}
	}

    private float _lastInternetCheck;


	// Just replace the IDs below with your appIDs.
	private static readonly Dictionary<String, String> AppIds = new Dictionary<String, String>() 
	{
		{ "Android", "51f6c88099180133e500001b"},
		{ "IOS", "51f6c86d12157cae92000004" }
	};
	private RevMob _revmob;
	private bool _hasInternet;

	public static RevMobManager Instance;


	void Awake()
	{
		Instance = this;

		Debug.Log("Creating RevMob Session");
		_revmob = RevMob.Start(AppIds, gameObject.name);

		if(_revmob == null)
		{
			Destroy(gameObject);
			return;
		}

		ViewsManager.NextEvent -= OnViewChanged;
		ViewsManager.NextEvent += OnViewChanged;

		ViewsManager.BackEvent -= OnViewChanged;
		ViewsManager.BackEvent += OnViewChanged;
	}

	void Start()
	{
		_revMobFullscreen = _revmob.CreateFullscreen();

		HasInternet = CheckInternetConnectivity();
	}

	void Update()
	{
		// Check the internet connection every 3 min, but then only once
		if(Time.realtimeSinceStartup > _lastInternetCheck + 10 && (int)Time.realtimeSinceStartup % 180 == 0)
		{
		    _lastInternetCheck = Time.realtimeSinceStartup;

			HasInternet = CheckInternetConnectivity();
		}
	}

	private static bool CheckInternetConnectivity()
	{
		// Test 1: Check dns connection to dns.msftncsi.com
		try
		{
			var ipHost = Dns.GetHostEntry("dns.msftncsi.com");
			if (ipHost.AddressList.Length == 0)
			{
				//return ConnectionState.NotConnected;
				return false;
			}
			else
			{
				// 131.107.255.255 is the DNS address of dns.msft.ncsi.com and shall not change
				if (!ipHost.AddressList[0].ToString().Equals("131.107.255.255"))
				{
					//return ConnectionState.LimitedAccess;
					return false;
				}
			}
		}
		catch
		{
			//return ConnectionState.NotConnected;
			return false;
		}

		// Test 2: DNS lookup to http://www.msftncsi.com and download ncsi.txt
		var wReq = (HttpWebRequest)WebRequest.Create("http://www.msftncsi.com/ncsi.txt");
		try
		{
			var wRes = (HttpWebResponse)wReq.GetResponse();

			if (wRes.StatusCode != HttpStatusCode.OK)
			{
				// The response is timing out or redirecting
				//return ConnectionState.LimitedAccess;
				return false;
			}
			using (var sr = new StreamReader(wRes.GetResponseStream()))
			{
				if (sr.ReadToEnd().Equals("Microsoft NCSI"))
				{
					//return ConnectionState.Connected;
					return true;
				}
				else
				{
					// A page can be downloaded but not correctly, possible redirection
					//return ConnectionState.LimitedAccess;
					return false;
				}
			}
		}
		catch
		{
			// If an error occurs, mark as no connection
			//return ConnectionState.NotConnected;
			return false;
		}
	}

	private void OnViewChanged(FoodRouletteView foodRouletteView, FoodRouletteView rouletteView)
	{
		_viewChangesCount++;

		if (_viewChangesCount >= ViewChangeCountForFullScreenAd)
		{
			if (_adLoaded)
			{
				_revMobFullscreen.Show();
			}
		}
	}

	public void AdDidReceive(string revMobAdType)
	{
		Debug.Log("AdDidReceive");

		_adLoaded = true;
	}

	public void AdDidFail(string revMobAdType)
	{
		Debug.Log("AdDidFail");

		if (HasInternet)
		{
            // Recheck the connection, maybe we lost the connection
            HasInternet = CheckInternetConnectivity();

		    if(HasInternet) _revMobFullscreen = _revmob.CreateFullscreen();
		}

		_adLoaded = false;
	}

	public void AdDisplayed(string revMobAdType)
	{
		Debug.Log("AdDisplayed");
		_viewChangesCount = 0;
	}

	public void UserClickedInTheAd(string revMobAdType)
	{
		Debug.Log("UserClickedInTheAd");
		_revMobFullscreen = _revmob.CreateFullscreen();
	}

	public void UserClosedTheAd(string revMobAdType)
	{
		Debug.Log("UserClosedTheAd");
		_revMobFullscreen = _revmob.CreateFullscreen();
	}

	public void InstallDidReceive(string message)
	{
		Debug.Log("InstallDidReceive");

		// TODO: Thank the user for the support
	}

	public void InstallDidFail(string message)
	{
		Debug.Log("InstallDidFail");
	}
}