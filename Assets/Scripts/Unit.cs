using System;

namespace FoodRoulette
{
    public class Unit : CategoryIngredientMealUnitBase
    {
        private string _plural;
        public string Plural
        {
            get { return _plural; }
            set
            {
                var temp = value;

                _plural = value;

                if (temp == value || PluralChangedEvent == null) return;
                Logger.Log(new EventFiringLog("Unit - PluralChangedEvent - NewPlural: " + value));
                PluralChangedEvent(temp, value);
            }
        }

        private bool _usePlural;
        public bool UsePlural
        {
            get { return _usePlural; }
            set
            {
                _usePlural = value;

                if (UsePluralWasSetEvent != null) UsePluralWasSetEvent(_usePlural);
            }
        }

        public event Action<bool> UsePluralWasSetEvent;

        public override string Name
        {
            get
            {
                return UsePlural ? _plural : base.Name;
            }
            set
            {
                base.Name = value;
                _singular = value;
            }
        }

        private string _singular;
        public string Singular
        {
            get { return _singular; }
            set
            {
                var temp = value;

                _singular = value;
                Name = value;

                if (temp == value || SingularChangedEvent == null) return;
                Logger.Log(new EventFiringLog("Unit - SingularChangedEvent - NewSingular: " + value));
                SingularChangedEvent(temp, value);
            }
        }

        private string _unitOfMeasurement;
        public string UnitOfMeasurement
        {
            get { return _unitOfMeasurement; }
            set
            {
                var temp = value;

                _unitOfMeasurement = value;

                if (temp == value || UnitOfMeasurementChangedEvent == null) return;
                Logger.Log(new EventFiringLog("Unit - UnitOfMeasurementChangedEvent - NewUnitOfMeasurement: " + value));
                UnitOfMeasurementChangedEvent(temp, value);
            }
        }

        public event Action<string, string> PluralChangedEvent;
        public event Action<string, string> SingularChangedEvent;
        public event Action<string, string> UnitOfMeasurementChangedEvent;


        public Unit(Guid guid, string singular, string plural, string unitOfMeasurement) : base(guid, singular)
        {
            Singular = singular ?? string.Empty;
            Plural = plural ?? string.Empty;
            UnitOfMeasurement = unitOfMeasurement ?? string.Empty;
        }
    }
}