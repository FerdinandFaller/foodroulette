using System;
using FoodRoulette;
using UnityEngine;
using System.Collections;


public class ScrollToAndRecenterOnAddedIngredient : MonoBehaviour
{
    private UIDraggablePanel _draggablePanel;
    private Vector3 _originalPanelPos;
    public UIIngredientsList UiIngredientsList;


    void Awake()
    {
        UiIngredientsList.AddedContentEvent += OnIngredientsListAddedUiIngredient;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnAddedIngredient - Awake - UiIngredientsList.AddedUiIngredientEvent += OnIngredientsListAddedUiIngredient"));

        // Get the DraggablePanel via the IngredientsList
        _draggablePanel = NGUITools.FindInParents<UIDraggablePanel>(UiIngredientsList.Grid.gameObject);
        _originalPanelPos = _draggablePanel.transform.position;
    }

    private void OnIngredientsListAddedUiIngredient(UIIngredient addedUiIngredient)
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnAddedIngredient - OnIngredientsListAddedUiIngredient - addedUiIngredient: " + addedUiIngredient));

        StartCoroutine_Auto(ScrollAndRecenterOnUiIngredientCoRoutine(addedUiIngredient));
    }

    private IEnumerator ScrollAndRecenterOnUiIngredientCoRoutine(UIIngredient uiIngredient)
    {
        if (uiIngredient == null) throw new ArgumentNullException("uiIngredient");

        // Wait a bit. The Grid needs a short moment to resort and reposition the added ingredient
        yield return new WaitForSeconds(0.001f);

        // Calculate the Position of the added ingredient
        var pos = uiIngredient.transform.worldToLocalMatrix.MultiplyPoint3x4(_draggablePanel.transform.position);
        pos.z = _originalPanelPos.z;
        pos.x = _originalPanelPos.x;

        // Use a SpringPanel to scroll the DraggablePanel to the added UIIngredient
        var spring = SpringPanel.Begin(_draggablePanel.gameObject, pos, 100);
        spring.onFinished += OnSpringPanelScrollFinished;
        Logger.Log(new EventSubscriptionLog("ScrollToAndRecenterOnAddedIngredient - ScrollAndRecenterOnUiIngredientCoRoutine"));
    }

    private void OnSpringPanelScrollFinished()
    {
        Logger.Log(new EventReceivingLog("ScrollToAndRecenterOnAddedIngredient - OnSpringPanelScrollFinished"));

        // Force a Recenter by Starting the WaitCoRoutine of the IngredientsList manually
        UiIngredientsList.StartWaitCoroutine();
    }
}
