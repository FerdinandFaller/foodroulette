using FoodRoulette;


public class UIUnit : UICategoryIngredientMealUnitBase<Unit>
{
    void Awake()
    {
        ContentChangedEvent -= OnContentChanged;
        ContentChangedEvent += OnContentChanged;
    }

    private void OnContentChanged(Unit oldUnit, Unit newUnit)
    {
        if(oldUnit != null) oldUnit.UsePluralWasSetEvent -= OnContentUsePluralWasSet;

        Content.UsePluralWasSetEvent -= OnContentUsePluralWasSet;
        Content.UsePluralWasSetEvent += OnContentUsePluralWasSet;
    }

    private void OnContentUsePluralWasSet(bool state)
    {
        RefreshLabelText();
    }

    public override void RefreshLabelText()
    {
        NameLabel.text = Content.Name;
    }
}
