using UnityEngine;

public class UIVersion : MonoBehaviour
{
    public int Major;
    public int Minor;
    public int Revision;

    public static Version AppVersion;


    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        AppVersion = new Version(Major, Minor, Revision);
    }
}