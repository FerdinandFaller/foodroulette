using System.Collections.Generic;
using UnityEngine;

public class UIDividers : MonoBehaviour
{
    [HideInInspector] public Dictionary<string, UIDivider> DividerDict;

    public UIWidget WidgetContainerGo;
    public GameObject DividerPrefab;
    public List<string> DividerNames;

    private UIDivider _prevDivider;


    void Awake()
    {
        DividerDict = new Dictionary<string, UIDivider>();
    }

    public void Initialize()
    {
        var i = 0;
        foreach (var dividerName in DividerNames)
        {
            // Create Divider
            var go = NGUITools.AddChild(gameObject, DividerPrefab);
            var dividerScript = go.GetComponent<UIDivider>();
            DividerDict.Add(dividerName, dividerScript);

            // Setup Divider
            dividerScript.Name = dividerName;
            dividerScript.Index = i;

            // Labeltext
            dividerScript.Label.text = dividerName;

            // Divider position
            var anchor = go.GetComponent<UIAnchor>();
            anchor.widgetContainer = WidgetContainerGo;
            anchor.relativeOffset.x = i*(1f/DividerNames.Count);
            i++;

            // Divider size
            var spriteStretchScript = dividerScript.SpriteComplete.GetComponent<UIStretch>();
            spriteStretchScript.widgetContainer = WidgetContainerGo;

            // Name Divider GO
            go.name = "Divider(" + dividerName + ")";
        }
    }

    public bool SelectDivider(string dividerName)
    {
        if(!DividerDict.ContainsKey(dividerName)) return false;
        var divider = DividerDict[dividerName];

        return SelectDivider(divider);
    }

    public bool SelectDivider(UIDivider divider)
    {
        if (!DividerDict.ContainsValue(divider)) return false;
        if (divider == _prevDivider) return false;

        // Prev Divider
        if (_prevDivider != null)
        {
            TweenDividerSize(_prevDivider, 1f, 0.7f, 0.5f);
        }

        // Current Divider
        _prevDivider = divider;
        TweenDividerSize(divider, 0.7f, 1f, 0.5f);

        return true;
    }

    private static void TweenDividerSize(UIDivider divider, float from, float to, float time)
    {
        iTween.ValueTo(divider.gameObject, iTween.Hash( "from", from, 
                                                        "to", to, 
                                                        "time", time, 
                                                        "easetype", iTween.EaseType.linear,
                                                        "onupdate", "OnDividerTweenSizeUpdate"));
    }
}
