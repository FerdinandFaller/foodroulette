﻿using System;


namespace FoodRoulette
{
    public abstract class CategoryIngredientMealUnitBase : IComparable<CategoryIngredientMealUnitBase>, INameable, IGloballyIdentifyable
    {
        public Guid Guid { get; protected set; }

        private string _name;
        public virtual string Name
        {
            get { return _name; }
            set
            {
                var temp = _name;

                _name = value;

                if (temp == value || NameChangedEvent == null) return;
                Logger.Log(new EventFiringLog("CategoryIngredientMealUnitBase - NameChangedEvent - Name: " + value));
                NameChangedEvent(this, new NameChangedEventArgs(temp, value));
            }
        }
        public event EventHandler<NameChangedEventArgs> NameChangedEvent;


        protected CategoryIngredientMealUnitBase()
        {
            
        }
        
        protected CategoryIngredientMealUnitBase(Guid guid, string name)
        {
            Guid = guid == Guid.Empty ? Guid.NewGuid() : guid;
            Name = name ?? string.Empty;
        }
        
        public int CompareTo(CategoryIngredientMealUnitBase other)
        {
            if (other == null) throw new ArgumentNullException("other");
            return Guid.CompareTo(other.Guid);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
