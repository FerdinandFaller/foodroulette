using FoodRoulette;
using UnityEngine;


/// <summary>
/// Important: Don't forget to override Awake, OnDestroy or OnCenteredObjectChanged if necessary. Unity only calls Awake and OnDestroy of the actual script, not the scripts it inherits from.
/// </summary>
public class CenterOnChildEventListener : MonoBehaviour
{
    public CenterOnChildWithEvent CenterOnChildScript;


    protected virtual void Awake()
    {
        if (CenterOnChildScript == null) return;
        CenterOnChildScript.CenteredObjectChangedEvent += OnCenteredObjectChanged;
        Logger.Log(new EventSubscriptionLog("CenterOnChildEventListener - Awake - CenterOnChildScript.CenteredObjectChangedEvent += OnCenteredObjectChanged"));
    }

    protected virtual void OnDestroy()
    {
        if (CenterOnChildScript == null) return;
        CenterOnChildScript.CenteredObjectChangedEvent -= OnCenteredObjectChanged;
        Logger.Log(new EventSubscriptionLog("CenterOnChildEventListener - OnDestroy - CenterOnChildScript.CenteredObjectChangedEvent -= OnCenteredObjectChanged"));
    }

    protected virtual void OnCenteredObjectChanged(GameObject prevGo, GameObject go)
    {
        Logger.Log(new EventReceivingLog("CenterOnChildEventListener - OnCenteredObjectChanged"));
    }
}
