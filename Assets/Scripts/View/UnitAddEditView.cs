﻿using FoodRoulette;
using UnityEngine;


public class UnitAddEditView : FoodRouletteView
{
    public UIButton BackButton;
    public UIButton DeleteThisUnitButton;
    public UIInputWithExactEvent SingularInput;
    public UIInputWithExactEvent PluralInput;
    public UIInputWithExactEvent UnitOfMeasurementInput;
    public UILabel TitleLabel;
    public UILabel SingularLabel;
    public UILabel PluralLabel;
    public UILabel UnitOfMeasurementLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnScreenManagerNext;
        ViewsManager.NextEvent += OnScreenManagerNext;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        Model.UnitInUnitAddEditWasSetEvent -= OnModelUnitInUnitAddEditWasSet;
        Model.UnitInUnitAddEditWasSetEvent += OnModelUnitInUnitAddEditWasSet;

        SingularInput.TextHasChangedEvent -= OnSingularTextHasChanged;
        SingularInput.TextHasChangedEvent += OnSingularTextHasChanged;

        PluralInput.TextHasChangedEvent -= OnPluralTextHasChanged;
        PluralInput.TextHasChangedEvent += OnPluralTextHasChanged;

        UnitOfMeasurementInput.TextHasChangedEvent -= OnUnitOfMeasurementTextHasChanged;
        UnitOfMeasurementInput.TextHasChangedEvent += OnUnitOfMeasurementTextHasChanged;

        var deleteScript = DeleteThisUnitButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        deleteScript.TriggeredEvent -= OnDeleteThisUnitButtonTriggered;
        deleteScript.TriggeredEvent += OnDeleteThisUnitButtonTriggered;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelUnitInUnitAddEditWasSet(Unit unit)
    {
        unit.NameChangedEvent -= OnUnitNameChanged;
        unit.NameChangedEvent += OnUnitNameChanged;

        TitleLabel.text = unit.Singular;
        BackTextForNextView = unit.Singular;

        SingularInput.text = unit.Singular;
        PluralInput.text = unit.Plural;
        UnitOfMeasurementInput.text = unit.UnitOfMeasurement;
    }

    private void OnDeleteThisUnitButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Unit unit;
        Model.GetUnitInUnitAddEdit(out unit);

        var label = "Do you want to permanently\ndelete the Unit\n'" + unit.Name + "'?";

        ViewsManager.ShowConfirmationView(label, false, OnDeleteThisUnitButtonTriggeredConfirmed, null);
    }

    private void OnDeleteThisUnitButtonTriggeredConfirmed(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Unit unit;
        Model.GetUnitInUnitAddEdit(out unit);

        Controller.RemoveUnit(unit);
        ViewsManager.Back();
    }

    private void OnUnitOfMeasurementTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        Unit unit;
        Model.GetUnitInUnitAddEdit(out unit);

        Controller.SetUnitOfMeasurementOfUnit(args.newText, unit);
    }

    private void OnPluralTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        Unit unit;
        Model.GetUnitInUnitAddEdit(out unit);

        Controller.SetPluralOfUnit(args.newText, unit);
    }

    private void OnSingularTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        Unit unit;
        Model.GetUnitInUnitAddEdit(out unit);

        Controller.SetSingularOfUnit(args.newText, unit);
    }

    private void OnUnitNameChanged(object sender, NameChangedEventArgs args)
    {
        TitleLabel.text = args.NewName;
        BackTextForNextView = args.NewName;
    }

    private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType()) return;

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}