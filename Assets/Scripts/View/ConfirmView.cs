using System;
using FoodRoulette;
using UnityEngine;


public class ConfirmView : FoodRouletteView
{
    public UIButton ConfirmButton;
    public UIButton CancelButton;
    public UIButton ConfirmButtonCentered;
    public UISlicedSprite BackgroundSprite;
    public UISlicedSprite LabelBackgroundSprite;
    public UILabel Label;

    public EventHandler<UIButtonMessageWithEvent.TriggeredEventArgs> ConfirmButtonTriggeredHandler;
    public EventHandler<UIButtonMessageWithEvent.TriggeredEventArgs> CancelButtonTriggeredHandler;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        var confirmScript = ConfirmButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        confirmScript.TriggeredEvent -= OnConfirmButtonTriggered;
        confirmScript.TriggeredEvent += OnConfirmButtonTriggered;

        var confirmCenteredScript = ConfirmButtonCentered.gameObject.GetComponent<UIButtonMessageWithEvent>();
        confirmCenteredScript.TriggeredEvent -= OnConfirmButtonTriggered;
        confirmCenteredScript.TriggeredEvent += OnConfirmButtonTriggered;

        var cancelScript = CancelButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        cancelScript.TriggeredEvent -= OnCancelButtonTriggered;
        cancelScript.TriggeredEvent += OnCancelButtonTriggered;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnCancelButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.HideConfirmationView();

        if (CancelButtonTriggeredHandler != null)
        {
            CancelButtonTriggeredHandler(sender, args);
        }
    }

    private void OnConfirmButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.HideConfirmationView();

        if (ConfirmButtonTriggeredHandler != null)
        {
            ConfirmButtonTriggeredHandler(sender, args);
        }
    }
}