﻿using System.Linq;
using FoodRoulette;
using UnityEngine;


public class CategoriesView : FoodRouletteView
{
    private Job _updateCategoriesListJob;

    public UIButton AddNewCategoryButton;
    public UIButton BackButton;
    public UICategoriesList CategoriesList;
    public UILabel TitleLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
 	    base.Initialize(controller, model);

        BackTextForNextView = "Categories";

        ViewsManager.NextEvent -= OnViewsManagerNext;
        ViewsManager.NextEvent += OnViewsManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        _updateCategoriesListJob = Job.Make(UIListHelper.UpdateList(CategoriesList, Model), false);

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        var addScript = AddNewCategoryButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        addScript.TriggeredEvent -= OnAddNewCategoryButtonTriggered;
        addScript.TriggeredEvent += OnAddNewCategoryButtonTriggered;
        
        Model.AllCategoriesDeSerializedEvent -= OnModelDeserializedAllCategories;
        Model.AllCategoriesDeSerializedEvent += OnModelDeserializedAllCategories;

        CategoriesList.AddedContentEvent -= OnUiCategoriesListAddedUiCategory;
        CategoriesList.AddedContentEvent += OnUiCategoriesListAddedUiCategory;
    }

    private void OnModelDeserializedAllCategories()
    {
        // Initial loading is over, now we can listen to adding and removing
        Model.CategoryAddedEvent -= OnModelCategoryAdded;
        Model.CategoryAddedEvent += OnModelCategoryAdded;

        Model.CategoryRemovedEvent -= OnModelCategoryRemoved;
        Model.CategoryRemovedEvent += OnModelCategoryRemoved;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void UpdateCategories()
    {
        if(_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
        _updateCategoriesListJob = Job.Make(UIListHelper.UpdateList(CategoriesList, Model));

        CategoriesList.ScrollToTop();
    }

    private void OnAddNewCategoryButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Category newCategory;
        Controller.AddNewCategory(out newCategory);

        Controller.SetCategoryInCategoryAddEdit(newCategory);

        ViewsManager.Next<CategoryAddEditView>();
    }

    private void OnUiCategoriesListAddedUiCategory(UICategory addedCategory)
    {
        Logger.Log(new EventReceivingLog("CategoriesView - OnUiCategoriesListAddedUiCategory"));

        // Register for the Triggered Event of this UICategory (User clicked on a UICategory)
        var script = addedCategory.gameObject.GetComponent<UIButtonMessageWithEvent>();
        if (script != null)
        {
            script.TriggeredEvent -= OnUiCategoryInListTriggered;
            script.TriggeredEvent += OnUiCategoryInListTriggered;
        }
    }

    private void OnUiCategoryInListTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs triggeredEventArgs)
    {
        Logger.Log(new EventReceivingLog("CategoriesView - OnUICategoryInListTriggered"));

        AudioController.Play("ButtonClick");

        var uiCat = ((UIButtonMessageWithEvent)sender).gameObject.GetComponent<UICategory>();

        Controller.SetCategoryInMealsOfCategory(uiCat.Content);

        ViewsManager.Next<MealsOfCategoryView>();
    }

    private void OnModelCategoryAdded(Category category)
    {
        UpdateCategories();
    }

    private void OnModelCategoryRemoved(Category category)
    {
        UpdateCategories();
    }

    private void OnViewsManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();

            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
        TitleLabel.text = "Categories";

        UpdateCategories();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            UpdateCategories();
        }
        else
        {
            if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}