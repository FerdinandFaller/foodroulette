﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using FoodRoulette;
using UnityEngine;


public class IngredientOfMealAddEditView : FoodRouletteView
{
    private UIPanel _unitsListPanel;

    private Job _updateUnitsListJob;
    private Job _updateIngredientsListJob;

    private Meal _currentMeal;
    private IngredientOfMeal _currentIngredientOfMeal;

    private CenterOnChildWithEvent _centerScriptOfIngredientsList;
    private CenterOnChildWithEvent _centerScriptOfUnitsList;

    public UIButton BackButton;
    public UIInputWithExactEvent TitleInput;

    public UIInputWithExactEvent AmountInput;
    public UIIngredientsList IngredientsList;
    public UICenteredUnitsList UnitsList;

    public GameObject UnitButtonsGrid;
    public UIButton AddNewUnitButton;
    public UIButton EditCurrentUnitButton;
    public UIButton DeleteCurrentUnitButton;

    public GameObject IngredientButtonsGrid;
    public UIButton AddNewIngredientButton;
    public UIButton DeleteCurrentIngredientButton;

    public UILabel NotIncludedInMealLabel;
    public UIButton AddIngredientToMealButton;

    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnViewsManagerNext;
        ViewsManager.NextEvent += OnViewsManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        _unitsListPanel = UnitsList.gameObject.GetComponent<UIPanel>();

        _currentIngredientOfMeal = new IngredientOfMeal();

        Model.MealAndIngredientOfMealInMealAddEditWasSetEvent -= OnModelMealAndIngredientOfMealInMealAddEditWasSet;
        Model.MealAndIngredientOfMealInMealAddEditWasSetEvent += OnModelMealAndIngredientOfMealInMealAddEditWasSet;

        _updateUnitsListJob = Job.Make(UIListHelper.UpdateList(UnitsList, Model), false);
        _updateIngredientsListJob = Job.Make(UIListHelper.UpdateList(IngredientsList, Model), false);

        _centerScriptOfIngredientsList = IngredientsList.Grid.GetComponent<CenterOnChildWithEvent>();
        _centerScriptOfIngredientsList.CenteredObjectChangedEvent -= OnIngredientsListCenteredObjectChanged;
        _centerScriptOfIngredientsList.CenteredObjectChangedEvent += OnIngredientsListCenteredObjectChanged;
        _centerScriptOfUnitsList = UnitsList.Grid.GetComponent<CenterOnChildWithEvent>();
        _centerScriptOfUnitsList.CenteredObjectChangedEvent -= OnUnitsListCenteredObjectChanged;
        _centerScriptOfUnitsList.CenteredObjectChangedEvent += OnUnitsListCenteredObjectChanged;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        TitleInput.TextHasChangedEvent -= OnTitleTextHasChanged;
        TitleInput.TextHasChangedEvent += OnTitleTextHasChanged;

        AmountInput.TextHasChangedEvent -= OnAmountTextHasChanged;
        AmountInput.TextHasChangedEvent += OnAmountTextHasChanged;

        var addNewUnitScript = AddNewUnitButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        addNewUnitScript.TriggeredEvent -= OnAddNewUnitButtonTriggered;
        addNewUnitScript.TriggeredEvent += OnAddNewUnitButtonTriggered;

        var editCurrentUnitScript = EditCurrentUnitButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        editCurrentUnitScript.TriggeredEvent -= OnEditCurrentUnitButtonTriggered;
        editCurrentUnitScript.TriggeredEvent += OnEditCurrentUnitButtonTriggered;

        var deleteCurrentUnitScript = DeleteCurrentUnitButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        deleteCurrentUnitScript.TriggeredEvent -= OnDeleteCurrentUnitButtonTriggered;
        deleteCurrentUnitScript.TriggeredEvent += OnDeleteCurrentUnitButtonTriggered;

        var addNewIngredientScript = AddNewIngredientButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        addNewIngredientScript.TriggeredEvent -= OnAddNewIngredientButtonTriggered;
        addNewIngredientScript.TriggeredEvent += OnAddNewIngredientButtonTriggered;

        var deleteCurrentIngredientScript = DeleteCurrentIngredientButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        deleteCurrentIngredientScript.TriggeredEvent -= OnDeleteCurrentIngredientButtonTriggered;
        deleteCurrentIngredientScript.TriggeredEvent += OnDeleteCurrentIngredientButtonTriggered;

        var addIngredientToMealScript = AddIngredientToMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        addIngredientToMealScript.TriggeredEvent -= OnAddIngredientToMealButtonTriggered;
        addIngredientToMealScript.TriggeredEvent += OnAddIngredientToMealButtonTriggered;

        Model.AllUnitsDeSerializedEvent -= OnModelDeserializedAllUnits;
        Model.AllUnitsDeSerializedEvent += OnModelDeserializedAllUnits;

        Model.AllIngredientsDeSerializedEvent -= OnModelDeserializedAllIngredients;
        Model.AllIngredientsDeSerializedEvent += OnModelDeserializedAllIngredients;

        UnitsList.RemovedContentEvent -= OnUnitsListRemovedContent;
        UnitsList.RemovedContentEvent += OnUnitsListRemovedContent;

        IngredientsList.RemovedContentEvent -= OnIngredientsListRemovedContent;
        IngredientsList.RemovedContentEvent += OnIngredientsListRemovedContent;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelMealAndIngredientOfMealInMealAddEditWasSet(Meal meal, IngredientOfMeal ingredientOfMeal)
    {
        if (meal == null) return;
        if (!Model.ContainsIngredient(ingredientOfMeal.IngredientGuid)) return;

        if (_currentMeal != null) _currentMeal.IngredientAddedEvent -= OnMealAddedIngredient;

        _currentMeal = meal;
        _currentIngredientOfMeal = ingredientOfMeal;

        _currentMeal.IngredientAddedEvent -= OnMealAddedIngredient;
        _currentMeal.IngredientAddedEvent += OnMealAddedIngredient;

        var ingredientIsInMeal = meal.ContainsIngredient(ingredientOfMeal.IngredientGuid);

        // Enable/Disable Unitslist, UnitButtons and AmountInput
        ShowHideUnitsListAmountInputAndUnitButtonsGrid(ingredientIsInMeal);

        IngredientsList.Reposition();

        // Center on Ingredient
        Ingredient ingredient;
        Model.Get(ingredientOfMeal.IngredientGuid, out ingredient);
        Job.Make(WaitThenCenterOnIngredient(ingredient));

        // Set TitleInput
        TitleInput.IsTextHasChangedEventEnabled = false;
        TitleInput.text = ingredient.Name;
        TitleInput.IsTextHasChangedEventEnabled = true;

        // Enable/Disable NotincludedInMealLabel and AddToMealButton
        ShowHideNotIncludedInMealLabelAndAddToMealButton(!ingredientIsInMeal);

        // Set amount
        AmountInput.IsTextHasChangedEventEnabled = false;
        AmountInput.text = ingredientOfMeal.Amount.ToString(CultureInfo.InvariantCulture);
        AmountInput.IsTextHasChangedEventEnabled = true;

        // Check if we have a current unit
        if (_currentIngredientOfMeal.UnitGuid != Guid.Empty)
        {
            // Should Unit use plural?
            Unit unit;
            Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);
            unit.UsePlural = Math.Abs(_currentIngredientOfMeal.Amount - 1f) > 0.0001f;
        }
    }

    private void ShowHideUnitsListAmountInputAndUnitButtonsGrid(bool shouldBeVisible)
    {
        // Enable/Disable Unitslist and UnitButtons
        UnitsList.transform.parent.gameObject.SetActive(shouldBeVisible);

        if (shouldBeVisible)
        {
            TweenAlpha.Begin(_unitsListPanel.gameObject, 0.1f, 1);
            TweenAlpha.Begin(UnitsList.Background.gameObject, 0.1f, 1);
            TweenAlpha.Begin(UnitsList.CenterBackground.gameObject, 0.1f, 1);

            UnitsList.Reposition();

            Unit unit;
            Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);
            Job.Make(WaitThenCenterOnUnit(unit));
        }
        else
        {
            TweenAlpha.Begin(_unitsListPanel.gameObject, 1.1f, 0.001f);
            TweenAlpha.Begin(UnitsList.Background.gameObject, 0.1f, 0.001f);
            TweenAlpha.Begin(UnitsList.CenterBackground.gameObject, 0.1f, 0.001f);
        }

        UnitButtonsGrid.SetActive(shouldBeVisible);

        // Enable/Disable AmountInput
        AmountInput.gameObject.SetActive(shouldBeVisible);
    }

    private void ShowHideNotIncludedInMealLabelAndAddToMealButton(bool shouldBeVisible)
    {
        // Enable/Disable NotincludedInMealLabel and AddToMealButton
        NotIncludedInMealLabel.gameObject.SetActive(shouldBeVisible);
        AddIngredientToMealButton.gameObject.SetActive(shouldBeVisible);
    }

    private void OnMealAddedIngredient(object sender, Meal.IngredientEventArgs args)
    {
        // Disable NotIncluded Label and AddIngredientToMeal Button
        ShowHideNotIncludedInMealLabelAndAddToMealButton(false);

        // Enable Unitslist, Unit Buttons and AmountInput
        ShowHideUnitsListAmountInputAndUnitButtonsGrid(true);

        // Center on its Unit
        Unit unit;
        Model.Get(_currentMeal.GetIngredientUnitGuid(args.IngredientOfMeal.IngredientGuid), out unit);
        Job.Make(WaitThenCenterOnUnit(unit));

        _currentIngredientOfMeal.UnitGuid = unit.Guid;

        // Set amount
        _currentIngredientOfMeal.Amount = _currentMeal.GetIngredientAmount(args.IngredientOfMeal.IngredientGuid);
        AmountInput.IsTextHasChangedEventEnabled = false;
        AmountInput.text = _currentIngredientOfMeal.Amount.ToString(CultureInfo.InvariantCulture);
        AmountInput.IsTextHasChangedEventEnabled = true;

        Ingredient ingredient;
        Model.Get(args.IngredientOfMeal.IngredientGuid, out ingredient);

        // Set TitleInput
        TitleInput.IsTextHasChangedEventEnabled = false;
        TitleInput.text = ingredient.Name;
        TitleInput.IsTextHasChangedEventEnabled = true;
    }

    #region Units

    private void OnModelDeserializedAllUnits()
    {
        // Initial loading is over, now we can listen to adding and removing
        Model.UnitAddedEvent -= OnModelUnitAdded;
        Model.UnitAddedEvent += OnModelUnitAdded;

        Model.UnitRemovedEvent -= OnModelUnitRemoved;
        Model.UnitRemovedEvent += OnModelUnitRemoved;

        UpdateUnitsList();
    }

    private void OnModelUnitAdded(Unit unit)
    {
        UpdateUnitsList();
    }

    private void OnModelUnitRemoved(Unit unit)
    {
        UpdateUnitsList();
    }

    private void OnUnitsListCenteredObjectChanged(GameObject oldGameObject, GameObject newGameObject)
    {
        if (oldGameObject != null) oldGameObject.GetComponent<UIUnit>().Content.UsePlural = false;

        if (_currentMeal == null || _currentIngredientOfMeal == null) return;

        // Get the Unit
        var uiUnit = newGameObject.GetComponent<UIUnit>();

        if (uiUnit == null) return;

        // Should Unit use plural?
        uiUnit.Content.UsePlural = Math.Abs(_currentIngredientOfMeal.Amount - 1f) > 0.0001f;

        var unitGuid = uiUnit.Content.Guid;

        // Is it a change?
        if (unitGuid == _currentIngredientOfMeal.UnitGuid) return;

        // Is the current ingredient in the meal?
        if (!_currentMeal.ContainsIngredient(_currentIngredientOfMeal.IngredientGuid)) return;

        Ingredient ingredient;
        Model.Get(_currentIngredientOfMeal.IngredientGuid, out ingredient);

        Controller.SetUnitOfIngredientInMeal(_currentMeal, ingredient, uiUnit.Content);
        _currentIngredientOfMeal.UnitGuid = unitGuid;
    }

    private void OnAddNewUnitButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Unit newUnit;
        Controller.AddNewUnit(out newUnit);

        Controller.SetUnitInUnitAddEdit(newUnit);

        if (_currentIngredientOfMeal != null && _currentIngredientOfMeal.IngredientGuid != Guid.Empty && _currentMeal != null)
        {
            _currentMeal.ChangeIngredientUnit(_currentIngredientOfMeal.IngredientGuid, newUnit.Guid);
            Job.Make(WaitThenCenterOnUnit(newUnit));
        }

        ViewsManager.Next<UnitAddEditView>();
    }

    private IEnumerator WaitThenCenterOnUnit(Unit unit)
    {
        yield return new WaitForSeconds(0.2f);

        UnitsList.CenterOn(unit);
    }

    private void OnEditCurrentUnitButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        if (_currentIngredientOfMeal == null || _currentIngredientOfMeal.UnitGuid == Guid.Empty) return;

        Unit unit;
        Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);

        Controller.SetUnitInUnitAddEdit(unit);

        ViewsManager.Next<UnitAddEditView>();
    }

    private void OnDeleteCurrentUnitButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        if (_currentIngredientOfMeal == null || _currentIngredientOfMeal.UnitGuid == Guid.Empty) return;

        Unit unit;
        Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);

        var label = "Do you want to permanently\ndelete the Unit\n'" + unit.Singular + "'?";

        ViewsManager.ShowConfirmationView(label, false, OnDeleteCurrentUnitButtonTriggeredConfirmed, null);
    }

    private void OnDeleteCurrentUnitButtonTriggeredConfirmed(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        if (_currentIngredientOfMeal == null || _currentIngredientOfMeal.UnitGuid == Guid.Empty) return;

        Unit unit;
        Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);

        Controller.RemoveUnit(unit);
    }

    private void OnUnitsListRemovedContent(UIUnit uiUnit)
    {
        UnitsList.Reposition();
        UnitsList.Recenter();
    }

    private void UpdateUnitsList()
    {
        if (_updateUnitsListJob.Running) _updateUnitsListJob.Kill();
        _updateUnitsListJob = Job.Make(UIListHelper.UpdateList(UnitsList, Model));
    }

    #endregion // Units

    #region Ingredients

    private void OnModelDeserializedAllIngredients()
    {
        // Initial loading is over, now we can listen to adding and removing
        Model.IngredientAddedEvent -= OnModelIngredientAdded;
        Model.IngredientAddedEvent += OnModelIngredientAdded;

        Model.IngredientRemovedEvent -= OnModelIngredientRemoved;
        Model.IngredientRemovedEvent += OnModelIngredientRemoved;

        UpdateIngredientsList();
    }

    private void OnModelIngredientAdded(Ingredient ingredient)
    {
        UpdateIngredientsList();
    }

    private void OnModelIngredientRemoved(Ingredient ingredient)
    {
        UpdateIngredientsList();
    }

    private void OnIngredientsListCenteredObjectChanged(GameObject oldGameObject, GameObject newGameObject)
    {
        if (_currentMeal == null || _currentIngredientOfMeal == null || newGameObject == null) return;

        // Get the Ingredient
        var newUiIngredient = newGameObject.GetComponent<UIIngredient>();

        if (newUiIngredient == null) return;

        var newIngredientGuid = newUiIngredient.Content.Guid;

        // Is it a change?
        if (newIngredientGuid == _currentIngredientOfMeal.IngredientGuid) return;

        _currentIngredientOfMeal.IngredientGuid = newIngredientGuid;

        // Disable old units plural in any case
        var oldUiIngredient = oldGameObject.GetComponent<UIIngredient>();
        if (oldUiIngredient != null)
        {
            var oldIngredientGuid = oldUiIngredient.Content.Guid;
            Unit oldIngredientUnit;
            Model.Get(_currentMeal.GetIngredientUnitGuid(oldIngredientGuid), out oldIngredientUnit);
            if (oldIngredientUnit != null) oldIngredientUnit.UsePlural = false;
        }

        // Is the ingredient in the meal?
        if (_currentMeal.ContainsIngredient(newIngredientGuid))
        {
            // Disable NotIncluded Label and AddIngredientToMeal Button
            ShowHideNotIncludedInMealLabelAndAddToMealButton(false);

            // Enable Unitslist, Unit Buttons and AmountInput
            ShowHideUnitsListAmountInputAndUnitButtonsGrid(true);

            // Center on its Unit
            Unit unit;
            Model.Get(_currentMeal.GetIngredientUnitGuid(newIngredientGuid), out unit);
            Job.Make(WaitThenCenterOnUnit(unit));

            _currentIngredientOfMeal.UnitGuid = unit.Guid;

            // Set amount
            _currentIngredientOfMeal.Amount = _currentMeal.GetIngredientAmount(newIngredientGuid);
            AmountInput.IsTextHasChangedEventEnabled = false;
            AmountInput.text = _currentIngredientOfMeal.Amount.ToString(CultureInfo.InvariantCulture);
            AmountInput.IsTextHasChangedEventEnabled = true;

            // Should Unit use plural?
            unit.UsePlural = Math.Abs(_currentIngredientOfMeal.Amount - 1f) > 0.0001f;

            // Set TitleInput
            TitleInput.IsTextHasChangedEventEnabled = false;
            TitleInput.text = newUiIngredient.Content.Name;
            TitleInput.IsTextHasChangedEventEnabled = true;
        }
        else
        {
            // Disable Unitslist, Unit Buttons and AmountInput
            ShowHideUnitsListAmountInputAndUnitButtonsGrid(false);

            // Enable NotIncluded Label and AddIngredientToMeal Button
            ShowHideNotIncludedInMealLabelAndAddToMealButton(true);

            // Set TitleInput
            TitleInput.IsTextHasChangedEventEnabled = false;
            TitleInput.label.text = newUiIngredient.Content.Name;
            TitleInput.IsTextHasChangedEventEnabled = true;
        }
    }

    private void OnAddNewIngredientButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Ingredient newIngredient;
        Controller.AddNewIngredient(out newIngredient);

        Job.Make(WaitThenCenterOnIngredient(newIngredient));
    }

    private IEnumerator WaitThenCenterOnIngredient(Ingredient ingredient)
    {
        yield return new WaitForSeconds(0.2f);

        IngredientsList.CenterOn(ingredient);
    }

    private void OnDeleteCurrentIngredientButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        if (_currentIngredientOfMeal == null || _currentIngredientOfMeal.IngredientGuid == Guid.Empty) return;

        Ingredient ingredient;
        Model.Get(_currentIngredientOfMeal.IngredientGuid, out ingredient);

        var label = "Do you want to permanently\ndelete the Ingredient\n'" + ingredient.Name + "'?";

        ViewsManager.ShowConfirmationView(label, false, OnDeleteCurrentIngredientButtonTriggeredConfirmed, null);
    }

    private void OnDeleteCurrentIngredientButtonTriggeredConfirmed(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        if (_currentIngredientOfMeal == null || _currentIngredientOfMeal.IngredientGuid == Guid.Empty) return;

        Ingredient ingredient;
        Model.Get(_currentIngredientOfMeal.IngredientGuid, out ingredient);

        Controller.RemoveIngredient(ingredient);
    }

    private void OnIngredientsListRemovedContent(UIIngredient uiIngredient)
    {
        if (IngredientsList.Children.Count == 0)
        {
            // The user has deleted the last ingredient from the model
            // Disable Unitsliststuff and NotIncludedInMealstuff
            ShowHideUnitsListAmountInputAndUnitButtonsGrid(false);
            ShowHideNotIncludedInMealLabelAndAddToMealButton(false);

            // Set TitleInput
            TitleInput.IsTextHasChangedEventEnabled = false;
            TitleInput.text = "No Ingredient selected";
            TitleInput.IsTextHasChangedEventEnabled = true;
        }

        IngredientsList.Reposition();
        IngredientsList.Recenter();
    }

    private void OnAddIngredientToMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        if (_currentIngredientOfMeal == null || _currentIngredientOfMeal.IngredientGuid == Guid.Empty || _currentMeal == null) return;

        Ingredient ingredient;
        Model.Get(_currentIngredientOfMeal.IngredientGuid, out ingredient);

        // Enable Unitslist, Unit Buttons and AmountInput
        //ShowHideUnitsListAmountInputAndUnitButtonsGrid(true);

        Controller.AddIngredientToMeal(_currentMeal, ingredient, 0, null);
    }

    private void UpdateIngredientsList()
    {
        if (_updateIngredientsListJob.Running) _updateIngredientsListJob.Kill();
        _updateIngredientsListJob = Job.Make(UIListHelper.UpdateList(IngredientsList, Model));
    }

    private void OnTitleTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        if (_currentIngredientOfMeal == null) return;

        Ingredient ingredient;
        Model.Get(_currentIngredientOfMeal.IngredientGuid, out ingredient);

        Controller.SetNameOfIngredient(args.newText, ingredient);
    }

    #endregion // Ingredients

    private void OnAmountTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        float newAmount;
        if (!float.TryParse(args.newText, out newAmount)) return;

        // Is it a change?
        if (Math.Abs(newAmount - _currentIngredientOfMeal.Amount) < 0.0001f) return;

        // Is the current ingredient in the meal?
        if (!_currentMeal.ContainsIngredient(_currentIngredientOfMeal.IngredientGuid)) return;

        Ingredient ingredient;
        Model.Get(_currentIngredientOfMeal.IngredientGuid, out ingredient);

        Controller.SetAmountOfIngredientInMeal(_currentMeal, ingredient, newAmount);
        _currentIngredientOfMeal.Amount = newAmount;

        // Should Unit use plural?
        Unit unit;
        Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);
        unit.UsePlural = Math.Abs(_currentIngredientOfMeal.Amount - 1f) > 0.0001f;
    }

 
    private void OnViewsManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;

        UpdateUnitsList();
        UpdateIngredientsList();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            UpdateUnitsList();
            UpdateIngredientsList();

            // Check if we have a current unit
            if (_currentIngredientOfMeal.UnitGuid != Guid.Empty)
            {
                // Should Unit use plural?
                Unit unit;
                Model.Get(_currentIngredientOfMeal.UnitGuid, out unit);
                unit.UsePlural = Math.Abs(_currentIngredientOfMeal.Amount - 1f) > 0.0001f;
            }
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}