using System.Collections.ObjectModel;
using FoodRoulette;
using UnityEngine;


public class HomeView : FoodRouletteView
{
    private Meal _lastQuickMeal;

    public UIButton OptionsButton;
    public UIButton BrowseButton;
    public UIButton SearchButton;
    public UIButton PlannerButton;
    public UIButton QuickMealButton;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnScreenManagerNext;
        ViewsManager.NextEvent += OnScreenManagerNext;

        BackTextForNextView = "Home";

        var optionsScript = OptionsButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        optionsScript.TriggeredEvent -= OnOptionsButtonTriggered;
        optionsScript.TriggeredEvent += OnOptionsButtonTriggered;

        var browseScript = BrowseButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        browseScript.TriggeredEvent -= OnBrowseButtonTriggered;
        browseScript.TriggeredEvent += OnBrowseButtonTriggered;

        var searchScript = SearchButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        searchScript.TriggeredEvent -= OnSearchButtonTriggered;
        searchScript.TriggeredEvent += OnSearchButtonTriggered;

        var plannerScript = PlannerButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        plannerScript.TriggeredEvent -= OnPlannerButtonTriggered;
        plannerScript.TriggeredEvent += OnPlannerButtonTriggered;

        var quickMealScript = QuickMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        quickMealScript.TriggeredEvent -= OnQuickMealButtonTriggered;
        quickMealScript.TriggeredEvent += OnQuickMealButtonTriggered;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnPlannerButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Next<PlannerView>();
    }

    private void OnSearchButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Next<SearchView>();
    }

    private void OnBrowseButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Next<CategoriesView>();
    }

    private void OnOptionsButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Next<CreditsAndOptionsView>();
    }

    private void OnQuickMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        // Choose a random meal and show it in mealoverview
        ReadOnlyCollection<Meal> meals;
        Model.GetAll(out meals);

        if (meals.Count == 0) return;

        Meal meal;

        if (meals.Count > 1)
        {
            do
            {
                var index = Random.Range(0, meals.Count);
                meal = meals[index];
            } while (meal == _lastQuickMeal);
        }
        else
        {
            meal = meals[0];
        }

        _lastQuickMeal = meal;

        Controller.SetMealInMealOverview(meal);
        ViewsManager.Next<MealOverviewView>();
    }

    private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType()) return;
    }
}