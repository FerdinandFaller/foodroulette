using System;
using FoodRoulette;
using FoodRoulette.Controller;
using FoodRoulette.Model;
using UnityEngine;


public class FoodRouletteView : MonoBehaviour
{
    protected IFoodRouletteController Controller;
    protected IFoodRouletteModel Model;

    public string BackTextForNextView { get; protected set; }


    public virtual void Initialize(IFoodRouletteController controller, IFoodRouletteModel model)
    {
        if (controller == null) throw new ArgumentNullException("controller");
        if (model == null) throw new ArgumentNullException("model");

        Controller = controller;
        Model = model;

        BackTextForNextView = "Back";

        ViewsManager.JumpOut(gameObject, Vector3.zero);
        ViewsManager.AlphaOut(gameObject);
    }

    public virtual Vector3 GetBackgroundScale()
    {
        return Vector3.zero;
    }
}