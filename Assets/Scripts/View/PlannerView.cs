using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using FoodRoulette;
using UnityEngine;
using Random = UnityEngine.Random;


public class PlannerView : FoodRouletteView
{
    public UIButton BackButton;
    public UILabel TitleLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;
    public List<PlannerSlot> Slots;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        BackTextForNextView = "Planner";

        ViewsManager.NextEvent -= OnViewsManagerNext;
        ViewsManager.NextEvent += OnViewsManagerNext;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        Model.AllPlannerMealsDeSerialized -= OnModelDeserializedAllPlannerMeals;
        Model.AllPlannerMealsDeSerialized += OnModelDeserializedAllPlannerMeals;

        Model.MealWasSetInPlannerSlotEvent -= OnModelMealWasSetInPlannerSlot;
        Model.MealWasSetInPlannerSlotEvent += OnModelMealWasSetInPlannerSlot;

        Model.MealRemovedEvent -= OnModelRemovedMeal;
        Model.MealRemovedEvent += OnModelRemovedMeal;

        // Register for the Triggered Event of the UIMeals (User clicked on a UIMeal)
        foreach (var plannerSlot in Slots)
        {
            var script = plannerSlot.UiMeal.gameObject.GetComponent<UIButtonMessageWithEvent>();
            if (script != null)
            {
                script.TriggeredEvent -= OnUiMealInSlotTriggered;
                script.TriggeredEvent += OnUiMealInSlotTriggered;
            }

            var script2 = plannerSlot.PickMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
            if (script2 != null)
            {
                script2.TriggeredEvent -= OnPickMealButtonInSlotTriggered;
                script2.TriggeredEvent += OnPickMealButtonInSlotTriggered;
            }

            var script3 = plannerSlot.RandomizeMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
            if (script3 != null)
            {
                script3.TriggeredEvent -= OnRandomizeMealButtonInSlotTriggered;
                script3.TriggeredEvent += OnRandomizeMealButtonInSlotTriggered;
            }
        }
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelDeserializedAllPlannerMeals()
    {
        ReadOnlyCollection<Meal> plannerMeals;
        Model.GetAllPlannerMeals(out plannerMeals);

        for (int i = 0; i < plannerMeals.Count; i++)
        {
            SetMealInSlot(plannerMeals[i], i);
        }
    }

    private void OnModelRemovedMeal(Meal removedMeal)
    {
        var slot = Slots.FirstOrDefault(plannerSlot => plannerSlot.UiMeal.Content == removedMeal);
        if (slot != null)
        {
            slot.UiMeal.Content = null;
        }
    }

    private void OnModelMealWasSetInPlannerSlot(Meal meal, int slotIndex)
    {
        SetMealInSlot(meal, slotIndex);
    }

    private void SetMealInSlot(Meal meal , int slot)
    {
        if(slot < 0 || slot > 6) throw new ArgumentOutOfRangeException("slot");

        Slots[slot].UiMeal.Content = meal;
    }

    private void OnUiMealInSlotTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Logger.Log(new EventReceivingLog("PlannerView - OnUiMealInSlotTriggered"));

        AudioController.Play("ButtonClick");

        var uiMeal = ((UIButtonMessageWithEvent)sender).gameObject.GetComponent<UIMeal>();

        if (uiMeal.Content == null)
        {
            OnPickMealButtonInSlotTriggered(sender, args);
            return;
        }

        Controller.SetMealInMealOverview(uiMeal.Content);

        ViewsManager.Next<MealOverviewView>();
    }

    private void OnPickMealButtonInSlotTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        var go = ((UIButtonMessageWithEvent)sender).gameObject;
        var slot = NGUITools.FindInParents<PlannerSlot>(go);

        Controller.SetRequestedPlannerSlotIndex(slot.Index);

        ViewsManager.Next<PlannerMealPickerView>();
    }

    private void OnRandomizeMealButtonInSlotTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        var go = ((UIButtonMessageWithEvent)sender).gameObject;
        var slot = NGUITools.FindInParents<PlannerSlot>(go);

        ReadOnlyCollection<Meal> meals;
        Model.GetAll(out meals);

        var random = Random.Range(0, meals.Count);

        var meal = meals.ElementAt(random);

        Controller.SetMealInPlannerSlotIndex(meal, slot.Index);
    }

    private void OnViewsManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType()) return;

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
        TitleLabel.text = "Planner";
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}