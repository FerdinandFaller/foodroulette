﻿using System;
using System.Linq;
using FoodRoulette;
using UnityEngine;


public class CategoryAddEditView : FoodRouletteView
{
    private Job _updateMealsListJob;
    public UIMealsList MealsList;
    public UIButton BackButton;
    public UIButton AddNewMealButton;
    public UIInputWithExactEvent TitleInput;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnScreenManagerNext;
        ViewsManager.NextEvent += OnScreenManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        _updateMealsListJob = Job.Make(UIListHelper.UpdateList(MealsList, Model), false);

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        var addScript = AddNewMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        addScript.TriggeredEvent -= OnAddNewMealButtonTriggered;
        addScript.TriggeredEvent += OnAddNewMealButtonTriggered;

        Model.CategoryInCategoryAddEditWasSetEvent -= OnModelCategoryInCategoryAddEditWasSet;
        Model.CategoryInCategoryAddEditWasSetEvent += OnModelCategoryInCategoryAddEditWasSet;

        TitleInput.TextHasChangedEvent -= OnCategoryNameTextChanged;
        TitleInput.TextHasChangedEvent += OnCategoryNameTextChanged;

        Model.AllMealsDeSerializedEvent -= OnModelDeserializedAllMeals;
        Model.AllMealsDeSerializedEvent += OnModelDeserializedAllMeals;

        MealsList.AddedContentEvent -= OnMealsListAddedContent;
        MealsList.AddedContentEvent += OnMealsListAddedContent;
    }

    private void OnModelDeserializedAllMeals()
    {
        // Initial loading is over, now we can listen to adding and removing
        Model.MealAddedEvent -= OnModelMealAdded;
        Model.MealAddedEvent += OnModelMealAdded;

        Model.MealRemovedEvent -= OnModelRemovedMeal;
        Model.MealRemovedEvent += OnModelRemovedMeal;
    }

    private void OnModelMealAdded(Meal meal)
    {
        UpdateMeals();
    }

    private void OnModelRemovedMeal(Meal meal)
    {
        UpdateMeals();
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelCategoryInCategoryAddEditWasSet(Category category)
    {
        foreach (var uiMeal in MealsList.Contents)
        {
            // All Meals currently in the List: Check Checkbox state and checkoff according to the new category
            SetCheckedStateOfMeal(uiMeal.Content, category.ContainsMeal(uiMeal.Content), false);
        }

        category.NameChangedEvent -= OnCategoryNameChanged;
        category.NameChangedEvent += OnCategoryNameChanged;

        category.MealAddedEvent -= OnCategoryAddedMeal;
        category.MealAddedEvent += OnCategoryAddedMeal;

        category.MealRemovedEvent -= OnCategoryRemovedMeal;
        category.MealRemovedEvent += OnCategoryRemovedMeal;

        TitleInput.text = category.Name;
        BackTextForNextView = category.Name;
    }

    private void OnMealsListAddedContent(UIMeal uiMeal)
    {
        RegisterEventsAndCheckOff(uiMeal);
    }

    private void UpdateMeals()
    {
        Category category;
        Model.GetCategoryInCategoryAddEdit(out category);

        if (category == null) return;

        if (_updateMealsListJob.Running) _updateMealsListJob.Kill();
        _updateMealsListJob = Job.Make(UIListHelper.UpdateList(MealsList, Model));

        MealsList.ScrollToTop();
    }

    private void RegisterEventsAndCheckOff(UIMeal uiMeal)
    {
        // Register for events
        var selMeal = (UIMealWithCheckbox)uiMeal;
        if (selMeal == null) throw new NullReferenceException();
        if (selMeal.UiCheckBox == null) throw new NullReferenceException();
        selMeal.UiCheckBox.onStateChanged += OnMealCheckboxStateChanged;

        // Check off the meal if it is contained in the category
        Category category;
        Model.GetCategoryInCategoryAddEdit(out category);
        if (category != null) SetCheckedStateOfMeal(uiMeal.Content, category.ContainsMeal(uiMeal.Content), false);
    }

    private void OnAddNewMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Meal meal;
        Controller.AddNewMeal(out meal);

        Category category;
        Model.GetCategoryInCategoryAddEdit(out category);

        Controller.AddMealToCategory(meal, category);

        Controller.SetMealInMealAddEdit(meal);

        ViewsManager.Next<MealAddEditView>();
    }

    private void OnCategoryNameTextChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        Category category;
        Model.GetCategoryInCategoryAddEdit(out category);
        Controller.SetNameOfCategory(args.newText, category);
    }

    private void OnCategoryRemovedMeal(object sender, Category.MealEventArgs args)
    {
        SetCheckedStateOfMeal(args.Meal, false, false);
    }

    private void OnCategoryAddedMeal(object sender, Category.MealEventArgs args)
    {
        SetCheckedStateOfMeal(args.Meal, true, false);
    }

    private void SetCheckedStateOfMeal(Meal meal, bool shouldBeChecked, bool withEvent)
    {
        // Get all uiMeals
        foreach (var uiMeal in MealsList.Contents)
        {
            if (uiMeal.Content.Guid != meal.Guid) continue;

            var uiSelectableMeal = uiMeal as UIMealWithCheckbox;
            if (uiSelectableMeal == null) throw new ArgumentNullException();
            if (uiSelectableMeal.UiCheckBox.isChecked == shouldBeChecked) return;

            if (withEvent)
            {
                uiSelectableMeal.UiCheckBox.isChecked = shouldBeChecked;
            }
            else
            {
                uiSelectableMeal.UiCheckBox.SetWithoutEvent(shouldBeChecked);
            }
        }
    }
    
    private void OnMealCheckboxStateChanged(UICheckboxWithImprovedEvent sender, bool state)
    {
        // Get the Meal that belongs to this checkbox
        var meal = NGUITools.FindInParents<UIMealWithCheckbox>(sender.gameObject).Content;

        Category category;
        Model.GetCategoryInCategoryAddEdit(out category);
        if (state)
        {
            Controller.AddMealToCategory(meal, category);
        }
        else
        {
            Controller.RemoveMealFromCategory(meal, category);
        }
    }

    private void OnCategoryNameChanged(object sender, NameChangedEventArgs args)
    {
        TitleInput.text = args.NewName;
        BackTextForNextView = args.NewName;

        if (ViewsManager.PreviousView.GetType() == typeof(MealsOfCategoryView))
            BackButton.GetComponentInChildren<UILabel>().text = args.NewName;
    }

    private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            if (_updateMealsListJob.Running) _updateMealsListJob.Kill();

            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;

        UpdateMeals();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            UpdateMeals();
        }
        else
        {
            if (_updateMealsListJob.Running) _updateMealsListJob.Kill();
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}