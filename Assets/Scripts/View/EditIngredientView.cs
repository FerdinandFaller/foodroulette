﻿using FoodRoulette;
using UnityEngine;


public class EditIngredientView : FoodRouletteView
{
    public UIButton BackButton;
    public UIButton DeleteThisIngredientButton;
    public UIInputWithExactEvent NameInput;
    public UILabel TitleLabel;
    public UILabel NameLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;

    private Ingredient _currentIngredient;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnScreenManagerNext;
        ViewsManager.NextEvent += OnScreenManagerNext;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        Model.IngredientInEditIngredientWasSetEvent -= OnModelIngredientInEditIngredientWasSet;
        Model.IngredientInEditIngredientWasSetEvent += OnModelIngredientInEditIngredientWasSet;

        NameInput.TextHasChangedEvent -= OnNameTextHasChanged;
        NameInput.TextHasChangedEvent += OnNameTextHasChanged;

        var deleteScript = DeleteThisIngredientButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        deleteScript.TriggeredEvent -= OnDeleteThisIngredientButtonTriggered;
        deleteScript.TriggeredEvent += OnDeleteThisIngredientButtonTriggered;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelIngredientInEditIngredientWasSet(Ingredient ingredient)
    {
        if (_currentIngredient != null) _currentIngredient.NameChangedEvent -= OnIngredientNameChanged;

        _currentIngredient = ingredient;

        TitleLabel.text = ingredient.Name;
        BackTextForNextView = ingredient.Name;

        NameInput.text = ingredient.Name;

        ingredient.NameChangedEvent -= OnIngredientNameChanged;
        ingredient.NameChangedEvent += OnIngredientNameChanged;
    }

    private void OnDeleteThisIngredientButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Ingredient ingredient;
        Model.GetIngredientInEditIngredient(out ingredient);

        var label = "Do you want to permanently\ndelete the Ingredient\n'" + ingredient.Name + "'?";

        ViewsManager.ShowConfirmationView(label, false, OnDeleteThisIngredientButtonTriggeredConfirmed, null);
    }

    private void OnDeleteThisIngredientButtonTriggeredConfirmed(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Ingredient ingredient;
        Model.GetIngredientInEditIngredient(out ingredient);

        Controller.RemoveIngredient(ingredient);
        ViewsManager.Back();
    }

    private void OnNameTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
    {
        Ingredient ingredient;
        Model.GetIngredientInEditIngredient(out ingredient);

        Controller.SetNameOfIngredient(args.newText, ingredient);
    }

    private void OnIngredientNameChanged(object sender, NameChangedEventArgs nameChangedEventArgs)
    {
        TitleLabel.text = nameChangedEventArgs.NewName;
    }

    private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType()) return;

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}