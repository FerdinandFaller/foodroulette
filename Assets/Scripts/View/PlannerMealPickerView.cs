using FoodRoulette;
using UnityEngine;


public class PlannerMealPickerView : FoodRouletteView
{
    private Job _updateMealsListJob;

    public UIButton BackButton;
    public UILabel TitleLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;
    public UIMealsList MealsList;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        BackTextForNextView = "Meal Picker";

        ViewsManager.NextEvent -= OnViewsManagerNext;
        ViewsManager.NextEvent += OnViewsManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        MealsList.AddedContentEvent -= OnUiMealsListAddedUiMeal;
        MealsList.AddedContentEvent += OnUiMealsListAddedUiMeal;

        _updateMealsListJob = Job.Make(UIListHelper.UpdateList(MealsList, Model), false);

        Model.AllMealsDeSerializedEvent -= OnModelDeserializedAllMeals;
        Model.AllMealsDeSerializedEvent += OnModelDeserializedAllMeals;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelDeserializedAllMeals()
    {
        // Initial loading is over, now we can listen to adding and removing
        Model.MealAddedEvent -= OnModelMealAdded;
        Model.MealAddedEvent += OnModelMealAdded;

        Model.MealRemovedEvent -= OnModelMealRemoved;
        Model.MealRemovedEvent += OnModelMealRemoved;
    }

    private void UpdateMeals()
    {
        if (_updateMealsListJob.Running) _updateMealsListJob.Kill();
        _updateMealsListJob = Job.Make(UIListHelper.UpdateList(MealsList, Model));

        MealsList.ScrollToTop();
    }

    private void OnModelMealAdded(Meal obj)
    {
        UpdateMeals();
    }

    private void OnModelMealRemoved(Meal meal)
    {
        UpdateMeals();
    }

    private void OnUiMealsListAddedUiMeal(UIMeal addedMeal)
    {
        Logger.Log(new EventReceivingLog("PlannerMealPickerView - OnUiMealsListAddedUiMeal"));

        // Register for the Triggered Event of this UIMeal (User clicked on a UIMeal)
        var script = addedMeal.gameObject.GetComponent<UIButtonMessageWithEvent>();
        if (script != null)
        {
            script.TriggeredEvent -= OnUiMealInListTriggered;
            script.TriggeredEvent += OnUiMealInListTriggered;
        }
    }

    private void OnUiMealInListTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Logger.Log(new EventReceivingLog("PlannerMealPickerView - OnUiMealInListTriggered"));

        AudioController.Play("ButtonClick");

        var uiMeal = ((UIButtonMessageWithEvent)sender).gameObject.GetComponent<UIMeal>();
        int requestedPlannerSlot;
        Model.GetRequestedPlannerSlotIndex(out requestedPlannerSlot);

        Controller.SetMealInPlannerSlotIndex(uiMeal.Content, requestedPlannerSlot);

        ViewsManager.Back();
    }

    private void OnViewsManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            if (_updateMealsListJob.Running) _updateMealsListJob.Kill();

            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
        TitleLabel.text = "Meal Picker";

        UpdateMeals();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            UpdateMeals();
        }
        else
        {
            if (_updateMealsListJob.Running) _updateMealsListJob.Kill();
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}