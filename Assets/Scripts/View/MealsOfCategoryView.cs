﻿using System;
using FoodRoulette;
using UnityEngine;


public class MealsOfCategoryView : FoodRouletteView
{
    private Job _updateMealsOfCategoryListJob;
    public UIButton AddNewMealButton;
    public UIButton DeleteThisCategoryButton;
    public UIButton BackButton;
    public UIButton EditCategoryButton;
    public UILabel TitleLabel;
    public UIMealsList MealsList;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnScreenManagerNext;
        ViewsManager.NextEvent += OnScreenManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        Model.CategoryInMealsOfCategoryWasSetEvent -= OnModelCategoryInMealsOfCategoryWasSet;
        Model.CategoryInMealsOfCategoryWasSetEvent += OnModelCategoryInMealsOfCategoryWasSet;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        var editScript = EditCategoryButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        editScript.TriggeredEvent -= OnEditCategoryButtonTriggered;
        editScript.TriggeredEvent += OnEditCategoryButtonTriggered;

        var addScript = AddNewMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        addScript.TriggeredEvent -= OnAddNewMealButtonTriggered;
        addScript.TriggeredEvent += OnAddNewMealButtonTriggered;

        var deleteScript = DeleteThisCategoryButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        deleteScript.TriggeredEvent -= OnDeleteThisCategoryButtonTriggered;
        deleteScript.TriggeredEvent += OnDeleteThisCategoryButtonTriggered;

        MealsList.AddedContentEvent -= OnUiMealsListAddedUiMeal;
        MealsList.AddedContentEvent += OnUiMealsListAddedUiMeal;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelCategoryInMealsOfCategoryWasSet(Category category)
    {
        if (category == null) throw new ArgumentNullException("category");

        UpdateMealsOfCategoryList();

        category.MealAddedEvent -= OnCategoryMealAdded;
        category.MealAddedEvent += OnCategoryMealAdded;

        category.MealRemovedEvent -= OnCategoryMealRemoved;
        category.MealRemovedEvent += OnCategoryMealRemoved;

        category.NameChangedEvent -= OnCategoryNameChanged;
        category.NameChangedEvent += OnCategoryNameChanged;

        TitleLabel.text = category.Name;
        BackTextForNextView = category.Name;
    }

    private void OnUiMealsListAddedUiMeal(UIMeal addedMeal)
    {
        Logger.Log(new EventReceivingLog("MealsOfCategoryView - OnUiMealsListAddedUiMeal"));

        // Register for the Triggered Event of this UIMeal (User clicked on a UIMeal)
        var script = addedMeal.gameObject.GetComponent<UIButtonMessageWithEvent>();
        if (script != null)
        {
            script.TriggeredEvent -= OnUiMealInListTriggered;
            script.TriggeredEvent += OnUiMealInListTriggered;
        }
    }

    private void OnDeleteThisCategoryButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Category category;
        Model.GetCategoryInMealsOfCategory(out category);

        var label = "Do you want to permanently\ndelete the Category\n'" + category.Name + "'?";

        ViewsManager.ShowConfirmationView(label, false, OnDeleteThisCategoryButtonTriggeredConfirmed, null);
    }

    private void OnDeleteThisCategoryButtonTriggeredConfirmed(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Category category;
        Model.GetCategoryInMealsOfCategory(out category);

        Controller.RemoveCategory(category);
        ViewsManager.Back();
    }

    private void OnUiMealInListTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Logger.Log(new EventReceivingLog("MealsOfCategoryView - OnUiMealInListTriggered"));

        AudioController.Play("ButtonClick");

        var uiMeal = ((UIButtonMessageWithEvent)sender).gameObject.GetComponent<UIMeal>();

        Controller.SetMealInMealOverview(uiMeal.Content);

        ViewsManager.Next<MealOverviewView>();
    }

    private void OnAddNewMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Meal newMeal;
        Category category;

        Model.GetCategoryInMealsOfCategory(out category);

        Controller.AddNewMeal(out newMeal);
        Controller.AddMealToCategory(newMeal, category);
        Controller.SetMealInMealAddEdit(newMeal);

        ViewsManager.Next<MealAddEditView>();
    }

    private void OnEditCategoryButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Category category;
        Model.GetCategoryInMealsOfCategory(out category);

        Controller.SetCategoryInCategoryAddEdit(category);

        ViewsManager.Next<CategoryAddEditView>();
    }

    private void OnCategoryNameChanged(object sender, NameChangedEventArgs args)
    {
        TitleLabel.text = args.NewName;
        BackTextForNextView = args.NewName;
    }

    private void UpdateMealsOfCategoryList()
    {
        Category category;
        Model.GetCategoryInMealsOfCategory(out category);

        if (category == null) return;

        if (_updateMealsOfCategoryListJob != null && _updateMealsOfCategoryListJob.Running) _updateMealsOfCategoryListJob.Kill();
        _updateMealsOfCategoryListJob = Job.Make(UIListHelper.UpdateMealsOfCategoryList(MealsList, category, Model));
        
        MealsList.ScrollToTop();
    }

    private void OnCategoryMealAdded(object sender, Category.MealEventArgs args)
    {
        UpdateMealsOfCategoryList();
    }

    private void OnCategoryMealRemoved(object sender, Category.MealEventArgs args)
    {
        UpdateMealsOfCategoryList();
    }

    private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            if (_updateMealsOfCategoryListJob != null && _updateMealsOfCategoryListJob.Running) _updateMealsOfCategoryListJob.Kill();

            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;

        UpdateMealsOfCategoryList();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            UpdateMealsOfCategoryList();
        }
        else
        {
            if (_updateMealsOfCategoryListJob != null && _updateMealsOfCategoryListJob.Running) _updateMealsOfCategoryListJob.Kill();
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}