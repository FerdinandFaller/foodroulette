using System.Collections;
using UnityEngine;


public class LoadingIndicatorView : FoodRouletteView
{
    public UISprite Background;
    public UISprite Indicator;
    public UISprite BackgroundSprite;
    public UIAnchor Anchor;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);
    }

    void Start()
    {
        iTween.Init(Indicator.gameObject);
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    public IEnumerator Run(Vector2 relativeOffset)
    {
        Anchor.relativeOffset = relativeOffset;

        iTween.RotateBy(Indicator.gameObject, iTween.Hash("name", "LoadingIndicator", "amount", new Vector3(0, 0, -1), "speed", 200, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop, "ignoretimescale", true));
        
        yield return null;
    }

    public void Stop()
    {
        iTween.StopByName(Indicator.gameObject, "LoadingIndicator");
    }
}