using FF.JSONPrefs;
using FoodRoulette;
using UnityEngine;


public class LogoView : FoodRouletteView
{
	private const float _minDuration = 2;
	private bool _transitionToHome;
	private float _earliestLogoViewEndTime;
	private bool _isEverythingDeserialized;
	public UISprite LogoSprite;
	public UISprite BackgroundSprite;
	public UISprite KitchenSprite;


	public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
	{
		base.Initialize(controller, model);

		ViewsManager.NextEvent -= OnScreenManagerNext;
		ViewsManager.NextEvent += OnScreenManagerNext;

		model.EverythingDeSerializedEvent -= OnModelEverythingDeserialized;
		model.EverythingDeSerializedEvent += OnModelEverythingDeserialized;

		if (!JSONPrefs.GetBool("MuteMusic"))
		{
			AudioController.PlayMusicPlaylist();
		}

		if (JSONPrefs.GetBool("DisableSleepMode"))
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}
	}

	public override Vector3 GetBackgroundScale()
	{
		return BackgroundSprite.transform.localScale;
	}

	void Update()
	{
		if (!_transitionToHome && 
			_isEverythingDeserialized &&
			Time.time > _earliestLogoViewEndTime)
		{
			_transitionToHome = true;
			ViewsManager.Next<HomeView>(true, 1.5f);
		}
	}

	private void OnModelEverythingDeserialized()
	{
		_isEverythingDeserialized = true;

		_earliestLogoViewEndTime = Time.time + _minDuration;
	}

	private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
	{
		if (current == null) return;
		if (current.GetType() != GetType()) return;

		_earliestLogoViewEndTime = Time.time + _minDuration;
	}
}