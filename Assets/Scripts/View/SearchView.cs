using System;
using FoodRoulette;
using UnityEngine;


public class SearchView : FoodRouletteView
{
    private Job _showAllSearchResultsJob;

    public UIButton BackButton;
    public UILabel TitleLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;

    public UIButton SearchButton;
    public UIInputWithExactEvent SearchInput;

    public UISearchResultsList ResultsList;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        BackTextForNextView = "Search";

        ViewsManager.NextEvent -= OnViewsManagerNext;
        ViewsManager.NextEvent += OnViewsManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        var searchScript = SearchButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        searchScript.TriggeredEvent -= OnSearchButtonTriggered;
        searchScript.TriggeredEvent += OnSearchButtonTriggered;

        SearchInput.onSubmit += OnSearchInputSubmit;

        ResultsList.AddedContentEvent -= OnUiSearchResultsListAddedUiSearchResult;
        ResultsList.AddedContentEvent += OnUiSearchResultsListAddedUiSearchResult;
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnUiSearchResultsListAddedUiSearchResult(UISearchResult addedSearchResult)
    {
        Logger.Log(new EventReceivingLog("SearchView - OnUiSearchResultsListAddedUiSearchResult"));

        // Register for the Triggered Event of this UISearchResult (User clicked on a UISearchResult)
        var script = addedSearchResult.gameObject.GetComponent<UIButtonMessageWithEvent>();
        if (script != null)
        {
            script.TriggeredEvent -= OnUiSearchResultInListTriggered;
            script.TriggeredEvent += OnUiSearchResultInListTriggered;
        }
    }

    private void OnUiSearchResultInListTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Logger.Log(new EventReceivingLog("SearchView - OnUiSearchResultInListTriggered"));

        AudioController.Play("ButtonClick");

        var uiSearchResult = ((UIButtonMessageWithEvent)sender).gameObject.GetComponent<UISearchResult>();

        if (uiSearchResult.Content.GetType() == typeof(Category))
        {
            Controller.SetCategoryInMealsOfCategory(uiSearchResult.Content as Category);
            ViewsManager.Next<MealsOfCategoryView>();
        }
        else if (uiSearchResult.Content.GetType() == typeof(Meal))
        {
            Controller.SetMealInMealOverview(uiSearchResult.Content as Meal);
            ViewsManager.Next<MealOverviewView>();
        }
        else if (uiSearchResult.Content.GetType() == typeof(Ingredient))
        {
            var ingredient = uiSearchResult.Content as Ingredient;
            if (ingredient != null)
                Controller.SetIngredientInEditIngredientView(ingredient);
            ViewsManager.Next<EditIngredientView>();
        }
        else if (uiSearchResult.Content.GetType() == typeof(Unit))
        {
            Controller.SetUnitInUnitAddEdit(uiSearchResult.Content as Unit);
            ViewsManager.Next<UnitAddEditView>();
        }
    }

    private void OnSearchInputSubmit(string inputString)
    {
        Search(inputString);

        //SearchInput.text = "";
    }

    private void OnSearchButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Search(SearchInput.text);
    }

    private void Search(string searchParam)
    {
        if (string.IsNullOrEmpty(searchParam)) return;

        var results = Model.Search(searchParam);

        if (_showAllSearchResultsJob != null && _showAllSearchResultsJob.Running) _showAllSearchResultsJob.Kill();
        _showAllSearchResultsJob = Job.Make(UIListHelper.ShowAllSearchResults(ResultsList, results, Model));

        ResultsList.ScrollToTop();
    }

    private void OnViewsManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            if (_showAllSearchResultsJob != null && _showAllSearchResultsJob.Running) _showAllSearchResultsJob.Kill();

            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
        TitleLabel.text = "Search";

        SearchInput.text = "";
        ResultsList.Clear();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            Search(SearchInput.text);
        }
        else
        {
            if (_showAllSearchResultsJob != null && _showAllSearchResultsJob.Running) _showAllSearchResultsJob.Kill();
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}