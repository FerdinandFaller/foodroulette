using FF.JSONPrefs;
using FoodRoulette;
using UnityEngine;


public class CreditsAndOptionsView : FoodRouletteView
{
    public UIButton BackButton;
    public UILabel TitleLabel;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;
    public UILabel MusicByLabel;
    public UILabel MuteMusicLabel;
    public UICheckboxWithImprovedEvent MuteMusicCheckbox;
    public UILabel DisableSleepModeLabel;
    public UICheckboxWithImprovedEvent DisableSleepModeCheckbox;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        BackTextForNextView = "Options";

        ViewsManager.NextEvent -= OnViewsManagerNext;
        ViewsManager.NextEvent += OnViewsManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        MuteMusicCheckbox.onStateChanged += OnMuteMusicCheckboxStateChanged;
        MuteMusicCheckbox.SetWithoutEvent(JSONPrefs.GetBool("MuteMusic"));

        DisableSleepModeCheckbox.onStateChanged += OnDisableSleepModeCheckboxStateChanged;
        DisableSleepModeCheckbox.SetWithoutEvent(JSONPrefs.GetBool("DisableSleepMode"));
    }

    private static void OnMuteMusicCheckboxStateChanged(UICheckboxWithImprovedEvent sender, bool state)
    {
        var before = JSONPrefs.GetBool("MuteMusic");
        JSONPrefs.SetBool("MuteMusic", state);

        if (state)
        {
            AudioController.StopMusic(2);
        }
        else
        {
            if (before)
            {
                AudioController.PlayMusicPlaylist();
            }
        }
    }

    private static void OnDisableSleepModeCheckboxStateChanged(UICheckboxWithImprovedEvent sender, bool state)
    {
        var before = JSONPrefs.GetBool("DisableSleepMode");
        JSONPrefs.SetBool("DisableSleepMode", state);

        if (state)
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
        else
        {
            if (before)
            {
                Screen.sleepTimeout = SleepTimeout.SystemSetting;
            }
        }
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnViewsManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;
        TitleLabel.text = "Options";
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            
        }
        else
        {
            
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}