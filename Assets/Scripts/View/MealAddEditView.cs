﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using FoodRoulette;
using UnityEngine;


public class MealAddEditView : FoodRouletteView
{
	private Job _updateIngredientsListJob;
	private Job _updateCategoriesListJob;

	public UIButton BackButton;
	public UIContents Contents;
	public UIDividers Dividers;
	public Dictionary<UIDivider, GameObject> DividersToContents;
	public List<GameObject> ContentGameObjects;
	
	public UIIngredientsList IngredientsList;
	public UIInputWithExactEvent TitleInput;
	public UIInputWithExactEvent Recipe;
	public UICategoriesList CategoriesList;
	public UIButton AddIngredientButton;
	public UISprite BackgroundSprite;
	public UISprite KitchenSprite;


	public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
	{
		base.Initialize(controller, model);

		Dividers.Initialize();
		Contents.Initialize();

		DividersToContents = new Dictionary<UIDivider, GameObject>();
		foreach (var dividerKeyValue in Dividers.DividerDict)
		{
			foreach ( var contentKeyValue in Contents.ContentDict)
			{
				if(contentKeyValue.Key == dividerKeyValue.Key)
				{
					DividersToContents.Add(dividerKeyValue.Value, contentKeyValue.Value);
				}
			}
		}

		ViewsManager.NextEvent -= OnScreenManagerNext;
		ViewsManager.NextEvent += OnScreenManagerNext;

		ViewsManager.BackEvent -= OnViewsManagerBack;
		ViewsManager.BackEvent += OnViewsManagerBack;
		
		_updateIngredientsListJob = Job.Make(UIListHelper.UpdateList(CategoriesList, Model), false); // We dont have a meal yet, so we initialize the job with the wrong update method
		_updateCategoriesListJob = Job.Make(UIListHelper.UpdateList(CategoriesList, Model), false);

		var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
		backScript.TriggeredEvent -= OnBackButtonTriggered;
		backScript.TriggeredEvent += OnBackButtonTriggered;

		foreach (var dividerKeyValue in Dividers.DividerDict)
		{
			var script = dividerKeyValue.Value.gameObject.GetComponent<UIButtonMessageWithEvent>();
			script.TriggeredEvent -= OnDividerTriggered;
			script.TriggeredEvent += OnDividerTriggered;
		}

		var addScript = AddIngredientButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
		addScript.TriggeredEvent -= OnAddIngredientButtonTriggered;
		addScript.TriggeredEvent += OnAddIngredientButtonTriggered;

		Model.MealInMealAddEditWasSetEvent -= OnModelMealInMealAddEditWasSet;
		Model.MealInMealAddEditWasSetEvent += OnModelMealInMealAddEditWasSet;

		Model.AllCategoriesDeSerializedEvent -= OnModelDeserializedAllCategories;
		Model.AllCategoriesDeSerializedEvent += OnModelDeserializedAllCategories;

		Model.SelectedDividerInMealAddEditWasSetEvent -= OnModelSetSelectedDivider;
		Model.SelectedDividerInMealAddEditWasSetEvent += OnModelSetSelectedDivider;

		TitleInput.TextHasChangedEvent -= OnTitleTextHasChanged;
		TitleInput.TextHasChangedEvent += OnTitleTextHasChanged;

		Recipe.TextHasChangedEvent -= OnRecipeTextHasChanged;
		Recipe.TextHasChangedEvent += OnRecipeTextHasChanged;

		CategoriesList.AddedContentEvent -= OnCategoriesListAddedContent;
		CategoriesList.AddedContentEvent += OnCategoriesListAddedContent;

		IngredientsList.AddedContentEvent -= OnIngredientsListAddedContent;
		IngredientsList.AddedContentEvent += OnIngredientsListAddedContent;
	}

	public override Vector3 GetBackgroundScale()
	{
		return BackgroundSprite.transform.localScale;
	}

	private void OnModelDeserializedAllCategories()
	{
		// Initial loading is over, now we can listen to adding and removing

		Model.CategoryAddedEvent -= OnModelCategoryAdded;
		Model.CategoryAddedEvent += OnModelCategoryAdded;

		Model.CategoryRemovedEvent -= OnModelCategoryRemoved;
		Model.CategoryRemovedEvent += OnModelCategoryRemoved;
	}

	private void OnModelMealInMealAddEditWasSet(Meal meal)
	{
		foreach (var uiCategory in CategoriesList.Contents)
		{
			// All Categories currently in the List: Check Checkbox state and checkoff according to the new meal
			SetCheckedStateOfCategory(uiCategory.Content, uiCategory.Content.ContainsMeal(meal), false);
		}

		UpdateIngredientsOfMealList();

		meal.IngredientAddedEvent -= OnMealIngredientAdded;
		meal.IngredientAddedEvent += OnMealIngredientAdded;

		meal.IngredientRemovedEvent -= OnMealIngredientRemoved;
		meal.IngredientRemovedEvent += OnMealIngredientRemoved;

		meal.NameChangedEvent -= OnMealNameChanged;
		meal.NameChangedEvent += OnMealNameChanged;

		TitleInput.text = meal.Name;
		BackTextForNextView = meal.Name;

		Recipe.text = !string.IsNullOrEmpty(meal.Recipe) ? meal.Recipe : "";
	}

	private void OnMealNameChanged(object sender, NameChangedEventArgs args)
	{
		TitleInput.text = args.NewName;
		BackTextForNextView = args.NewName;
	}

	private void OnRecipeTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);
		Controller.SetRecipeOfMeal(args.newText, meal);
	}

	private void OnTitleTextHasChanged(object sender, UIInputWithExactEvent.InputTextHasChangedEventArgs args)
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);
		Controller.SetNameOfMeal(args.newText, meal);
	}

	private void OnCategoriesListAddedContent(UICategory uiCategory)
	{
		RegisterEventsAndCheckOff(uiCategory);
	}

	private void OnIngredientsListAddedContent(UIIngredient uiIngredient)
	{
		RegisterEvents(uiIngredient);
	}

	private void UpdateCategoriesList()
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		if (meal == null) return;
		if (_updateIngredientsListJob.Running) _updateIngredientsListJob.Kill();
		if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
		_updateCategoriesListJob = Job.Make(UIListHelper.UpdateList(CategoriesList, Model));

		CategoriesList.ScrollToTop();
	}
	
	private void UpdateIngredientsOfMealList()
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		if (meal == null) return;

		if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
		if (_updateIngredientsListJob.Running) _updateIngredientsListJob.Kill();
		_updateIngredientsListJob = Job.Make(UIListHelper.UpdateIngredientsOfMealList(IngredientsList, meal, Model));

		IngredientsList.ScrollToTop();
	}

	private void RegisterEvents(UIIngredient uiIngredient)
	{
		uiIngredient.Model = Model;

		// Register for events
		var selIng = (UIEditableAndDeletableIngredient)uiIngredient;

		var selIngEdit = selIng.EditButton.GetComponent<UIButtonMessageWithEvent>();
		selIngEdit.TriggeredEvent -= OnIngredientEditButtonTriggered;
		selIngEdit.TriggeredEvent += OnIngredientEditButtonTriggered;

		var selIngDel = selIng.DeleteButton.GetComponent<UIButtonMessageWithEvent>();
		selIngDel.TriggeredEvent -= OnIngredientRemoveFromMealButtonTriggered;
		selIngDel.TriggeredEvent += OnIngredientRemoveFromMealButtonTriggered;

		Meal meal;
		Model.GetMealInMealAddEdit(out meal);
		if (meal != null)
		{
			SetMealIBelongToOfUiIngredient(uiIngredient.Content, meal);
		}
	}

	private void RegisterEventsAndCheckOff(UICategory uiCategory)
	{
		// Register for events
		uiCategory.Content.MealAddedEvent -= OnCategoryAddedMeal;
		uiCategory.Content.MealAddedEvent += OnCategoryAddedMeal;
		uiCategory.Content.MealRemovedEvent -= OnCategoryRemovedMeal;
		uiCategory.Content.MealRemovedEvent += OnCategoryRemovedMeal;
		var selCat = (UISelectableCategory)uiCategory;
		selCat.UiCheckBox.onStateChanged += OnCategoryCheckboxStateChanged;

		Meal meal;
		Model.GetMealInMealAddEdit(out meal);
		if(meal != null) SetCheckedStateOfCategory(uiCategory.Content, uiCategory.Content.ContainsMeal(meal), false);
	}

	private void OnModelCategoryAdded(Category category)
	{
		UpdateCategoriesList();
	}

	private void OnModelCategoryRemoved(Category category)
	{
		UpdateCategoriesList();
	}

	private void OnAddIngredientButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		var ingredientOfMeal = new IngredientOfMeal();

		// Get all ingredients
		ReadOnlyCollection<Ingredient> modelIngredients;
		Model.GetAll(out modelIngredients);
		var list = new List<Ingredient>(modelIngredients);

		// Sort them alphabetically to mimic the UIList sorting
		list.Sort(UIListHelper.CompareINameablesAlphabetically);

		// Get the first that isn't in this meal
		var ingredient = list.FirstOrDefault(entry => !meal.ContainsIngredient(entry.Guid));

		if (ingredient == null)
		{
			// All Ingredients are part of this meal. We create a new one and use it.
			Controller.AddNewIngredient(out ingredient);
		}

		ingredientOfMeal.IngredientGuid = ingredient.Guid;
		ingredientOfMeal.Amount = 0;
		ingredientOfMeal.UnitGuid = Model.GetNoUnitUnit().Guid;

		Controller.SetMealAndIngredientOfMealInIngredientOfMealAddEdit(meal, ingredientOfMeal);

		ViewsManager.Next<IngredientOfMealAddEditView>();
	}

	#region Divider and Content Stuff

	private static void ShowContent(GameObject gameObject, bool show)
	{
		if (gameObject == null) throw new ArgumentNullException("gameObject");

		if (show)
		{
			ViewsManager.JumpIn(gameObject, Vector3.zero);
			ViewsManager.FadeIn(gameObject, 0.2f);
		}
		else
		{
			ViewsManager.JumpOut(gameObject, Vector3.zero);
			ViewsManager.AlphaOut(gameObject);
		}
	}

	private void OnDividerTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
	{
		// Which Divider?
		var dividerGo = ((UIButtonMessageWithEvent)sender).gameObject;
		var dividerScript = dividerGo.GetComponent<UIDivider>();

		if (dividerScript == null || !Dividers.DividerDict.ContainsValue(dividerScript)) return;

		foreach (KeyValuePair<string, UIDivider> keyValuePair in Dividers.DividerDict)
		{
			if (keyValuePair.Value == dividerScript)
			{
				Controller.SetSelectedDividerInMealAddEdit(keyValuePair.Key);
				return;
			}
		}
	}

	private void OnModelSetSelectedDivider(string dividerName)
	{
		SelectDivider(dividerName);
	}

	private void SelectDivider(string dividerName)
	{
		if (!Dividers.DividerDict.ContainsKey(dividerName)) return;
		var divider = Dividers.DividerDict[dividerName];

		if(Dividers.SelectDivider(divider))
		{
			switch (dividerName)
			{
				case "Categories":
					UpdateCategoriesList();
					break;
				case "Ingredients":
					UpdateIngredientsOfMealList();
					break;
				default:
					if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
					if (_updateIngredientsListJob.Running) _updateIngredientsListJob.Kill();
					break;
			}

			// Show the Content of the Divider
			foreach (KeyValuePair<UIDivider, GameObject> dividersToContent in DividersToContents)
			{
				ShowContent(dividersToContent.Value, dividersToContent.Key == divider);
			}
		}
	}

	#endregion

	private void OnIngredientEditButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
	{
		var ingredient = NGUITools.FindInParents<UIIngredient>(((UIButtonMessageWithEvent) sender).gameObject).Content;

		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		var amount = meal.GetIngredientAmount(ingredient.Guid);
		var unitGuid = meal.GetIngredientUnitGuid(ingredient.Guid);

		Controller.SetMealAndIngredientOfMealInIngredientOfMealAddEdit(meal, new IngredientOfMeal{Amount = amount, UnitGuid = unitGuid, IngredientGuid = ingredient.Guid});

		ViewsManager.Next<IngredientOfMealAddEditView>();
	}

	private void OnIngredientRemoveFromMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
	{
		var ingredient = NGUITools.FindInParents<UIIngredient>(((UIButtonMessageWithEvent)sender).gameObject).Content;

		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		if(meal.ContainsIngredient(ingredient.Guid)) Controller.RemoveIngredientFromMeal(meal, ingredient);
	}

	private void OnMealIngredientRemoved(object sender, Meal.IngredientEventArgs args)
	{
		UpdateIngredientsOfMealList();
	}

	private void OnMealIngredientAdded(object sender, Meal.IngredientEventArgs args)
	{
		UpdateIngredientsOfMealList();
	}
	
	private void SetMealIBelongToOfUiIngredient(Ingredient ingredient, Meal meal)
	{
		// Get all uiIngredients
		foreach (var uiIngredient in IngredientsList.Contents)
		{
			if (uiIngredient.Content.Guid != ingredient.Guid) continue;

			uiIngredient.MealIBelongTo = meal;
		}
	}

	private void OnCategoryCheckboxStateChanged(UICheckboxWithImprovedEvent sender, bool state)
	{
		// Get the Category that belongs to this checkbox
		var category = NGUITools.FindInParents<UISelectableCategory>(sender.gameObject).Content;

		Meal meal;
		Model.GetMealInMealAddEdit(out meal);
		if (state)
		{
			Controller.AddMealToCategory(meal, category);
		}
		else
		{
			Controller.RemoveMealFromCategory(meal, category);
		}
	}

	private void SetCheckedStateOfCategory(Category category, bool shouldBeChecked, bool withEvent = true)
	{
		// Get all uiCategories
		foreach (var uiCategory in CategoriesList.Contents)
		{
			if (uiCategory.Content.Guid != category.Guid) continue;

			var uiSelectableCategory = uiCategory as UISelectableCategory;
			if (uiSelectableCategory == null) throw new ArgumentNullException();
			if (uiSelectableCategory.UiCheckBox.isChecked == shouldBeChecked) return;

			if (withEvent)
			{
				uiSelectableCategory.UiCheckBox.isChecked = shouldBeChecked;
			}
			else
			{
				uiSelectableCategory.UiCheckBox.SetWithoutEvent(shouldBeChecked);
			}
		}
	}

	private void OnCategoryRemovedMeal(object sender, Category.MealEventArgs args)
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		// Was our Meal removed?
		if (meal != null && meal.Guid == args.Meal.Guid)
		{
			SetCheckedStateOfCategory(sender as Category, false, false);
		}
	}

	private void OnCategoryAddedMeal(object sender, Category.MealEventArgs args)
	{
		Meal meal;
		Model.GetMealInMealAddEdit(out meal);

		// Was our Meal added?
		if (meal != null && meal.Guid == args.Meal.Guid)
		{
			SetCheckedStateOfCategory(sender as Category, true, false);
		}
	}

	private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
	{
		if (current == null) return;
		if (current.GetType() != GetType())
		{
			if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
			if (_updateIngredientsListJob.Running) _updateIngredientsListJob.Kill();

			return;
		}

		var backText = previous.GetType() == typeof(MealOverviewView) ? "Overview" : previous.BackTextForNextView;

		BackButton.GetComponentInChildren<UILabel>().text = backText;

		// If no Divider is set, set "Categories"
		string dividerName;
		Model.GetSelectedDividerInMealAddEdit(out dividerName);
		if (string.IsNullOrEmpty(dividerName))
		{
			Controller.SetSelectedDividerInMealAddEdit("Categories");
		}
	}

	private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
	{
		if (current == null) return;

		if (current.GetType() == GetType())
		{
			UpdateCategoriesList();
			UpdateIngredientsOfMealList();
		}
		else
		{
			if (_updateCategoriesListJob.Running) _updateCategoriesListJob.Kill();
			if (_updateIngredientsListJob.Running) _updateIngredientsListJob.Kill();
		}
	}

	private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
	{
		ViewsManager.Back();
	}
}