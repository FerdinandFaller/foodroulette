﻿using System;
using FoodRoulette;
using UnityEngine;


public class MealOverviewView : FoodRouletteView
{
    private Job _updateIngredientsOfMealListJob;
    public UIButton BackButton;
    public UIButton EditMealButton;
    public UIButton DeleteThisMealButton;
    public UIIngredientsList IngredientsList;
    public UILabel IngredientsCaptionLabel;
    public UILabel RecipeCaptionLabel;
    public UILabel TitleLabel;
    public UIRecipe Recipe;
    public UISprite BackgroundSprite;
    public UISprite KitchenSprite;


    public override void Initialize(FoodRoulette.Controller.IFoodRouletteController controller, FoodRoulette.Model.IFoodRouletteModel model)
    {
        base.Initialize(controller, model);

        ViewsManager.NextEvent -= OnScreenManagerNext;
        ViewsManager.NextEvent += OnScreenManagerNext;

        ViewsManager.BackEvent -= OnViewsManagerBack;
        ViewsManager.BackEvent += OnViewsManagerBack;

        var backScript = BackButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        backScript.TriggeredEvent -= OnBackButtonTriggered;
        backScript.TriggeredEvent += OnBackButtonTriggered;

        Model.MealInMealOverviewWasSetEvent -= OnModelMealInMealOverviewWasSet;
        Model.MealInMealOverviewWasSetEvent += OnModelMealInMealOverviewWasSet;

        var editScript = EditMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        editScript.TriggeredEvent -= OnEditMealButtonTriggered;
        editScript.TriggeredEvent += OnEditMealButtonTriggered;

        var deleteScript = DeleteThisMealButton.gameObject.GetComponent<UIButtonMessageWithEvent>();
        deleteScript.TriggeredEvent -= OnDeleteThisMealButtonTriggered;
        deleteScript.TriggeredEvent += OnDeleteThisMealButtonTriggered;

        IngredientsList.AddedContentEvent -= OnUiIngredientsListAddedUiIngredient;
        IngredientsList.AddedContentEvent += OnUiIngredientsListAddedUiIngredient;

        // Register for the Triggered Event of the Recipe (User clicked on the Recipe)
        var recipeMessageWithEventScript = Recipe.gameObject.GetComponent<UIButtonMessageWithEvent>();
        if (recipeMessageWithEventScript != null)
        {
            recipeMessageWithEventScript.TriggeredEvent -= OnRecipeTriggered;
            recipeMessageWithEventScript.TriggeredEvent += OnRecipeTriggered;
        }
    }

    public override Vector3 GetBackgroundScale()
    {
        return BackgroundSprite.transform.localScale;
    }

    private void OnModelMealInMealOverviewWasSet(Meal meal)
    {
        if (_updateIngredientsOfMealListJob != null && _updateIngredientsOfMealListJob.Running) _updateIngredientsOfMealListJob.Kill();
        _updateIngredientsOfMealListJob = Job.Make(UIListHelper.UpdateIngredientsOfMealList(IngredientsList, meal, Model), false);

        foreach (var uiIngredient in IngredientsList.Contents)
        {
            // Update the mealIBelongTo Variable of each Ingredient
            uiIngredient.MealIBelongTo = meal;
        }

        meal.IngredientAddedEvent -= OnMealIngredientAdded;
        meal.IngredientAddedEvent += OnMealIngredientAdded;

        meal.IngredientRemovedEvent -= OnMealIngredientRemoved;
        meal.IngredientRemovedEvent += OnMealIngredientRemoved;

        meal.RecipeChangedEvent -= OnMealRecipeChanged;
        meal.RecipeChangedEvent += OnMealRecipeChanged;

        meal.NameChangedEvent -= OnMealNameChanged;
        meal.NameChangedEvent += OnMealNameChanged;

        TitleLabel.text = meal.Name;
        BackTextForNextView = meal.Name;

        Recipe.RecipeLabel.text = meal.Recipe;
    }

    private void OnUiIngredientsListAddedUiIngredient(UIIngredient addedIngredient)
    {
        Logger.Log(new EventReceivingLog("MealOverviewView - OnUiIngredientsListAddedUiIngredient"));

        // Register for the Triggered Event of this UIIngredient (User clicked on a UIIngredient)
        var script = addedIngredient.gameObject.GetComponent<UIButtonMessageWithEvent>();
        if (script != null)
        {
            script.TriggeredEvent -= OnUiIngredientInListTriggered;
            script.TriggeredEvent += OnUiIngredientInListTriggered;
        }

        // Set Meal I Belong to
        Meal meal;
        Model.GetMealInMealOverview(out meal);
        if (meal != null && addedIngredient.MealIBelongTo != meal)
        {
            addedIngredient.MealIBelongTo = meal;
        }
    }

    private void OnUiIngredientInListTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs triggeredEventArgs)
    {
        Logger.Log(new EventReceivingLog("MealOverviewView - OnUiIngredientInListTriggered"));

        AudioController.Play("ButtonClick");

        var uiIng = ((UIButtonMessageWithEvent)sender).gameObject.GetComponent<UIIngredient>();

        Meal meal;
        Model.GetMealInMealOverview(out meal);

        var amount = meal.GetIngredientAmount(uiIng.Content.Guid);
        var unitGuid = meal.GetIngredientUnitGuid(uiIng.Content.Guid);

        Controller.SetMealAndIngredientOfMealInIngredientOfMealAddEdit(meal, new IngredientOfMeal{Amount = amount, UnitGuid = unitGuid, IngredientGuid = uiIng.Content.Guid});

        ViewsManager.Next<IngredientOfMealAddEditView>();
    }

    private void OnRecipeTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs triggeredEventArgs)
    {
        Logger.Log(new EventReceivingLog("MealOverviewView - OnUiIngredientInListTriggered"));

        AudioController.Play("ButtonClick");

        Meal meal;
        Model.GetMealInMealOverview(out meal);

        Controller.SetMealInMealAddEdit(meal);
        Controller.SetSelectedDividerInMealAddEdit("Recipe");

        ViewsManager.Next<MealAddEditView>();
    }

    private void OnDeleteThisMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Meal meal;
        Model.GetMealInMealOverview(out meal);

        var label = "Do you want to permanently\ndelete the Meal\n'" + meal.Name + "'?";

        ViewsManager.ShowConfirmationView(label, false, OnDeleteThisMealButtonTriggeredConfirmed, null);
    }

    private void OnDeleteThisMealButtonTriggeredConfirmed(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        Meal meal;
        Model.GetMealInMealOverview(out meal);

        Controller.RemoveMeal(meal);
        ViewsManager.Back();
    }

    private void OnMealNameChanged(object sender, NameChangedEventArgs args)
    {
        TitleLabel.text = args.NewName;
        BackTextForNextView = args.NewName;
    }

    private void OnEditMealButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs e)
    {
        Logger.Log(new EventReceivingLog("MealOverviewView - OnEditMealButtonTriggered"));

        Meal meal;
        Model.GetMealInMealOverview(out meal);

        Controller.SetMealInMealAddEdit(meal);

        ViewsManager.Next<MealAddEditView>();
    }

    private void OnMealRecipeChanged(string recipe)
    {
        Recipe.RecipeLabel.text = recipe;
    }

    private void OnMealIngredientAdded(object sender, Meal.IngredientEventArgs args)
    {
        UpdateIngredientsOfMealList();
    }

    private void OnMealIngredientRemoved(object sender, Meal.IngredientEventArgs args)
    {
        UpdateIngredientsOfMealList();
    }

    private void UpdateIngredientsOfMealList()
    {
        Meal meal;
        Model.GetMealInMealOverview(out meal);

        if (meal == null) return;

        if (_updateIngredientsOfMealListJob != null && _updateIngredientsOfMealListJob.Running) _updateIngredientsOfMealListJob.Kill();
        _updateIngredientsOfMealListJob = Job.Make(UIListHelper.UpdateIngredientsOfMealList(IngredientsList, meal, Model));

        IngredientsList.ScrollToTop();
    }

    private void OnScreenManagerNext(FoodRouletteView previous, FoodRouletteView current)
    {
        if (current == null) return;
        if (current.GetType() != GetType())
        {
            if (_updateIngredientsOfMealListJob != null && _updateIngredientsOfMealListJob.Running) _updateIngredientsOfMealListJob.Kill();

            return;
        }

        BackButton.GetComponentInChildren<UILabel>().text = previous.BackTextForNextView;

        UpdateIngredientsOfMealList();
    }

    private void OnViewsManagerBack(FoodRouletteView oldCurrent, FoodRouletteView current)
    {
        if (current == null) return;

        if (current.GetType() == GetType())
        {
            UpdateIngredientsOfMealList();
        }
        else
        {
            if (_updateIngredientsOfMealListJob != null && _updateIngredientsOfMealListJob.Running) _updateIngredientsOfMealListJob.Kill();
        }
    }

    private void OnBackButtonTriggered(object sender, UIButtonMessageWithEvent.TriggeredEventArgs args)
    {
        ViewsManager.Back();
    }
}