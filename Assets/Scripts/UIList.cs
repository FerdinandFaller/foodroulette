using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using FoodRoulette;
using UnityEngine;
using System.Collections;


public class UIList<T, K> : MonoBehaviour where K : CategoryIngredientMealUnitBase where T : UICategoryIngredientMealUnitBase<K>
{
	private CenterOnChildWithEvent _centerOnChildScript;
	private UIGrid _gridScript;


	public ReadOnlyCollection<GameObject> Children
	{
		get
		{
			var list = new List<GameObject>();

			for (var i = 0; i < Grid.transform.childCount; i++)
			{
				list.Add(Grid.transform.GetChild(i).gameObject);
			}

			return list.AsReadOnly();
		}
	}

	public ReadOnlyCollection<T> Contents
	{
		get 
		{ 
			var list = Children.Select(child => child.GetComponent<T>()).Where(content => content != null).ToList();

			return list.AsReadOnly();
		}
	}

	public ReadOnlyCollection<K> InnerContents
	{
		get
		{
			var list = Contents.Select(content => content.Content).ToList();

			return list.AsReadOnly();
		}
	}

	public UIGrid Grid;
	public GameObject Prefab;

	public event Action<T> AddedContentEvent;
	public event Action<T> RemovedContentEvent;


	protected virtual void Awake () 
	{
		_centerOnChildScript = Grid.GetComponent<CenterOnChildWithEvent>();
		_gridScript = Grid.GetComponent<UIGrid>();

		// Remove all Template Childs
		for (var i = 0; i < Grid.transform.childCount; i++)
		{
			Destroy(Grid.transform.GetChild(i).gameObject);
		}
	}

	public T Add(K innerContent, bool useWaitCoRoutine = true)
	{
		if (innerContent == null) throw new ArgumentNullException("innerContent");

		var go = NGUITools.AddChild(Grid.gameObject, Prefab);

		Logger.Log(new Log("UIList - Add - GO Instantiation"));

		var content = go.GetComponent<T>();
		content.Content = innerContent;

		if (AddedContentEvent != null)
		{
			Logger.Log(new EventFiringLog("UIList - Add - AddedContentEvent - go: " + go));
			AddedContentEvent(content);
		}

		if (useWaitCoRoutine) StartCoroutine_Auto(WaitCoRoutine());

		return content;
	}

	public bool Add(T content, bool useWaitCoRoutine = true)
	{
		if(content == null) throw new ArgumentNullException("content");

		// Already in the list?
		if (Contents.Contains(content))
		{
			// Yes
			// Active?
			if (content.gameObject.activeSelf)
			{
				// Yes
				return false;
			}
			else
			{
				// No
				content.gameObject.SetActive(true);

				Logger.Log(new Log("UIList - Add - GO Activation"));

				if (AddedContentEvent != null)
				{
					Logger.Log(new EventFiringLog("UIList - Add - AddedContentEvent - content: " + content));
					AddedContentEvent(content);
				}

				if (useWaitCoRoutine) StartCoroutine_Auto(WaitCoRoutine());

				return true;
			}
		}

		// No

		var t = content.transform;
		t.parent = Grid.transform;
		t.localPosition = Vector3.zero;
		t.localScale = Vector3.one;
		t.localRotation = Quaternion.identity;
		content.gameObject.layer = Grid.gameObject.layer;
		content.gameObject.SetActive(true);

		return true;
	}

	public void Remove(T content, bool useWaitCoRoutine = true)
	{
		if(content == null) throw new ArgumentNullException("content");

		if (!Contents.Contains(content)) return;

		Remove(content.gameObject, useWaitCoRoutine);
	}

	public void Remove(GameObject go, bool useWaitCoRoutine = true)
	{
		if (go == null) throw new ArgumentNullException("go");

		if (!Children.Contains(go)) return;

		/// TODO: Feature: Deactivating instead of Destroying

		NGUITools.Destroy(go);

		if (RemovedContentEvent != null)
		{
			Logger.Log(new EventFiringLog("UIList - Remove - RemovedContentEvent - go: " + go));
			var content = go.GetComponent<T>();
			RemovedContentEvent(content);
		}

		if (useWaitCoRoutine) StartCoroutine_Auto(WaitCoRoutine());
	}

	public void Remove(K innerContent, bool useWaitCoRoutine = true)
	{
		if(innerContent == null) throw new ArgumentNullException("innerContent");

		foreach (var content in Contents)
		{
			if (content.Content == innerContent)
			{
				Remove(content, useWaitCoRoutine);
				return;
			}
		}
	}

	public void Clear(bool useWaitCoRoutine = true)
	{
		Logger.Log(new Log("UIList - Clear"));

		// Remove all GOs in the Grid
		var children = Children;
		for (var i = Grid.transform.childCount - 1; i > -1; i--)
		{
			Remove(children[i], false);
		}

		if (useWaitCoRoutine) StartCoroutine_Auto(WaitCoRoutine());
	}
	
	public void StartWaitCoroutine()
	{
		Logger.Log(new Log("UIList - StartWaitCoRoutine"));

		StartCoroutine_Auto(WaitCoRoutine());
	}

	protected IEnumerator WaitCoRoutine()
	{
		Reposition();
		Recenter();

		yield return null;
	}

	public void Reposition()
	{
		if (Grid == null) return;
		if (_gridScript == null) return;

		Logger.Log(new Log("UIList - Reposition"));
		_gridScript.Reposition();
	}

	public void Recenter()
	{
		if (_centerOnChildScript == null) return;

		Logger.Log(new Log("UIList - Recenter"));
		_centerOnChildScript.Recenter();
	}

	public void ScrollToTop()
	{
		var panel = gameObject.GetComponent<UIPanel>();
		var target = ((panel.clipRange.w * 0.5f) - (Grid.cellHeight * 0.5f) - panel.clipSoftness.y);
		SpringPanel.Begin(gameObject, new Vector3(gameObject.transform.localPosition.x, target, gameObject.transform.localPosition.z), 10);
	}

	public void ScrollToBottom()
	{
		var panel = gameObject.GetComponent<UIPanel>();
		var target = (panel.clipRange.w + (Grid.cellHeight * 0.5f) + panel.clipSoftness.y);
		SpringPanel.Begin(gameObject, new Vector3(gameObject.transform.localPosition.x, target, gameObject.transform.localPosition.z), 10);
	}

	public void CenterOn(GameObject go)
	{
		// Check if the GO is in the list
		if (!Children.Contains(go)) return;

		//var panel = gameObject.GetComponent<UIPanel>();
		var target = go.transform.localPosition.y;
		var temp = SpringPanel.Begin(gameObject, new Vector3(gameObject.transform.localPosition.x, -target, gameObject.transform.localPosition.z), 10);
	    temp.onFinished += OnCenterOnFinished;
	}

    private void OnCenterOnFinished()
    {
        //Recenter();
        StartWaitCoroutine();
    }

	public void CenterOn(T content)
	{
		CenterOn(content.gameObject);
	}

	public void CenterOn(K innerContent)
	{
        if (innerContent == null)
        {
            Debug.LogError("innercontent == null");
            return;
        }

		foreach (var content in Contents.Where(content => content.Content == innerContent))
		{
			CenterOn(content.gameObject);
			return;
		}
	}

    public GameObject GetCenteredGameObject()
    {
        if (_centerOnChildScript == null) return null;

        return _centerOnChildScript.centeredObject;
    }
}
