using UnityEngine;

public class UIDivider : MonoBehaviour
{
    public string Name;
    public UILabel Label;
    public UISprite SpriteComplete;
    public int Index;

    private UIStretch _uiStretch;


    void Start()
    {
        _uiStretch = SpriteComplete.GetComponent<UIStretch>();
    }

    private void OnDividerTweenSizeUpdate(float value)
    {
        _uiStretch.relativeSize.y = value;
        Label.MarkAsChanged();
    }
}
