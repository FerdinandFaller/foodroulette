using FoodRoulette;


public class UICategory : UICategoryIngredientMealUnitBase<Category>
{/*
    private void OnDataManagerRemovedCategory(Category removedCategory)
    {
        Logger.Log(new EventReceivingLog("UICategory - OnDataManagerRemovedCategory - removedCategory: " + removedCategory));

        if (removedCategory != Content) return;

        NGUITools.Destroy(gameObject);
    }*/

    public override void RefreshLabelText()
    {
        NameLabel.text = Content.Name;
    }
}