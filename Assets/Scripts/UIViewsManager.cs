﻿using System.Collections.Generic;
using FoodRoulette;
using UnityEngine;


public class UIViewsManager : MonoBehaviour
{
    public List<FoodRouletteView> Views;


    private void Awake()
    {
        ViewsManager.Views = Views;
    }

    private void Start()
    {
        ViewsManager.Next<LogoView>(false, 0, 0, 0);
    }

#if UNITY_ANDROID
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            AudioController.Play("ButtonClick");
            if (ViewsManager.HasPreviousView)
            {
                ViewsManager.Back();
            }
            else
            {
                Application.Quit();
            }
        }
    }
#endif
}