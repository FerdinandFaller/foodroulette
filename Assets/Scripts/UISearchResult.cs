using System;
using FoodRoulette;


public class UISearchResult : UICategoryIngredientMealUnitBase<CategoryIngredientMealUnitBase>
{
    /*
    private Type _fullType;
    public Type FullType
    {
        get { return _fullType; }
        set
        {
            _fullType = value;

            RefreshLabelText();
        }
    }
    */

    public void AddTypeToNameLabel()
    {
        RefreshLabelText();
    }

    public override void RefreshLabelText()
    {
        NameLabel.text = Content.Name;

        if (Content != null) NameLabel.text = Content.GetType().Name + ": " + NameLabel.text;
    }
}