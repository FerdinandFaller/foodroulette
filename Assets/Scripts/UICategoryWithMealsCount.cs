using System.Globalization;
using FoodRoulette;


public class UICategoryWithMealsCount : UICategory
{
    public UILabel MealsCountLabel;


    protected override void Start()
    {
        base.Start();

        if (Content != null)
        {
            Content.MealAddedEvent -= OnMealAdded;
            Content.MealAddedEvent += OnMealAdded;
            Logger.Log(new EventSubscriptionLog("UICategory - Start - Content.MealAddedEvent += OnMealAdded"));

            Content.MealRemovedEvent -= OnMealRemoved;
            Content.MealRemovedEvent += OnMealRemoved;
            Logger.Log(new EventSubscriptionLog("UICategory - Start - Content.MealRemovedEvent += OnMealRemoved"));
        }

        UpdateMealsCountLabel();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        UpdateMealsCountLabel();
    }

    private void UpdateMealsCountLabel()
    {
        if (Content != null) MealsCountLabel.text = Content.MealsCount.ToString(CultureInfo.InvariantCulture);
    }

    private void OnMealAdded(object sender, Category.MealEventArgs mealEventArgs)
    {
        UpdateMealsCountLabel();
    }

    private void OnMealRemoved(object sender, Category.MealEventArgs mealEventArgs)
    {
        UpdateMealsCountLabel();
    }
}