using UnityEngine;


public class UIRecipe : MonoBehaviour
{
    public UILabel RecipeLabel;

    private UIMeal _uiMeal;
    public UIMeal UiMeal
    {
        get { return _uiMeal; }
        set
        {
            if (_uiMeal == value) return;

            _uiMeal = value;

            RecipeLabel.text = _uiMeal.Content.Recipe;
        }
    }
}
