﻿using UnityEngine;

public class LoadingIndicator : MonoBehaviour
{
    private static LoadingIndicator _instance;
    public static LoadingIndicator Instance
    {
        get
        {
            if (_instance == null)
            {
                var go = new GameObject("LoadingIndicator");
                _instance = go.AddComponent<LoadingIndicator>();
            }

            return _instance;
        }
        set { _instance = value; }
    }

    private GameObject _indicatorGo;
    private TimedTrailRenderer _trailRenderer;
    private const float Offset = 0.2f;
    

    void Start()
    {
        iTween.Init(gameObject);

        transform.localPosition += new Vector3(0,0,1);

        _indicatorGo = new GameObject("LoadingIndicator");
        _trailRenderer = _indicatorGo.AddComponent<TimedTrailRenderer>();
        _trailRenderer.layer = 8;
        _indicatorGo.transform.parent = transform;
        _indicatorGo.transform.localPosition = new Vector3(0, Offset, 0);

        // Set Up TrailRenderer
        _trailRenderer.material = (Material)Resources.Load("Materials/LoadingIndicatorMaterial", typeof(Material));
        var colors = new Color[2];
        colors[0] = new Color(1, 1, 1, 0.9f);
        colors[1] = new Color(1, 1, 1, 0);
        _trailRenderer.colors = colors;

        var sizes = new float[1];
        sizes[0] = Offset;
        _trailRenderer.sizes = sizes;

        _trailRenderer.minVertexDistance = 0.01f;
        _trailRenderer.maxRebuildTime = 0.01f;
        _trailRenderer.lifeTime = 2;
    }

    public void Run()
    {
        Run(transform.position);
    }

    public void Run(Vector3 position)
    {
        transform.position = position;

        iTween.RotateBy(gameObject, iTween.Hash("name", "LoadingIndicator", "amount", new Vector3(0, 0, -1), "speed", 100, "easetype", iTween.EaseType.easeInOutSine, "looptype", iTween.LoopType.loop, "ignoretimescale", true));
    }

    public void Stop()
    {
        iTween.StopByName(gameObject, "LoadingIndicator");
    }
}
