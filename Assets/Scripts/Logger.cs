using System;
using System.Collections.ObjectModel;
using UnityEngine;


namespace FoodRoulette
{
    public enum PrintSeverity
    {
        Log,
        LogWarning,
        LogError,
    }

    #region Log Classes
    public class Log
    {
        public string Message { get; protected set; }
        public PrintSeverity Severity { get; protected set; }


        public Log(string message, PrintSeverity severity = PrintSeverity.Log)
        {
            Message = message;
            Severity = severity;
        }
    }

    public class EventLog : Log
    {
        public EventLog(string message, PrintSeverity severity = PrintSeverity.Log)
            : base(message, severity)
        {
        }
    }

    class EventUnSubscriptionLog : EventLog
    {
        public EventUnSubscriptionLog(string message, PrintSeverity severity = PrintSeverity.Log)
            : base(message, severity)
        {
        }
    }

    public class EventSubscriptionLog : EventLog
    {
        public EventSubscriptionLog(string message, PrintSeverity severity = PrintSeverity.Log)
            : base(message, severity)
        {
        }
    }

    public class EventFiringLog : EventLog
    {
        public EventFiringLog(string message, PrintSeverity severity = PrintSeverity.Log)
            : base(message, severity)
        {
        }
    }

    class EventReceivingLog : EventLog
    {
        public EventReceivingLog(string message, PrintSeverity severity = PrintSeverity.Log)
            : base(message, severity)
        {
        }
    }
    #endregion // Log Classes

    public static class Logger
    {
        public static Collection<Type> Filters;

        static Logger()
        {
            Filters = new Collection<Type>();

            //Filters.Add(typeof(EventSubscriptionLog));
            //Filters.Add(typeof(EventUnSubscriptionLog));
            //Filters.Add(typeof(EventFiringLog));
            //Filters.Add(typeof(EventLog));
            //Filters.Add(typeof(Log));
        }

        public static void Log(Log log)
        {
            if (log == null || string.IsNullOrEmpty(log.Message)) throw new ArgumentNullException("log");

            if (Filters.Contains(log.GetType())) return;

            switch (log.Severity)
            {
                case PrintSeverity.Log:
                    {
                        Debug.Log(log.GetType() + " " + log.Message);
                    }
                    break;

                case PrintSeverity.LogWarning:
                    {
                        Debug.LogWarning(log.GetType() + " " + log.Message);
                    }
                    break;

                case PrintSeverity.LogError:
                    {
                        Debug.LogError(log.GetType() + " " + log.Message);
                    }
                    break;
            }
        }
    }
}