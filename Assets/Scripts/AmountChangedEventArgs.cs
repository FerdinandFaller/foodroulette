using System;

namespace FoodRoulette
{
    public class AmountChangedEventArgs : EventArgs
    {
        public float OldAmount;
        public float NewAmount;
    }
}