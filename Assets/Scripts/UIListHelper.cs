using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using FoodRoulette.Model;
using UnityEngine;


namespace FoodRoulette
{
    public static class UIListHelper
    {
        public static int CompareINameablesAlphabetically(INameable a, INameable b)
        {
            return String.Compare(a.Name, b.Name, StringComparison.Ordinal);
        }

        public static IEnumerator UpdateList(UICategoriesList list, IFoodRouletteModel model)
        {
            ReadOnlyCollection<Category> modelCategories;
            model.GetAll(out modelCategories);

            var listCategories = list.InnerContents;

            // Remove entries from the list that shouldn't be in there
            for (var index = listCategories.Count - 1; index >= 0; index--)
            {
                if (!modelCategories.Contains(listCategories[index]))
                {
                    list.Remove(listCategories[index]);
                }
            }

            // Create a list of Categories that need to be added to the list
            var missingCategories = modelCategories.Where(category => !listCategories.Contains(category)).ToList();

            if (missingCategories.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - UpdateList - Model has no missing Categories, can't add any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            missingCategories.Sort(CompareINameablesAlphabetically);

            foreach (var category in missingCategories)
            {
                list.Add(category);

                yield return new WaitForSeconds(0.1f);
            }

            list.StartWaitCoroutine();
        }

        public static IEnumerator UpdateList(UIMealsList list, IFoodRouletteModel model)
        {
            ReadOnlyCollection<Meal> modelMeals;
            model.GetAll(out modelMeals);

            var listMeals = list.InnerContents;

            // Remove entries from the list that shouldn't be in there
            for (var index = listMeals.Count - 1; index >= 0; index--)
            {
                if (!modelMeals.Contains(listMeals[index]))
                {
                    list.Remove(listMeals[index]);
                }
            }

            // Create a list of Meals that need to be added to the list
            var missingMeals = modelMeals.Where(meal => !listMeals.Contains(meal)).ToList();

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            missingMeals.Sort(CompareINameablesAlphabetically);

            if (missingMeals.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - UpdateList - Model has no missing Meals, can't add any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            foreach (var meal in missingMeals)
            {
                list.Add(meal);

                yield return new WaitForSeconds(0.1f);
            }

            list.StartWaitCoroutine();
        }

        public static IEnumerator UpdateList(UIIngredientsList list, IFoodRouletteModel model)
        {
            ReadOnlyCollection<Ingredient> modelIngredients;
            model.GetAll(out modelIngredients);

            var listIngredients = list.InnerContents;

            // Remove entries from the list that shouldn't be in there
            for (var index = listIngredients.Count - 1; index >= 0; index--)
            {
                if (!modelIngredients.Contains(listIngredients[index]))
                {
                    list.Remove(listIngredients[index]);
                }
            }

            // Create a list of Ingredients that need to be added to the list
            var missingIngredients = modelIngredients.Where(ingredient => !listIngredients.Contains(ingredient)).ToList();

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            missingIngredients.Sort(CompareINameablesAlphabetically);

            if (missingIngredients.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - UpdateList - Model has no missing Ingredients, can't add any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            foreach (var ingredient in missingIngredients)
            {
                list.Add(ingredient).Model = model;

                yield return new WaitForSeconds(0.1f);
            }

            list.StartWaitCoroutine();
        }

        public static IEnumerator UpdateList(UIUnitsList list, IFoodRouletteModel model)
        {
            ReadOnlyCollection<Unit> modelUnits;
            model.GetAll(out modelUnits);

            var listUnits = list.InnerContents;

            // Remove entries from the list that shouldn't be in there
            for (var index = listUnits.Count - 1; index >= 0; index--)
            {
                if (!modelUnits.Contains(listUnits[index]))
                {
                    list.Remove(listUnits[index]);
                }
            }

            // Create a list of Units that need to be added to the list
            var missingUnits = modelUnits.Where(unit => !listUnits.Contains(unit)).ToList();

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            missingUnits.Sort(CompareINameablesAlphabetically);

            if (missingUnits.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - UpdateList - Model has no missing Units, can't add any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            foreach (var unit in missingUnits)
            {
                list.Add(unit);

                yield return new WaitForSeconds(0.1f);
            }

            //list.StartWaitCoroutine();
        }

        public static IEnumerator UpdateMealsOfCategoryList(UIMealsList list, Category category, IFoodRouletteModel model)
        {
            var listMeals = list.InnerContents;
            var categoryMeals = category.GetReadOnlyMealGuids();

            // Remove entries from the list that shouldn't be in there
            for (var index = listMeals.Count - 1; index >= 0; index--)
            {
                if (!categoryMeals.Contains(listMeals[index].Guid))
                {
                    list.Remove(listMeals[index]);
                }
            }

            var missingMeals = new List<Meal>();

            // Create a list of Meals that need to be added to the list
            foreach (var categoryMealGuid in categoryMeals)
            {
                Meal meal;
                model.Get(categoryMealGuid, out meal);

                if (!listMeals.Contains(meal))
                {
                    missingMeals.Add(meal);
                }
            }

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            missingMeals.Sort(CompareINameablesAlphabetically);

            if (missingMeals.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - UpdateMealsOfCategoryList - Category has no missing Meals, can't add any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            foreach (var missingMeal in missingMeals)
            {
                try
                {
                    list.Add(missingMeal);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Logger.Log(new Log("UIListHelper - UpdateMealsOfCategoryList - Catched ArgumentOutOfRangeException", PrintSeverity.LogError));
                }

                yield return new WaitForSeconds(0.1f);
            }

            list.StartWaitCoroutine();
        }

        public static IEnumerator UpdateIngredientsOfMealList(UIIngredientsList list, Meal meal, IFoodRouletteModel model)
        {
            var listIngredients = list.InnerContents;
            var mealIngredients = meal.GetReadOnlyIngredientGuids();

            // Remove entries from the list that shouldn't be in there
            for (var index = listIngredients.Count - 1; index >= 0; index--)
            {
                if (!mealIngredients.Contains(listIngredients[index].Guid))
                {
                    list.Remove(listIngredients[index]);
                }
            }

            var missingIngredients = new List<Ingredient>();

            // Create a list of Ingredients that need to be added to the list
            foreach (var mealIngredient in mealIngredients)
            {
                Ingredient ingredient;
                model.Get(mealIngredient, out ingredient);

                if (!listIngredients.Contains(ingredient))
                {
                    missingIngredients.Add(ingredient);
                }
            }

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            missingIngredients.Sort(CompareINameablesAlphabetically);

            if (missingIngredients.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - UpdateIngredientsOfMealList - Meal has no missing Ingredients, can't add any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            foreach (var uiIngredient in list.Contents)
            {
                if (uiIngredient != null && uiIngredient.MealIBelongTo != meal)
                {
                    uiIngredient.MealIBelongTo = meal;
                }
            }

            foreach (var missingIngredient in missingIngredients)
            {
                var uiIngredient = list.Add(missingIngredient);

                uiIngredient.Model = model;
                uiIngredient.MealIBelongTo = meal;

                yield return new WaitForSeconds(0.1f);
            }

            list.StartWaitCoroutine();
        }

        public static IEnumerator ShowAllSearchResults(UISearchResultsList list, Dictionary<Guid, Type> results, IFoodRouletteModel model)
        {
            list.Clear(false);

            if (results.Count <= 0)
            {
                Logger.Log(new Log("UIListHelper - ShowAllSearchResults - No search results given, can't show any therefore!", PrintSeverity.LogWarning));
                yield return null;
            }

            var toAdd = new List<CategoryIngredientMealUnitBase>();

            foreach (KeyValuePair<Guid, Type> keyValuePair in results)
            {
                try
                {
                    if (keyValuePair.Value == typeof(Category))
                    {
                        Category category;
                        model.Get(keyValuePair.Key, out category);
                        toAdd.Add(category);
                    }
                    else if (keyValuePair.Value == typeof(Meal))
                    {
                        Meal meal;
                        model.Get(keyValuePair.Key, out meal);
                        toAdd.Add(meal);
                    }
                    else if (keyValuePair.Value == typeof(Ingredient))
                    {
                        Ingredient ingredient;
                        model.Get(keyValuePair.Key, out ingredient);
                        toAdd.Add(ingredient);
                    }
                    else if (keyValuePair.Value == typeof(Unit))
                    {
                        Unit unit;
                        model.Get(keyValuePair.Key, out unit);
                        toAdd.Add(unit);
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    Logger.Log(new Log("UIListHelper - ShowAllSearchResults - Catched ArgumentOutOfRangeException", PrintSeverity.LogError));
                }
            }

            //Sort the List alphabetically to use the same sorting as the grid does and thus show the user the first entries in the list first.
            toAdd.Sort(CompareINameablesAlphabetically);

            foreach (var categoryIngredientMealUnitBase in toAdd)
            {
                list.Add(categoryIngredientMealUnitBase).AddTypeToNameLabel();

                yield return new WaitForSeconds(0.1f);
            }

            list.StartWaitCoroutine();
        }
    }
}