#import "AdMobPlugin.h"

#pragma mark -
#pragma mark Script to native bridge

static NSString* CreateNSString(const char* string);

extern UIViewController* UnityGetGLViewController();


extern "C"
{
	//static AdMobPlugin *_adMobPlugin;
	static GADBannerView *_adMobView;
	static GADRequest *_request;
	static UIViewController *_rootViewController;
	static NSString* _publisherId;
    
	
	void CreateAdMobView()
	{
		_adMobView = [[GADBannerView alloc] init];
		_adMobView.rootViewController = _rootViewController;
        [_rootViewController.view addSubview:_adMobView];
		
		//[_adMobView setDelegate:_rootViewController.view];
		
		_adMobView.adUnitID = _publisherId;
	}
	
    void start(const char* publisherId)
    {
		_publisherId = CreateNSString(publisherId);
        _request = [GADRequest request];
		_rootViewController = UnityGetGLViewController();
        
        _rootViewController.view.autoresizesSubviews = true;
		
		CreateAdMobView();
    }
    
    void setTestDevices(NSString* testDevices[])
    {
        /*
		NSString* deviceID;
		
		if (NSClassFromString(@"ASIdentifierManager"))
		{
			deviceID = [[[ASIdentifierManager sharedManager]advertisingIdentifier] UUIDString];
		}
		else
		{
			deviceID = [[UIDevice currentDevice] uniqueIdentifier];
		}
		
		_request.testDevices = [NSArray arrayWithObjects:@"YOUR_SIMULATOR_IDENTIFIER", deviceID, nil];
		*/
		
		_request.testDevices = [NSArray arrayWithObjects:@"487cff8692cb61efc8c5ad5c540ab3ac", nil];
    }
    
    void setType(int type)
    {
        switch (type)
        {
            case 0:
				//_adMobView.adSize = kGADAdSizeBanner;
				[_adMobView setAdSize:kGADAdSizeBanner];
                break;
                
            case 1:
				[_adMobView setAdSize:kGADAdSizeMediumRectangle];
                break;
                
            case 2:
				[_adMobView setAdSize:kGADAdSizeFullBanner];
                break;
                
            case 3:
				[_adMobView setAdSize:kGADAdSizeLeaderboard];
                break;
                
            default:
                
                break;
        }
    }
    
    void createBanner()
    {
		if(_adMobView == nil)
		{
			CreateAdMobView();
		}
		
        // Set the Pos
        _adMobView.frame = CGRectMake(	(_rootViewController.view.frame.size.width / 2) - (_adMobView.frame.size.width / 2),
                                        _rootViewController.view.frame.size.height - _adMobView.frame.size.height,
                                        _adMobView.frame.size.width,
                                        _adMobView.frame.size.height);
        
        _adMobView.contentMode = UIViewContentModeBottom;
        _adMobView.autoresizingMask =   UIViewAutoresizingFlexibleLeftMargin |
                                        UIViewAutoresizingFlexibleRightMargin |
                                        UIViewAutoresizingFlexibleTopMargin;
        // request ads
        [_adMobView loadRequest:_request];
    }
    
    void destroyBanner()
    {
        _adMobView.delegate = nil;
        
        // Don't release the bannerView_ if you are using ARC in your project
        [_adMobView release];
    }
    
    void showOrHideBanner(bool shouldBeVisible)
    {
        if(shouldBeVisible == true)
		{
			if(![_adMobView isDescendantOfView:_rootViewController.view])
			{
				[_rootViewController.view addSubview:_adMobView];
			}
		}
		else
		{
			if([_adMobView isDescendantOfView:_rootViewController.view])
			{
				[_adMobView removeFromSuperview];
			}
		}
    }
    
    void refreshAd()
    {
		GADRequest *oldRequest = _request;
		
        _request = [GADRequest request];
		_request.testDevices = oldRequest.testDevices;
		
		[_adMobView loadRequest:_request];
    }
}

@implementation AdMobPlugin
	
	- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
	{
		NSLog(@"adViewDidReceiveAd");
	}

	- (void)adView:(GADBannerView *)bannerView
	didFailToReceiveAdWithError:(GADRequestError *)error
	{
		//NSLog(@”adView:didFailToReceiveAdWithError:%@”, [error localizedDescription]);
        NSLog(@"adViewdidFailToReceiveAdWithError");
    }

	- (void)adViewWillPresentScreen:(GADBannerView *)bannerView
	{
		NSLog(@"adViewWillPresentScreen");
	}

	- (void)adViewDidDismissScreen:(GADBannerView *)bannerView
	{
		NSLog(@"adViewDidDismissScreen");
	}

	- (void)adViewWillDismissScreen:(GADBannerView *)bannerView
	{
		NSLog(@"adViewWillDismissScreen");
	}

	- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView
	{
		NSLog(@"adViewWillLeaveApplication");
    }

	- (void)dealloc
	{
		NSLog(@"dealloc");
		
		destroyBanner();
		
		[super dealloc];
	}

@end

#pragma mark -
#pragma mark Helpers

static NSString* CreateNSString(const char* string)
{
	if (string != NULL)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}