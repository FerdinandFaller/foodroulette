using UnityEngine;
using System.Runtime.InteropServices;

#if UNITY_ANDROID
namespace AdMobMediationPlugin
{
    public class AdMobAndroid : AdMob
    {
        private readonly AndroidJavaObject _session;

        public AdMobAndroid(string appId, string gameObjectName)
        {
            GameObjectName = gameObjectName;
            var unityRevMobClass = new AndroidJavaClass("de.FF.AdMobMediation");
            _session = unityRevMobClass.CallStatic<AndroidJavaObject>("Start",
                                                                       CurrentActivity(),
                                                                       appId);
        }

        public static AndroidJavaObject CurrentActivity()
        {
            var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            return unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        }

        public override bool IsDevice()
        {
            return (Application.platform == RuntimePlatform.Android);
        }

        public override void SetTestDevices(string[] testDevices)
        {
            if (!IsDevice()) return;

            _session.Call("SetTestDevices", testDevices);
        }

        public override void SetType(AdType type)
        {
            if (!IsDevice()) return;

            _session.Call("SetType", (int)type);
        }

        public override void CreateBanner()
        {
            if (!IsDevice()) return;

            _session.Call("CreateBanner");
        }

        public override void DestroyBanner()
        {
            if (!IsDevice()) return;

            _session.Call("DestroyBanner");
        }

        public override void ShowOrHideBanner(bool shouldBeVisible)
        {
            if (!IsDevice()) return;

            _session.Call("ShowOrHideBanner", shouldBeVisible);
        }

        public override void RefreshAd()
        {
            if (!IsDevice()) return;

            _session.Call("RefreshAd");
        }
    }
}
#endif