using System.Runtime.InteropServices;
using UnityEngine;

#if UNITY_IPHONE
namespace AdMobMediationPlugin
{
    public class AdMobIOS : AdMob
    {
        [DllImport("__Internal")]
        private static extern void start(string appId);

        [DllImport("__Internal")]
        private static extern void setTestDevices(string[] testDevices);

        [DllImport("__Internal")]
        private static extern void setType(int type);

        [DllImport("__Internal")]
        private static extern void createBanner();

        [DllImport("__Internal")]
        private static extern void destroyBanner();

        [DllImport("__Internal")]
        private static extern void showOrHideBanner(bool shouldBeVisible);
        
        [DllImport("__Internal")]
        private static extern void refreshAd();


        public AdMobIOS(string appId, string gameObjectName)
        {
            GameObjectName = gameObjectName;

            start(appId);
        }

        public override bool IsDevice()
        {
            return (Application.platform == RuntimePlatform.IPhonePlayer);
        }

        public override void SetTestDevices(string[] testDevices)
        {
            setTestDevices(testDevices);
        }

        public override void SetType(AdType type)
        {
            setType((int)type);
        }

        public override void CreateBanner()
        {
            createBanner();
        }

        public override void DestroyBanner()
        {
            destroyBanner();
        }

        public override void ShowOrHideBanner(bool shouldBeVisible)
        {
            showOrHideBanner(shouldBeVisible);
        }

        public override void RefreshAd()
        {
            refreshAd();
        }
    }
}
#endif