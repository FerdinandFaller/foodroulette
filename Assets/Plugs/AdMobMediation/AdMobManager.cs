using System.Collections.Generic;
using AdMobMediationPlugin;
using UnityEngine;

public class AdMobManager : MonoBehaviour
{
    // Just replace the IDs below with your appIDs.
    private static readonly Dictionary<string, string> AppIds = new Dictionary<string, string>() 
        {
            { "Android", "090c911ae81f4aea"},
            { "IOS", "905c4ec309fa4c7f" }
        };
    private AdMob _adMob;

    public static AdMobManager Instance;


    void Awake()
    {
        Instance = this;

        Debug.Log("Creating AdMob Session");
        _adMob = AdMob.Start(AppIds, gameObject.name);

        if(_adMob == null)
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start()
    {
        // _adMob.SetTestDevices(new[] { "24848F959236A0D409A4F9EA64987B8D", "487cff8692cb61efc8c5ad5c540ab3ac" });
        _adMob.SetType(AdType.PhoneAndTablet320X50);
        _adMob.CreateBanner();
    }
}