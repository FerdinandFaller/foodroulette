﻿namespace AdMobMediationPlugin
{
    public enum AdType
    {
        PhoneAndTablet320X50 = 0,
        Tablet300X250,
        Tablet468X60,
        Tablet728X90,
    }

    interface IAdvertisement
    {
        bool IsDevice();
        void SetTestDevices(string[] testDevices);
        void SetType(AdType type);
        void CreateBanner();
        void DestroyBanner();
        void ShowOrHideBanner(bool shouldBeVisible);
        void RefreshAd();
    }
}
