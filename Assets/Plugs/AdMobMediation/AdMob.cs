using System.Collections.Generic;
using UnityEngine;


namespace AdMobMediationPlugin
{
	public abstract class AdMob : IAdvertisement
	{
		protected string GameObjectName;

		public static AdMob Start(Dictionary<string, string> appIds, string gameObjectName)
		{
			Debug.Log("Creating RevMob Session");
#if UNITY_EDITOR
			Debug.Log("It can't run in Unity Editor. Only in iOS or Android devices.");
			return null;
#elif UNITY_ANDROID
		AdMob session = new AdMobAndroid(appIds["Android"], gameObjectName);
		return session;
#elif UNITY_IPHONE
		AdMob session = new AdMobIOS(appIds["IOS"], gameObjectName);
		return session;
#else
		return null;
#endif
		}

		public abstract bool IsDevice();
		public abstract void SetTestDevices(string[] testDevices);
		public abstract void SetType(AdType type);
		public abstract void CreateBanner();
		public abstract void DestroyBanner();
		public abstract void ShowOrHideBanner(bool shouldBeVisible);
		public abstract void RefreshAd();
	}
}