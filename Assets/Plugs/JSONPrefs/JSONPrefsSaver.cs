// Copyright (c) 2013 Ferdinand Faller
// Please direct any bugs/comments/suggestions to Dev.Ferdinand.Faller@gmail.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

namespace FF.JSONPrefs
{
    public class JSONPrefsSaver : MonoBehaviour
    {
        private float _saveInterval;
        public float SaveInterval
        {
            get { return _saveInterval; }
            set
            {
                _saveInterval = value;
                _lastSave = Time.realtimeSinceStartup;
            }
        }

        private bool _saveOnInterval;
        public bool SaveOnInterval
        {
            get { return _saveOnInterval; }
            set
            {
                _saveOnInterval = value;
                _lastSave = Time.realtimeSinceStartup;
            }
        }

        public bool SaveOnApplicationQuit { get; set; }
        public bool SaveOnApplicationPause { get; set; }

        public string IntsPath { get; private set; }
        public string FloatsPath { get; private set; }
        public string StringsPath { get; private set; }
        public string BoolsPath { get; private set; }

        private float _lastSave;


        void Awake()
        {
#if UNITY_WEBPLAYER || UNITY_NACL
            Debug.LogError("JSONPrefs doesn't support Webplayer Builds!");
#endif

            DontDestroyOnLoad(gameObject);

            SaveOnApplicationQuit = true;
            SaveOnApplicationPause = true;

            IntsPath = Application.persistentDataPath + "/JSONPrefs/Ints.txt";
            FloatsPath = Application.persistentDataPath + "/JSONPrefs/Floats.txt";
            StringsPath = Application.persistentDataPath + "/JSONPrefs/Strings.txt";
            BoolsPath = Application.persistentDataPath + "/JSONPrefs/Bools.txt";
        }

        void Update()
        {
            if (!SaveOnInterval || !(Time.realtimeSinceStartup - _lastSave > SaveInterval)) return;
            _lastSave = Time.realtimeSinceStartup;

            JSONPrefs.Save();
        }

        public void OnApplicationPause()
        {
            if (SaveOnApplicationPause) JSONPrefs.Save();
        }

        public void OnApplicationQuit()
        {
            if (SaveOnApplicationQuit) JSONPrefs.Save();
        }
    }
}