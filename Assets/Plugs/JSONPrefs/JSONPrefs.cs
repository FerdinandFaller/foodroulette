// Copyright (c) 2013 Ferdinand Faller
// Please direct any bugs/comments/suggestions to Dev.Ferdinand.Faller@gmail.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LitJson;
using UnityEngine;

namespace FF.JSONPrefs
{
    public static class JSONPrefs
    {
        private static bool _usePrettyPrint;
        private static Dictionary<string, int> _ints = new Dictionary<string, int>();
        private static Dictionary<string, double> _floats = new Dictionary<string, double>();
        private static Dictionary<string, string> _strings = new Dictionary<string, string>();
        private static Dictionary<string, bool> _bools = new Dictionary<string, bool>();
        private static readonly JSONPrefsSaver Saver;


        static JSONPrefs()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            // Instantiate the JSONPrefsSaver
            GameObject saverGo = null;
            if (Saver == null) saverGo = new GameObject("JSONPrefsSaver", typeof(JSONPrefsSaver));
            if (saverGo != null) Saver = saverGo.GetComponent<JSONPrefsSaver>();

            // Set the default Configuration
            Configure();

            // Read existing Data from Disc
            ReadInts();
            ReadFloats();
            ReadStrings();
            ReadBools();
#endif
        }

        #region Private Methods

        private static void ReadInts()
        {
            var json = ReadFromPath(Saver.IntsPath);
            var tempDict = JsonMapper.ToObject<Dictionary<string, int>>(json);
            if (tempDict != null) _ints = tempDict;
        }

        private static void ReadFloats()
        {
            var json = ReadFromPath(Saver.FloatsPath);
            var tempDict = JsonMapper.ToObject<Dictionary<string, double>>(json);
            if (tempDict != null) _floats = tempDict;
        }

        private static void ReadStrings()
        {
            var json = ReadFromPath(Saver.StringsPath);
            var tempDict = JsonMapper.ToObject<Dictionary<string, string>>(json);
            if (tempDict != null) _strings = tempDict;
        }

        private static void ReadBools()
        {
            var json = ReadFromPath(Saver.BoolsPath);
            var tempDict = JsonMapper.ToObject<Dictionary<string, bool>>(json);
            if (tempDict != null) _bools = tempDict;
        }

        private static void WriteToPath(object data, string path)
        {
            CreatePath(path);

            StreamWriter streamWriter = null;

            try
            {
                var stream = File.Open(path, FileMode.Truncate);
                streamWriter = new StreamWriter(stream);
                var jsonWriter = new JsonWriter(streamWriter) { PrettyPrint = _usePrettyPrint };
                JsonMapper.ToJson(data, jsonWriter);
            }
            catch (Exception ex)
            {
                Debug.LogError("JSONPrefs WriteException: " + ex.Message);
            }
            finally
            {
                if (streamWriter != null) streamWriter.Close();
            }
        }

        private static string ReadFromPath(string path)
        {
            CreatePath(path);

            var json = "";
            TextReader reader = null;

            try
            {
                var stream = File.Open(path, FileMode.Open);
                reader = new StreamReader(stream);
                json = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                Debug.LogError("JSONPrefs ReadException: " + ex.Message);
            }
            finally
            {
                if (reader != null) reader.Close();
            }

            return json;
        }

        private static void CreatePath(string path)
        {
            if(string.IsNullOrEmpty(path)) return;

            FileStream stream = null;

            try
            {
                var p = Path.GetDirectoryName(path);
                if (!string.IsNullOrEmpty(p) && !Directory.Exists(p)) Directory.CreateDirectory(p);
                if (!File.Exists(path))
                {
                    stream = File.Create(path);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("JSONPrefs Exception: " + ex.Message);
            }
            finally
            {
                if (stream != null) stream.Close();
            }
        }

        private static bool HasIntKey(string key)
        {
            return _ints.ContainsKey(key);
        }

        private static bool HasFloatKey(string key)
        {
            return _floats.ContainsKey(key);
        }

        private static bool HasStringKey(string key)
        {
            return _strings.ContainsKey(key);
        }

        private static bool HasBoolKey(string key)
        {
            return _bools.ContainsKey(key);
        }

        #endregion // Private Methods

        #region Public Methods

        /// <summary>
        /// Use this to configure how JSONPrefs should handle saving. Gets automatically called with the default values by the constructor.
        /// </summary>
        /// <param name="usePrettyPrint">Should your Data be nicely readable in the .txt file? Changes come to effect at the next Save() call. Default = true.</param>
        /// <param name="saveOnInterval">Should your Data be regularly saved? Default = false</param>
        /// <param name="saveInterval">The Interval (in seconds), your Data gets saved when saveOnInterval is true. Default = 60.</param>
        /// <param name="saveOnApplicationPause">Should your Data be saved when the Application gets paused? Default = true.</param>
        /// <param name="saveOnApplicationQuit">Should your Data be saved when the Application gets terminated? Default = true.</param>
        public static void Configure(bool usePrettyPrint = true, bool saveOnInterval = false, float saveInterval = 60, bool saveOnApplicationPause = true, bool saveOnApplicationQuit = true)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            _usePrettyPrint = usePrettyPrint;

            Saver.SaveOnApplicationPause = saveOnApplicationPause;
            Saver.SaveOnApplicationQuit = saveOnApplicationQuit;
            Saver.SaveOnInterval = saveOnInterval;
            Saver.SaveInterval = saveInterval;
#endif
        }

        /// <summary>
        /// Use this to get access to the internal integer collection (ReadOnly).
        /// </summary>
        /// <returns>Returns the internal integer Dictionary (ReadOnly).</returns>
        public static ReadOnlyDictionary<string, int> GetReadOnlyIntCollection()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            return new ReadOnlyDictionary<string, int>(_ints);
#else
            return new ReadOnlyDictionary<string, int>();
#endif
        }

        /// <summary>
        /// Use this to retrieve an integer (System.Int32).
        /// </summary>
        /// <param name="key">The key of the integer you want to retrieve.</param>
        /// <param name="defaultValue">The value that is returned when the key isn't found. Default = 0.</param>
        /// <returns>Returns either the integer which is associated with the given key, or the default value.</returns>
        public static int GetInt(string key, int defaultValue = 0)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            int value;
            return _ints.TryGetValue(key, out value) ? value : defaultValue;
#else
            return defaultValue;
#endif
        }

        /// <summary>
        /// Use this to set/add an integer (System.Int32). Adding when the key isn't already associated.
        /// </summary>
        /// <param name="key">The key of the integer you want to set/add.</param>
        /// <param name="value">The integer you want to set/add.</param>
        public static void SetInt(string key, int value)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            if (HasIntKey(key))
            {
                _ints[key] = value;
            }
            else
            {
                _ints.Add(key, value);
            }
#endif
        }

        /// <summary>
        /// Use this to get access to the internal float collection (ReadOnly).
        /// </summary>
        /// <returns>Returns the internal float Dictionary (ReadOnly).</returns>
        public static ReadOnlyDictionary<string, float> GetReadOnlyFloatCollection()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            var floatDict = _floats.ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => (float)keyValuePair.Value);

            return new ReadOnlyDictionary<string, float>(floatDict);
#else
            return new ReadOnlyDictionary<string, float>();
#endif
        }

        /// <summary>
        /// Use this to retrieve a float (System.Single).
        /// </summary>
        /// <param name="key">The key of the float you want to retrieve.</param>
        /// <param name="defaultValue">The value that is returned when the key isn't found. Default = 0f.</param>
        /// <returns>Returns either the float which is associated with the given key, or the default value.</returns>
        public static float GetFloat(string key, float defaultValue = 0f)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            double value;
            return _floats.TryGetValue(key, out value) ? (float)value : defaultValue;
#else
            return defaultValue;
#endif
        }

        /// <summary>
        /// Use this to set/add a float (System.Single). Adding when the key isn't already associated.
        /// </summary>
        /// <param name="key">The key of the float you want to set/add.</param>
        /// <param name="value">The float you want to set/add.</param>
        public static void SetFloat(string key, float value)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            if (HasFloatKey(key))
            {
                _floats[key] = value;
            }
            else
            {
                _floats.Add(key, value);
            }
#endif
        }

        /// <summary>
        /// Use this to get access to the internal string collection (ReadOnly).
        /// </summary>
        /// <returns>Returns the internal integer Dictionary (ReadOnly)</returns>
        public static ReadOnlyDictionary<string, string> GetReadOnlyStringCollection()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            return new ReadOnlyDictionary<string, string>(_strings);
#else
            return new ReadOnlyDictionary<string, string>();
#endif
        }

        /// <summary>
        /// Use this to retrieve a string (System.String).
        /// </summary>
        /// <param name="key">The key of the string you want to retrieve</param>
        /// <param name="defaultValue">The value that is returned when the key isn't found. Default = ""</param>
        /// <returns>Returns either the string which is associated with the given key, or the default value.</returns>
        public static string GetString(string key, string defaultValue = "")
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            string value;
            return _strings.TryGetValue(key, out value) ? value : defaultValue;
#else
            return defaultValue;
#endif
        }

        /// <summary>
        /// Use this to set/add a string (System.String). Adding when the key isn't already associated.
        /// </summary>
        /// <param name="key">The key of the string you want to set/add.</param>
        /// <param name="value">The string you want to set/add.</param>
        public static void SetString(string key, string value)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            if (HasStringKey(key))
            {
                _strings[key] = value;
            }
            else
            {
                _strings.Add(key, value);
            }
#endif
        }

        /// <summary>
        /// Use this to get access to the internal bool collection (ReadOnly).
        /// </summary>
        /// <returns>Returns the internal integer Dictionary (ReadOnly)</returns>
        public static ReadOnlyDictionary<string, bool> GetReadOnlyBoolCollection()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            return new ReadOnlyDictionary<string, bool>(_bools);
#else
        return new ReadOnlyDictionary<string, bool>();
#endif
        }

        /// <summary>
        /// Use this to retrieve a bool (System.Boolean).
        /// </summary>
        /// <param name="key">The key of the bool you want to retrieve</param>
        /// <param name="defaultValue">The value that is returned when the key isn't found. Default = false</param>
        /// <returns>Returns either the bool which is associated with the given key, or the default value.</returns>
        public static bool GetBool(string key, bool defaultValue = false)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            bool value;
            return _bools.TryGetValue(key, out value) ? value : defaultValue;
#else
            return defaultValue;
#endif
        }

        /// <summary>
        /// Use this to set/add a bool (System.Boolean). Adding when the key isn't already associated.
        /// </summary>
        /// <param name="key">The key of the bool you want to set/add.</param>
        /// <param name="value">The bool you want to set/add.</param>
        public static void SetBool(string key, bool value)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            if (HasBoolKey(key))
            {
                _bools[key] = value;
            }
            else
            {
                _bools.Add(key, value);
            }
#endif
        }

        /// <summary>
        /// Use this to check if a given key is already associated.
        /// </summary>
        /// <param name="key">The key you want to check.</param>
        /// <returns>Returns true if the key is already associated, otherwise false.</returns>
        public static bool HasKey(string key)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            return HasIntKey(key) ||
                   HasFloatKey(key) ||
                   HasStringKey(key) ||
                   HasBoolKey(key);
#else
            return false;
#endif
        }

        /// <summary>
        /// Use this to delete a given key and its value.
        /// </summary>
        /// <param name="key">The key you want to delete.</param>
        public static void DeleteKey(string key)
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            if (HasIntKey(key))
            {
                _ints.Remove(key);
                Save();
            }

            if (HasFloatKey(key))
            {
                _floats.Remove(key);
                Save();
            }

            if (HasStringKey(key))
            {
                _strings.Remove(key);
                Save();
            }

            if (HasBoolKey(key))
            {
                _bools.Remove(key);
                Save();
            }
#endif
        }

        /// <summary>
        /// Use this if you want to delete all keys and values.
        /// </summary>
        public static void DeleteAll()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            _ints.Clear();
            _floats.Clear();
            _strings.Clear();
            _bools.Clear();
            Save();
#endif
        }

        /// <summary>
        /// Use this to save all keys and values to disc.
        /// </summary>
        public static void Save()
        {
#if !UNITY_WEBPLAYER && !UNITY_NACL
            WriteToPath(_ints, Saver.IntsPath);
            WriteToPath(_floats, Saver.FloatsPath);
            WriteToPath(_strings, Saver.StringsPath);
            WriteToPath(_bools, Saver.BoolsPath);
#endif
        }

        #endregion // Public Methods
    }
}