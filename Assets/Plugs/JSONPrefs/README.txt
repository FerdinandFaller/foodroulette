
-- JSONPrefs V1.0 --

Creator: Ferdinand Faller


Description:
JSONPrefs is an alternative to PlayerPrefs for persistant Data Storage. You can use JSONPrefs exactly like PlayerPrefs.
JSONPrefs offers some additional Features over PlayerPrefs, listed under "Features".

JSONPrefs uses litJSON v0.7.0 (http://lbv.github.io/litjson/)


Features:
- A lot faster than PlayerPrefs
- SetBool and GetBool Methods
- Access to the internal Dictionaries
- Save on ApplicationPause and/or on ApplicationQuit and/or on a definable Interval
- Uses JSON
- Easy and fast switch from PlayerPrefs possible, due to identical Method Names. Just replace 'PlayerPrefs' with 'JSONPrefs'!